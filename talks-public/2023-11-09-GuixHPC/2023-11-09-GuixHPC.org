#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Software Heritage, the universal archive at the crossroads of Software Forges
#+AUTHOR: Benoit Chauvet
#+EMAIL: benoit.chauvet@inria.fr

#+BEAMER_HEADER: \date[09 November 2023]{09 November 2023\\Guix HPC Workshop\\Montpellier, France\\[-3ex]}
#+BEAMER_HEADER: \title[Software Heritage: Archive your source code]{Software Heritage: Archive your source code}
#+BEAMER_HEADER: \author[Benoit Chauvet~~~~ benoit.chauvet@inria.fr ~~~~ (CC-BY-SA 4.0)]{Benoit Chauvet}
#+BEAMER_HEADER: \institute[Inria]{Software Heritage\\Software Engineering Manager}

#+INCLUDE: "../../common/modules/prelude-toc.org" :minlevel 1
#+INCLUDE: "../../common/modules/169.org"

* Introduction
** Who we are
*** 
    - [[https://www.softwareheritage.org][Software Heritage]] 
    - Started in 2015 (Roberto Di Cosmo & Stefano Zacchiroli)
    - Universal archive of source code
    - Initiated by Inria, in collaboration with Unesco
    #+BEGIN_EXPORT latex
    \begin{center}
    \includegraphics[width=.7\linewidth]{swh-team-2022}
    \end{center}
    #+END_EXPORT   
* Free Open source Software and Research 
** Software is everywhere (and a key mediator) in society
#+latex: \begin{center}
#+ATTR_LATEX: :width .7\linewidth
file:software-center.pdf
#+latex: \end{center}
# #+INCLUDE: "../../common/modules/foss-commons.org::#freeswdef"
** A few basic needs for research software
   #+INCLUDE: "../../common/modules/swh-ardc.org::#ardc" :only-contents t :minlevel 3
* An Endangered Knowledge
** But /where/ is this commons?
   #+latex: \begin{flushleft}
   #+ATTR_LATEX: :width \extblockscale{.7\linewidth}
   file:myriadsources.png
   #+latex: \end{flushleft}
*** 
    - many disparate *development* platforms, with a few dominant players (e.g.,
      GitHub)
    - a myriad places where *distribution* may happen
    - most of them operated by *for-profit* companies
** Software source code is fragile
#+INCLUDE: "../../common/modules/swh-motivations.org::#fragile" :only-contents t :minlevel 3
* Our Mission
** Software Heritage in a nutshell \hfill www.softwareheritage.org
   #+INCLUDE: "../../common/modules/swh-goals-oneslide-vertical.org::#goals" :only-contents t :minlevel 3
** The largest public source code archive, principled \hfill \small \url{bit.ly/swhpaper}
*** 
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.5
    :END:
    #+latex: \centering
    #+ATTR_LATEX: :width \linewidth
    file:SWH-as-foundation-slim.png
*** 
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.5
    :END:
    #+latex: \centering
    #+ATTR_LATEX: :width \linewidth
    file:archive-growth.png\\
    [[https://archive.softwareheritage.org][archive.softwareheritage.org]]
*** linebreak                                               :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEAMER: \pause
*** Technology
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.34
    :END:
    - transparency and FOSS
    - replicas all the way down
*** Content (billions!)
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.32
    :END:
    - intrinsic identifiers
    - facts and provenance
*** Organization
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.33
    :END:
    - non-profit
    - multi-stakeholder
** A peek under the hood: a global view on the software commons
*** 
    #+BEAMER: \begin{center}
    #+BEAMER: \vspace{-2mm}
    #+BEAMER: \includegraphics[width=.8\textwidth]{swh-dataflow-merkle.pdf}
    #+BEAMER: \end{center}
    #+BEAMER: \pause
*** 
    #+BEAMER: \vspace{-2mm} \small
    A *global graph* linking together fully *deduplicated* source code artifact
    (files, commits, directories, releases, etc.) to the places that distribute
    them (e.g., Git repositories), providing a *unified view* on the entire
    */Software Commons/*.
    (Size: *~30 B* nodes, *~300 B* edges, *~1 PiB* blobs)
* Collaboration Guix / SWH
** How does this relate to Guix?
- Nothing is eternal, source code (in all forms) disappears
- Hopefully, SWH keeps a copy of everything
- [[https://www.softwareheritage.org/2019/04/18/software-heritage-and-gnu-guix-join-forces-to-enable-long-term-reproducibility/][Guix ensures source code is archived in SWH when building]] ("Save Code Now")
- After source code actually disappears, falls back to SWH when rebuilding ("Software Heritage Vault")
* Archive your code 
** Archive a repository - Save code now
*** Archive a single repository
    #+BEGIN_EXPORT latex
    \begin{center}
    \includegraphics[width=.85\linewidth]{save-code-now-1}
    \end{center}
    #+END_EXPORT   
** SWH Browser extension
    - Directly check the availability of your repos in the archive
    #+BEGIN_EXPORT latex
    \begin{center}
    \includegraphics[width=.85\linewidth]{browser}
    \end{center}
    #+END_EXPORT   
    - [[https://www.softwareheritage.org/browser-extensions/?lang=fr][HowTo and download...]]
** Archive a repository - Webhooks
*** Automate archival from your forge
    - Automatically trigger archival in Software Heritage
    - Triggering events:
        - Tags or branch creation
        - Release creation
        - ...
    - Available for:
        - Bitbucket
        - Gitea
        - GitHub
        - GitLab
        - Sourceforge
    - [[https://www.softwareheritage.org/2023/06/01/webhooks-integrate-swh-workflow/][Webhooks howto available here...]]
** Archive a forge - Add forge now
*** Archive a whole forge
    #+BEGIN_EXPORT latex
    \begin{center}
    \includegraphics[width=.85\linewidth]{add-forge-now-1}
    \end{center}
    #+END_EXPORT
* Identify, cite and reference your code
** Software Heritage /intrinsic/ Identifiers (SWHID) \hfill [[https://docs.softwareheritage.org/devel/swh-model/persistent-identifiers.html][(full spec)]]
# #+INCLUDE: "../../common/modules/swh-id-syntax.org::#swh-id-syntax" :only-contents t :minlevel 3
    #+BEGIN_EXPORT latex
    \begin{center}
    \includegraphics[width=.85\linewidth]{SWHID-v1.4_3.png}
    \end{center}
    #+END_EXPORT   
    - Software Hash IDentifier - [[https://www.swhid.org/specification/v1.1/][Spec v1.1]]
* HAL deposit    
** HAL Deposit
    #+BEGIN_EXPORT latex
    \begin{center}
    \includegraphics[width=.3\linewidth]{HAL-logo}
    \end{center}
    #+END_EXPORT   
*** How to deposit
    - Local method: deposit .zip/.tar.gz file
    - SWHID method: deposit SWHID + metadata
*** Embedded metadata
    - README file (Mardown or plain text)
    - AUTHORS files (plain text)
    - LICENCE file (plain text) or LICENCES directory
    - codemeta.json file [[https://codemeta.github.io/codemeta-generator/][CodeMeta generator]]
** CodeMeta generator
    #+BEGIN_EXPORT latex
    \begin{center}
    \includegraphics[width=1\linewidth]{CodeMetaGeneratorFull}
    \end{center}
    #+END_EXPORT
** HAL deposit with SWHID method
*** SWHID Deposit in HAL
    - https://www.softwareheritage.org/2023/04/04/swhid-deposit-hal/
    #+BEGIN_EXPORT latex
    \begin{center}
    \includegraphics[width=.4\linewidth]{hal-deposit-swhid}
    \end{center}
    #+END_EXPORT
** HAL tools
*** Search and expose software publications
    - [[https://haltools.archives-ouvertes.fr/?action=export&lang=fr][https://haltools.archives-ouvertes.fr]]
    #+BEGIN_EXPORT latex
    \begin{center}
    \includegraphics[width=.6\linewidth]{hal-tools}
    \end{center}
    #+END_EXPORT
* Conclusion
** 
   \vfill
   \centerline{\Huge Questions ?}
   \vfill
* Appendix
** Appendix
*** Software Heritage
    - [[https://archive.softwareheritage.org][Browse the archive]]
    - [[https://www.softwareheritage.org/save-and-reference-research-software/][Save and reference research software]]
    - [[https://www.softwareheritage.org/howto-archive-and-reference-your-code/][HOWTO archive and reference your code]]
    - [[https://archive.softwareheritage.org/save/][Save Code Now]]
    - [[https://www.softwareheritage.org/browser-extensions/?lang=fr][SWH browser extension]]
    - [[https://archive.softwareheritage.org/add-forge/request/create/][Add Forge Now]]
    - [[https://www.softwareheritage.org/2023/06/01/webhooks-integrate-swh-workflow/][Webhooks for auto-archival]]
*** Misc
    - [[https://codemeta.github.io/codemeta-generator/][CodeMeta generator]]
    - [[https://www.softwareheritage.org/2023/04/04/swhid-deposit-hal/][SWHID deposit in HAL]]
    - [[https://haltools.archives-ouvertes.fr/?action=export&lang=fr][HAL Tools]]
