#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Software Heritage: Browsing the Free Software Commons
# does not allow short title, so we override it for beamer as follows :
#+BEAMER_HEADER: \title[Software Heritage]{Software Heritage\\Browsing the Free Software Commons}
#+BEAMER_HEADER: \author{Nicolas Dandrimont}
#+BEAMER_HEADER: \date[2018-05-20 MiniDebConf Hamburg]{20 may 2018\\MiniDebConf Hamburg 2018}
#+AUTHOR: Nicolas Dandrimont
#+DATE: 2018-05-20
#+EMAIL: nicolas@dandrimont.eu
#+DESCRIPTION: Software Heritage: Browsing the Free Software Commons
#+KEYWORDS: software heritage legacy preservation knowledge mankind technology

#+INCLUDE: "../../common/modules/prelude-toc.org" :minlevel 1
#+INCLUDE: "../../common/modules/169.org"
#+BEAMER_HEADER: \institute[Software Heritage]{Software Engineer - Software Heritage\\\href{mailto:nicolas@dandrimont.eu}{\tt nicolas@dandrimont.eu}}

#+LATEX_HEADER_EXTRA: \usepackage{bbding}
#+LATEX_HEADER_EXTRA: \DeclareUnicodeCharacter{66D}{\FiveStar}
#+LATEX_HEADER_EXTRA: \usepackage{tikz}
#+LATEX_HEADER_EXTRA: \usetikzlibrary{arrows,shapes}
#+LATEX_HEADER_EXTRA: \definecolor{swh-orange}{RGB}{254,205,27}
#+LATEX_HEADER_EXTRA: \definecolor{swh-red}{RGB}{226,0,38}
#+LATEX_HEADER_EXTRA: \definecolor{swh-green}{RGB}{77,181,174}

#+MACRO: webui_login MiniDebConf
#+MACRO: webui_password 2018

* The Free Software commons
  #+INCLUDE: "../../common/modules/source-code-different-short.org::#softwareisdifferent" :minlevel 2
** Our Software Commons
   #+INCLUDE: "../../common/modules/foss-commons.org::#commonsdef" :only-contents t
   #+BEAMER: \pause
*** Source code is /a precious part/ of our commons
     \hfill are we taking care of it?
  #+INCLUDE: "../../common/modules/swh-motivations-foss.org::#fragile" :minlevel 2
  #+INCLUDE: "../../common/modules/swh-motivations-foss.org::#research" :minlevel 2
* Software Heritage
 #+INCLUDE: "../../common/modules/swh-overview-sourcecode.org::#mission" :minlevel 2
** Core principles
   #+latex: \begin{center}
   #+ATTR_LATEX: :width .9\linewidth
   file:SWH-as-foundation-slim.png
   #+latex: \end{center}
   #+BEAMER: \pause
*** Open approach 					      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.4
    :BEAMER_env: block
    :END:
    - 100% Free and Open Source Software
    - transparency
*** In for the long haul 				      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.4
    :BEAMER_env: block
    :END:
    - replication
    - non profit
 #+INCLUDE: "../../common/modules/status-extended.org::#archivinggoals" :minlevel 2
* Architecture
  #+INCLUDE: "../../common/modules/status-extended.org::#architecture" :only-contents t
#  #+INCLUDE: "../../common/modules/status-extended.org::#merkletree" :minlevel 2
  #+INCLUDE: "../../common/modules/status-extended.org::#merklerevision" :only-contents t
  #+INCLUDE: "../../common/modules/status-extended.org::#archive" :minlevel 2
  #+INCLUDE: "../../common/modules/status-extended.org::#technology" :only-contents t
#  #+INCLUDE: "../../common/modules/status-extended.org::#technology-short" :only-contents t
  #+INCLUDE: "../../common/modules/status-extended.org::#development" :only-contents t
#  #+INCLUDE: "../../common/modules/status-extended.org::#features" :minlevel 2

* Accessing the archive
 # #+INCLUDE: "../../common/modules/webui.org::#webui-short" :minlevel 2 :only-contents t
 #+INCLUDE: "../../common/modules/status-extended.org::#apiintro" :minlevel 2
 #+INCLUDE: "../../common/modules/webui.org::#main" :minlevel 2 :only-contents t
 #+INCLUDE: "../../common/modules/vault.org::#vault-short" :minlevel 2 :only-contents t
 # #+INCLUDE: "../../common/modules/vault.org::#main" :minlevel 2 :only-contents t

* Getting involved
 #+INCLUDE: "../../common/modules/status-extended.org::#features" :minlevel 2
** You can help!
   #+BEAMER: \vspace{-2mm}
*** Coding
    | ٭٭  | Web UI improvements                        |
    | ٭   | loaders/listers for unsupported VCS/forges |
    | ٭٭٭ | developer documentation                    |
    #+BEAMER: \vspace{-2mm} \footnotesize \centering
    \url{https://docs.softwareheritage.org/devel/}
    #+BEAMER: \pause
*** Community
    | ٭٭٭ | spread the world, help us with sustainability |
    | ٭٭  | document endangered source code               |
    #+BEAMER: \vspace{-2mm} \footnotesize \centering
    \url{wiki.softwareheritage.org/index.php?title=Suggestion_box}
    #+BEAMER: \pause
*** Join us
    #+BEAMER: \footnotesize \centering
    - \url{www.softwareheritage.org/jobs} --- *job openings*
    - \url{wiki.softwareheritage.org/index.php?title=Internship} --- *internships*
** Conclusion
*** Software Heritage is
    - a reference archive of *all Free Software* ever written
    - an international, open, nonprofit, *mutualized infrastructure*
    - *now accessible* to developers, users, vendors
    - at the service of our community, *at the service of society*
*** Come in, we're open!
    \url{www.softwareheritage.org} - general information \\
    \url{wiki.softwareheritage.org} - internships, leads \\
    \url{forge.softwareheritage.org} - our own code
* FAQ                                                            :B_appendix:
  :PROPERTIES:
  :BEAMER_env: appendix
  :END:
** Q: do you archive /only/ Free Software?
   - We only crawl origins /meant/ to host source code (e.g., forges)
   - Most (~90%) of what we /actually/ retrieve is textual content
   #+BEAMER: \vfill
*** Our goal
    Archive *the entire Free Software Commons*

  #+BEAMER: \vfill
*** 
   - Large parts of what we retrieve is /already/ Free Software, today
   - Most of the rest /will become/ Free Software in the long term
     - e.g., at copyright expiration
