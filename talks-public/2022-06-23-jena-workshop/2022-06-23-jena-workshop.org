#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: The Software Commons --- Production, risks, and opportunities
#+BEAMER_HEADER: \date[2022-06-23, Jena]{23 June 2022\\The Materiality of Intangible Goods\\Jena, Germany\\[-2mm]}
#+AUTHOR: Stefano Zacchiroli
#+DATE: 23 June 2022
#+EMAIL: stefano.zacchiroli@telecom-paris.fr

#+INCLUDE: "../../common/modules/prelude-toc.org" :minlevel 1
#+INCLUDE: "../../common/modules/169.org"
#+BEAMER_HEADER: \institute[Télécom Paris]{Télécom Paris, Polytechnic Institute of Paris\\ {\tt stefano.zacchiroli@telecom-paris.fr}}
#+BEAMER_HEADER: \title[The Software Commons]{The Software Commons --- Production, risks, and opportunities}
#+BEAMER_HEADER: \author{Stefano Zacchiroli}

* Preface
** About the speaker
  :PROPERTIES:
  :CUSTOM_ID: bio
  :END:
*** 
    - Professor of Computer Science, Télécom Paris, Institut Polytechnique de
      Paris
    - Free/Open Source Software activist (20+ years)
    - Debian Developer & Former 3x Debian Project Leader
    - Former Open Source Initiative (OSI) director
    - Software Heritage co-founder & CTO
** Software is everywhere (and a key mediator) in society
#+latex: \begin{center}
#+ATTR_LATEX: :width .7\linewidth
file:software-center.pdf
#+latex: \end{center}
* The Software Commons
# #+INCLUDE: "../../common/modules/foss-commons.org::#freeswdef"
# #+INCLUDE: "../../common/modules/foss-commons.org::#commonsdef"
** The Commons and FOSS
*** Definition (Commons)
    The *commons* is the cultural and natural resources accessible to all
    members of a society, including natural materials such as air, water, and a
    habitable earth. These resources are held in common, not owned privately.
*** Definition (Software Commons)
    The *software commons* consists of all computer software which is available
    at little or no cost and which can be altered and reused with few
    restrictions. Thus /all open source software and all free software are part
    of the [software] commons/. […]
** Software /source code/ is precious human knowledge
#+INCLUDE: "../../common/modules/source-code-different-short.org::#softwareisdifferent" :only-contents t :minlevel 3
** But /where/ is this commons?
   #+latex: \begin{flushleft}
   #+ATTR_LATEX: :width \extblockscale{.7\linewidth}
   file:myriadsources.png
   #+latex: \end{flushleft}
*** 
    - many disparate *development* platforms, with a few dominant players (e.g.,
      GitHub)
    - a myriad places where *distribution* may happen
    - most of them operated by *for-profit* companies
** Software source code is fragile
#+INCLUDE: "../../common/modules/swh-motivations.org::#fragile" :only-contents t :minlevel 3
** Takeaways
*** Software Commons
- Good news: thanks to FOSS developers we are growing by the day a novel kind
  of digital commons rich in valuable human knowledge: the *software commons*.
*** Risk of losing it
- However: there is a risk of "ruining" it (specifically: losing access to it
  forever).
- As per other commons (including traditional material commons, like natural
  resources), we need to apply *individual care, public policies, and proactive
  initiatives* to mitigate this risk.
* Avoiding the Tragedy of the Unmaintained Software Commons
** Software Heritage in a nutshell \hfill www.softwareheritage.org
   #+INCLUDE: "../../common/modules/swh-goals-oneslide-vertical.org::#goals" :only-contents t :minlevel 3
** The largest public source code archive, principled \hfill \small \url{bit.ly/swhpaper}
*** 
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.5
    :END:
    #+latex: \centering
    #+ATTR_LATEX: :width \linewidth
    file:SWH-as-foundation-slim.png
*** 
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.5
    :END:
    #+latex: \centering
    #+ATTR_LATEX: :width \linewidth
    file:2021-09-archive-growth.png\\
    [[https://archive.softwareheritage.org][archive.softwareheritage.org]]
*** linebreak                                               :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEAMER: \pause
*** Technology
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.34
    :END:
    - transparency and FOSS
    - replicas all the way down
*** Content (billions!)
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.32
    :END:
    - intrinsic identifiers
    - facts and provenance
*** Organization
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.33
    :END:
    - non-profit
    - multi-stakeholder
** A peek under the hood: a global view on the software commons
*** 
    #+BEAMER: \begin{center}
    #+BEAMER: \vspace{-2mm}
    #+BEAMER: \includegraphics[width=.8\textwidth]{swh-dataflow-merkle.pdf}
    #+BEAMER: \end{center}
    #+BEAMER: \pause
*** 
    #+BEAMER: \vspace{-2mm} \small
    A *global graph* linking together fully *deduplicated* source code artifact
    (files, commits, directories, releases, etc.) to the places that distribute
    them (e.g., Git repositories), providing a *unified view* on the entire
    */Software Commons/*.

    (Size: *~30 B* nodes, *~300 B* edges, *~1 PiB* blobs)
* Opportunities
** The Software Commons is growing fast and becoming more diverse
An observatory on the Software Commons enables studying broad phenomena and
trends.
#+BEAMER: \begin{center} \vspace{-3mm} \includegraphics[width=0.6\textwidth]{revision-content-growth-EMSE2020.png} \end{center}
- *Original content* released as public code doubles every 22-30 months (Di
  Cosmo, Rousseau, Zacchiroli. Emp.Sw.Eng. 2020).
- Other examples: long-term population trends such as *gender diversity*
  (Zacchiroli, IEEE Software 2021) and *geography diversity* (Rossi and
  Zacchiroli. ICSE & MSR 2022) in public code.
# #+BEGIN_EXPORT latex
# \begin{thebibliography}{Foo Bar, 1969}
# \scriptsize
# \bibitem{Rousseau2020} Roberto Di Cosmo, Guillaume Rousseau, Stefano Zacchiroli
# \newblock Software Provenance Tracking at the Scale of Public Source Code
# \newblock Empirical Software Engineering 25(4): 2930-2959 (2020)
# \end{thebibliography}
# #+END_EXPORT

** Who produces Free/Open Source Software?
*** 
- Historically, a community of hobbyists motivated by liberating computer users
- Today: the above + independent professionals + a lot of for-profit companies,
  producing FOSS they control in various ways (licensing, governance, etc.)
- But what is the ratio? We lack /comprehensive/ overviews of the landscape.\\
  Challenges:
  - Size
  - What do you measure? (projects? SLOCs? popularity?)
  - Free riding is not always visible
- How much exploitation (of labor) and appropriation (of value) is happening?
*** linebreak                                               :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEGIN_EXPORT latex
\begin{thebibliography}{Foo Bar, 1969}
\scriptsize
\bibitem{ONeil 2022a} Mathieu O'Neil, Xiaolan Cai, Laure Muselli, Fred Pailler, Stefano Zacchiroli
\newblock The Coproduction of Open Source Software by Volunteers and Big Tech Firms
\newblock Digital Commons Policy Council, 2021.
\bibitem{ONeil 2022b} Mathieu O'Neil, Laure Muselli, Xiaolan Cai, Stefano Zacchiroli
\newblock Co-producing industrial public goods on GitHub: Selective firm cooperation, volunteer-employee labour and participation inequality
\newblock New Media \& Society, April 2022.
\end{thebibliography}
#+END_EXPORT

* Outlook
** Outlook
*** Takeaways
- Collectively, free/open source software developers are building a novel form
  of digital commons: the *software commons*.
- It benefits society as a *library of technical knowledge* and is exploited by
  for-profit companies as an *industrial public good*.
- The *risk of losing access to it* should be countered with /individual care,
  public policies, and proactive initiatives/.
*** Open questions / discussion leads
- *Who is producing* the software commons? (volunteers/employees/civil
  servants/…)
- *Who is benefiting* the most from the software commons?
- Do we need different *public policies* and/or *licensing strategies* to fix
  any of this?
- What *other risks* is the software commons facing?
