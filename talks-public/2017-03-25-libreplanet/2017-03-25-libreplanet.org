#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Software Heritage: Preserving the Free Software Commons
# does not allow short title, so we override it for beamer as follows :
#+BEAMER_HEADER: \title[Software Heritage]{Software Heritage\\Preserving the Free Software Commons}
#+BEAMER_HEADER: \author{Stefano Zacchiroli}
#+BEAMER_HEADER: \date[25/03/2017, LibrePlanet]{25 March 2017\\LibrePlanet 2017\\Boston, MA, USA}
#+AUTHOR: Stefano Zacchiroli
#+DATE: 25 March 2017
#+EMAIL: zack@upsilon.cc
#+DESCRIPTION: Software Heritage: Preserving the Free Software Commons
#+KEYWORDS: software heritage legacy preservation knowledge mankind technology

#+INCLUDE: "../../common/modules/prelude.org" :minlevel 1
#+BEAMER_HEADER: \institute[Software Heritage]{Co-founder \& CTO\\Software Heritage\\\href{mailto:zack@upsilon.cc}{\tt zack@upsilon.cc}}

#+LATEX_HEADER: \usepackage{bbding}
#+LATEX_HEADER: \DeclareUnicodeCharacter{66D}{\FiveStar}

* Software, Source Code, and the Software Commons
** Free Software is everywhere
   #+latex: \begin{center}
   #+ATTR_LATEX: :width .9\linewidth
   file:software-center.pdf
   #+latex: \end{center}
   #+INCLUDE: "../../common/modules/source-code-different-short.org::#softwareisdifferent" :minlevel 2

** Our Software Commons
   #+INCLUDE: "../../common/modules/foss-commons.org::#commonsdef" :only-contents t
   #+BEAMER: \pause
*** Source code is /a precious part/ of our commons
     \hfill are we taking care of it?
  #+INCLUDE: "../../common/modules/swh-motivations-foss.org::#main" :only-contents t :minlevel 2
  #+INCLUDE: "../../common/modules/swh-overview-sourcecode.org::#mission" :minlevel 2

** Our principles
   #+latex: \begin{center}
   #+ATTR_LATEX: :width .9\linewidth
   file:SWH-as-foundation-slim.png
   #+latex: \end{center}
#+BEAMER: \pause
*** Open approach 					      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.4
    :BEAMER_env: block
    :END:
    - 100% Free Software
    - transparency
*** In for the long haul 				      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.4
    :BEAMER_env: block
    :END:
    - replication
    - non profit

** Archiving goals
   Targets: VCS repositories & source code releases (e.g., tarballs)
*** We DO archive
    - file *content* (= blobs)
    - *revisions* (= commits), with full metadata
    - *releases* (= tags), ditto
    - where (*origin*) & when (*visit*) we found any of the above
    # - time-indexed repo *snapshots* (i.e., we never delete anything)
    … in a VCS-/archive-agnostic *canonical data model*
*** We DON'T archive
    # - diffs → derived data from related contents
    - homepages, wikis
    - BTS/issues/code reviews/etc.
    - mailing lists
    Long term vision: play our part in a /"semantic wikipedia of software"/

* Where we are today: technical overview
  #+INCLUDE: "../../common/modules/status-extended.org::#architecture" :only-contents t
  # #+INCLUDE: "../../common/modules/status-extended.org::#merkletree" :minlevel 2
  #+INCLUDE: "../../common/modules/status-extended.org::#giantdag" :only-contents t
  #+INCLUDE: "../../common/modules/status-extended.org::#merklerevision" :only-contents t
  #+INCLUDE: "../../common/modules/status-extended.org::#archive" :minlevel 2
  #+INCLUDE: "../../common/modules/status-extended.org::#api" :only-contents t
  #+INCLUDE: "../../common/modules/status-extended.org::#features" :minlevel 2

* Come in, we're open!
** You can help!
*** Coding
    - \url{forge.softwareheritage.org} --- *our own code*
    #+BEAMER: \vspace{-5mm}
    | ٭٭٭ | listers for unsupported forges, distros, pkg. managers |
    | ٭٭٭ | loaders for unsupported VCS, source package formats    |
    | ٭٭  | Web UI: eye candy wrapper around the Web API           |
    #+BEAMER: \pause
*** Community
    | ٭٭  | spread the news, help us with long-term sustainability |
    | ٭٭٭ | document endangered source code                        |
    #+BEAMER: \vspace{-3mm} \scriptsize \centering
    \url{wiki.softwareheritage.org/index.php?title=Suggestion_box}
    #+BEAMER: \pause
*** Join us
    - \url{www.softwareheritage.org/jobs} --- *job openings*
    - \url{wiki.softwareheritage.org/index.php?title=Internship} --- *internships*
    #+BEAMER: \vspace{-3mm}

** The Software Heritage community
   #+latex: \begin{center}
   #+ATTR_LATEX: :width .38\linewidth
file:core-team-formal.png
   #+latex: \end{center}
   #+BEAMER: \vspace{-2mm}
   #+BEAMER: \pause
*** Inria as initiator 						 :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_opt: pic=inria-logo-new,leftpic=true,width=\extblockscale{.2\linewidth}
    :END:
    - .fr national computer science research entity
    - strong Free Software culture
    # - creating a non profit, international organisation
   #+BEAMER: \vspace{-2mm}
   #+BEAMER: \pause
*** Early Sponsors and Supporters
    *Société Générale, Microsoft, Huawei, Nokia, DANS, Univ. Bologna,* 
   #+latex: ~~
    ACM, Creative Commons, Eclipse, Engineering, FSF, Gandi, GitHub, IEEE, OIN,
    OSI, OW2, Software Freedom Conservancy, SFLC, The Document Foundation, ...

* Conclusion
** Conclusion
*** Software Heritage is
    - a /reference archive/ of /all/ Free Software ever written
    # - a fantastic new tool for /research/ software
    - a unique /complement/ for /development platforms/
    - an international, open, nonprofit, /mutualized infrastructure/
    - at the service of our community, at the service of society
*** Come in, we're open!
    \url{www.softwareheritage.org} --- /sponsoring/, /job openings/ \\
    \url{wiki.softwareheritage.org} --- /internships/, /leads/ \\
    \url{forge.softwareheritage.org} --- /our own code/
    #+BEAMER:  \vfill \flushright {\Huge Questions?} \vfill

* FAQ                                                            :B_appendix:
  :PROPERTIES:
  :BEAMER_env: appendix
  :END:
** Q: do you archive /only/ Free Software?
   - We only crawl origins /meant/ to host source code (e.g., forges)
   - Most (~90%) of what we /actually/ retrieve is textual content
   #+BEAMER: \vfill
*** Our goal
    Archive *the entire Free Software Commons*

  #+BEAMER: \vfill
*** 
   - Large parts of what we retrieve is /already/ Free Software, today
   - Most of the rest /will become/ Free Software in the long term
     - e.g., at copyright expiration
** Q: how about SHA1 collisions?
   #+BEAMER: \lstinputlisting[language=SQL,basicstyle=\small]{../../common/source/swh-content.sql}
