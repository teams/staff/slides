\documentclass[handout,xcolor=table]{beamer}
\usepackage{alltt}
\usepackage{wasysym}
\mode<presentation>
{
  \usetheme{irill}
  %\setbeamercovered{transparent}
  \beamertemplatenavigationsymbolsempty
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{headline}{} % suppresses navigation header
}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{pgfpages}
\usepackage{fancyvrb}
\usepackage{graphicx}
\usepackage{colortbl}
\usepackage{booktabs}
\usepackage{extblocks}
\usepackage{comment}
%\setbeameroption{show notes}
%\setbeameroption{show notes on second screen=right}

\long\def\ignore#1{\relax}

\title[Preserving Software\hspace{2em}\insertframenumber/\inserttotalframenumber]{Preserving Software}
\subtitle{Challenges and opportunities for reproductibility}
\author{Roberto Di Cosmo}
\institute[Irill/INRIA/UPD]{Journée Reproductibilité\\
  \url{roberto@dicosmo.org}\\
  \includegraphics[height=2cm]{irill}
%  \qquad
%  \includegraphics[height=1cm]{mancoosi.png}
%  \qquad
%  \includegraphics[height=2cm]{logo-debian.png}
}
\date{9 Décembre 2014}

\rowcolors[]{1}{blue!10}{blue!05} % \rowcolors is not in the colortbl manual

\AtBeginSection[]
{
\begin{frame}<beamer>
 \frametitle{Outline}
 \tableofcontents[currentsection]
\end{frame}
}

%
% make sure we get our pictures and images
%
\graphicspath{%
{pics/}{../images/}{../../images/}{../pics/}{../../pics/}{../figures/}{../../figures/}{../logos/}{../../logos/}%
{../../common/images/}{../../common/logos/}%
{pics/}{../images/}{../../images/}{../pics/}{../../pics/}%
{../figures/}{../../figures/}{../logos/}{../../logos/}{../../../logos/}%
{../../communication/web/graphics/carousel/}%
{../../communication/web/graphics/pictos/png/400x400/}%
}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

% \begin{frame}
%   \tableofcontents
% \end{frame}

\section{Reproductibility}

\begin{frame}{Reproducibility}

\begin{block}<+->{Reproducibility (Wikipedia)}
the ability of an entire experiment or study to be \emph{reproduced}, either by the researcher or \emph{by someone else working independently}.\\
It is one of the main principles of the scientific method.
\end{block}

\begin{block}<+->{Reproducibility in the digital age}
For an experiment involving software, we need\\
\mbox{}\hfill
\begin{minipage}[t]{.9\linewidth}
\begin{description}
 \item[open access] to the scientific article describing it
 \item[open data sets] used in the experiment
 \item[source code] of all the components
 \item[environment] of execution
 \item[stable references] between all this
\end{description}
\end{minipage}
\end{block}

\uncover<+->{\flushright The first two items are already widely discussed!}

\note{Concerning ``Replicability is not Reproducibility: Nor is it Good
  Science'' of Chris Drummond.\\
He claims correctly that the reproduction of the experiment is stronger
if done by changing seemingly irrelevant details.\\
He then falsely concludes that access to the source code used in experiment is of little of no value, because it would only allow to replicate the very same experimental setting.\\
In the framework of software, this is false for at least three reasons:
\begin{itemize}
 \item important details of the process used in the experiment, and the
       related knowledge are actually \emph{embedded} in the source code,
       without which one may be left wondering what actually was done in
       the original experiment; the source code is like the proof used in
       a theorem: I definitely want to see Fermat's ``source code'' for
       his famous theorem, and not just be left alone to try to reproduce
       the result from nothing!
       % marge des Arithmetiques de Diophante 1621
 \item the complexity of modern technology is such that even apprently
       simple experiments require direct or indirect access to dozens
       of software components, each of which can be configured in many
       ways; to set up a variant of the experiment, even from scratch,
       one needs to have access to the original code
 \item open source is mandatory: having access to all the source code is
       \emph{not just to reproduce}, it is also to \emph{evolve and modify},
       to \emph{build new experiments} from the old ones
\end{itemize}
}
\end{frame}

\begin{frame}{Why Open Source?}
Some people claim that having (all) the source of the code used in
an experiment is \emph{not worth the effort}
\footnote{``Replicability is not Reproducibility: Nor is it Good
  Science'', Chris Drummond, ICML 2009}.\\\pause

Sure, diversity \emph{is} important, but consider that:\\\pause

\begin{itemize}[<+->]
 \item % important knowledge is \emph{embedded} in the source code;\\
       source code is like the proof used in a theorem: can we really
       accept \emph{Fermat statements} like ``the details are omitted
       due to lack of space''?
 \item and even more so when the complexity of modern systems makes even
       the simplest experiment depend on a wealth of components and 
       configuration options?
 \item having access to all the source code is not just necessary 
       to \emph{reproduce}, it is also useful to \emph{evolve and modify},
       to \emph{build new experiments} from the old ones
\end{itemize}
\end{frame}

\section{Preservation}

\begin{frame}{Digital preservation}

\begin{block}<+->{Digital Preservation (Wikipedia)}
  In library and archival science, digital preservation is a \emph{formal
    endeavor} to ensure that digital information of continuing value
  \emph{remains accessible and usable}.
\end{block}

\begin{block}<+->{Digital Preservation for Reproducibility}
(Digital) preservation is the \emph{unstated assumption} underlying
reproducibility efforts for all scientific experiments:\\[1em]
\begin{center}
  we cannot reproduce an experiment\\ whose description has been lost!
\end{center}
\end{block}

\begin{block}<+->{What is a \emph{description}?}
In our modern world, this comprises \emph{articles}, \emph{data} and, yes, \emph{software}!\\
% \begin{description}
% \item[articles] describing the findings
% \item[software] implementing the experiment
% \item[data] used for experimental validation
% \end{description}
%\hfill \alert{And all of this is now \emph{digital}!}
\end{block}
\end{frame}

% \begin{frame}{PLAN}

%  preservation level 0 ... - just code availability
%  preservation level 1 ... - source code ...
%  preservation level 2 ... - toolchain ...
%  preservation level 3 ... - environment ...

%  no reprod without preservation

%  great picture of preservation with a triangle : open access, 
%  open data, open source w/ stable reference, and long term preservation

%
% challenges: do it properly
%
% opportunity: nobody did it yet, let's try to do it right :-)
%              taking into account all the needs we have
%

% \end{frame}

\begin{comment}
   \begin{frame}{Software is pervasive}
     \begin{block}<+->{Software is everywhere (and just wait for the IoT!)}
     \begin{description}
     \item[televisions/fridges] $\approx$ 10M SLOC % like a phone, for Smart TVs, http://www.proofpoint.com/about-us/press-releases/01162014.php
     \item[phones] $\approx$ 20M SLOC  % http://electric-cloud.com/blog/2013/02/evolution-of-the-android-open-source-project-aosp-code-size-and-relative-build-time/
     \item[cars] $\approx$ 100M SLOC    % http://www.jamasoftware.com/blog/rethink-agile-manifesto-software-everywhere/
     \item[razors] $\approx$ 10K SLOC % 
     \end{description}
     Not necessarily a good thing when your fridge turns into a spambot \url{http://www.proofpoint.com/about-us/press-releases/01162014.php}!
     \end{block}

     \begin{block}<+->{Investors noticed it}
     \begin{quote}
     “Software and computing had evolved to a point where software was getting powerful enough that it could literally take over other industries”\\
     \hfill Ben Horowitz, Jan 2013
     \end{quote}
     \end{block}
     \end{frame}
\end{comment}

\section{Software}

\begin{frame}{Software is Science's cornerstone}

\begin{block}{Software is \emph{an essential component} of modern scientific research}
Deep \emph{knowledge} from mathematics, physics, chemistry, biology, medicine,
finance and social sciences \emph{is now inextricably embodied into complex software systems}, 
at a level of detail \alert{that goes way beyond} that of the usual scientific publications.
\end{block}

\pause

\begin{picblock}[pic=papermountain]{Top 100 papers (Nature, October 2014)}
\begin{quote}
[...] \alert{ the vast majority describe} experimental methods or \alert{software that have become essential in their fields}.\\
\url{http://www.nature.com/news/the-top-100-papers-1.16224}
\end{quote}
\end{picblock}

\pause

\structure{Bottomline}: Software is \emph{Knowledge} that needs to be preserved!
\end{frame}

\begin{frame}{Like all digital information, software is \emph{fragile}}

\begin{block}<+->{Causes of information loss}
\begin{description}
  \item[Human]
    accidental or malicious deletion, ... %failure to test recovery processes

  \item[Storage Media, Practices, Systems]
    disaster events, corruption, damage, wear and tear, aging ...

  \item[Logical Format or Migration]
    Inability to Access, Read, Interpret, Validate, or Use information\\
    Loss of the necessary \alert{software tools} \\
    Loss or damage to \alert{references} to associated information

   \item[Encryption] Lost access keys, decryption devices 

   \item[Authenticity]
       Failure to identify the \alert{intended} versions

   \item[External Service Providers]
    Out of business, high exit cost, ...
\end{description}
\end{block}

\begin{block}<+->{}
\flushright An example is worth a thousand words...
\end{block}
\end{frame}

\begin{frame}{Inconsiderate loss of code: Y2K}
\vfill
\begin{picblock}[pic=y2k,leftpic]{Y2K : The Year 2000 Bug Crisis}
The announced disasters did not occurr, and we'll never know if it's
because of the billions spent on fixing the bug, but ...
% Mads Tofte rich
\end{picblock}
\vfill\pause
\begin{block}{An Inconvenient Truth}
  ... this bug uncovered the astonishing fact that, in 1999, an estimated 40\% of
  companies had either \emph{lost}, or \alert{thrown away} the original source
  code for their systems!
\end{block}
\end{frame}

\begin{frame}{Malicious loss of code: R.I.P. CodeSpaces}
\begin{center}
   \includegraphics[width=.9\linewidth]{%
     codespaces-murdered}
\end{center}
\pause
\begin{alertblock}{}
Yes, for \emph{seven years} all seemed good and well!\\
\pause \flushright No, they did not recover the data.
\end{alertblock}
\end{frame}

\begin{frame}{Business-driven loss of code support: Google}
\begin{center}
   \includegraphics[width=.99\linewidth]{%
      google-code-stops-download-annotated}
\end{center}
\end{frame}

\begin{frame}{Disruption of the web of reference: our Gforge}
\begin{center}
   \includegraphics[width=.7\linewidth]{%
      gforge-changed-url}
\end{center}
\pause
\uncover<+->{\itshape\flushright Fixed, adding a redirection, by the Gforge team in {\bf 1} day!\\ Not always that lucky, though ...}
\end{frame}

\begin{frame}{Disruption of the web of reference: an old problem}

  \begin{block}{URLs used in articles \emph{do decay}!}
  Analysis of IEEE Computer (Computer), and the Communications of the ACM (CACM): 1995-1999\\
  \itshape 
   \begin{itemize}
     \item the half-life of a referenced URL is approximately 4 years from its publication date
     \item deep path hierarchies are linked to increased URL failures
    \end{itemize}
     \hfill D. Spinellis. The Decay and Failures of URL References.\\
     \hfill Communications of the ACM, 46(1):71-77, January 2003.\\
  \end{block}

  Similar findings in\\
  \emph{Lawrence, S. et al. Persistence of Web References in Scientific Research, IEEE Computer, 34(2), pp. 26–31, 2001.}
\end{frame}


\section{Preservation Initiatives}

\begin{frame}{Preservation of digital information is on the rise}
\begin{block}<+->{A wealth of initiatives around us}
\begin{description}
   \item[generalist] the Web archive at \url{archive.org}; Digital Preservation Coalition (UK); National Digital Information Infrastructure and Preservation Program (NDIIPP, USA); ...
   \item[culture] books, music, video: \url{http://www.nationalarchives.gov.uk} (UK); INA (FR); ...
   \item[social networks] Twitter is archived by the Library of Congress!
   \item[libraries and scholarly work] ArXiv;  Digital Preservation Network \url{http://www.dpn.org/}; ...
   \item[scientific data] CINES (FR); Zenodo/OpenAire (CERN); ...
\end{description}
\end{block}

\begin{alertblock}<+->{}
\begin{center}
What about the software?
\end{center}
\end{alertblock}
\end{frame}

\section{Software Preservation: Challenges}

\begin{frame}{Software preservation challenges: awareness}
  
 \begin{block}<+->{Software is the mediator for our digital culture}
 \begin{quotation}
 Absent an ability to correctly interpret digital information, we are left
 with files full of \alert{``rotting bits''} that are of no value.
  \end{quotation}
{\small
  \begin{thebibliography}{Foo Bar, 1969}
  \bibitem{cerf-2011-bitrot} Vinton G.~Cerf \newblock Avoiding ``bit rot'':
    Long-term preservation of digital information.  \newblock {\em Proceedings
      of the IEEE}, 99(6):915--916, 2011.
  \end{thebibliography}
}
  \end{block}

  \begin{block}<+->{}
     \structure{If we do not preserve software,} \alert{digital preservation is futile}!
  \end{block}

 \begin{block}<+->{And yet, up to now...}
 \structure{software} is \emph{largely ignored} as an \alert{object} of preservation...\\
 \structure{computer scientists} are \alert{mostly absent} in the preservation landscape!
 \end{block}

\end{frame}

\begin{frame}{Software Preservation Challenges: it's \emph{different}!}

Unlike for books or movies, there is a big difference between \emph{using}
and \emph{understanding} a piece of software.\\

\begin{block}<+->{Using software} % 
Requires an executable, and access to the \emph{execution environment}
\end{block}

\begin{block}<+->{Understanding software} % Use the source, Luke!
Requires access to the \emph{source code}:
      \begin{quotation}
        The \emph{source code} for a work means the preferred form of the work
        for making modifications to it.  
        % For an executable work, complete
        % source code means all the source code for all modules it contains, plus
        % any associated interface definition files, plus the scripts used to
        % control compilation and installation of the executable.
        \\
        \mbox{~}\hfill --- GNU General Public Licence, version 2
      \end{quotation}
\end{block}

\begin{alertblock}<+->{}
\begin{center}
For reproducibility, we need \emph{both}!
\end{center}
\end{alertblock}
\end{frame}

\begin{frame}{Software Preservation Challenges: it's \emph{different}!}
Preserving \emph{software} is \alert{more complex} than archiving books or scientific articles.

\begin{description}[<+->]
\item[interdependencies] a %software 
  program relies on other software
  (libraries, compilers, development tools, runtime systems, etc.) as well as
  specific hardware components (or equivalent virtual machines) to be executed;
  so does our understanding of its functioning.

\item[evolution] software is a \emph{live object}: its detailed history
  contains key knowledge that cannot be reconstructed
  by only looking at an individual snapshot of the software source code.
\end{description}

\begin{alertblock}<+->{}
  To preserve \emph{software} it is \emph{not enough} to mimic processes that were intended to archive books,
  scientific articles or data.
\end{alertblock}
\end{frame}

\section{Software Preservation: Opportunities}

\begin{frame}{Software Preservation: a Unique Opportunity}
\begin{block}<+->{The \emph{half empty glass} point of view}
Nobody cares about software...\\
Computer scientists are absent from the preservation landscape...\\
\flushright Nobody loves us...
\end{block}

\begin{block}<+->{The \emph{half full glass} point of view}
Preserving software is a highly challenging task...\\
... it requires Computer Scientists in the loop\\[1em]
\flushright Luckily nobody cared!
\end{block}

\begin{block}<+->{}
\begin{center}
Let's do it \emph{right}, ... let's do it \emph{now}!
\end{center}
\end{block}
\end{frame}

\begin{frame}{The Knowledge Conservancy Magic Triangle}

\begin{center}
\includegraphics[width=.7\linewidth]{PreservationTriangle}
\end{center}

\begin{block}{}
  \begin{description}
    \item[articles] ArXiv, HAL?, ...
    \item[data] Zenodo? (OpenAire)
    \item[software] coming soon from Inria!
  \end{description}
\end{block}
\end{frame}

\begin{frame}{Bits from the drawing board}
\begin{block}<+->{Replication is the key}
\itshape
  ...let us save what remains: not by vaults and locks which fence them
  from the public eye and use in consigning them to the waste of time,
  but by such a multiplication of copies, as shall place them beyond
  the reach of accident.\\

  \hfill Thomas Jefferson, February 18, 1791
\end{block}

\begin{alertblock}<+->{recommendation}
  our preferred platform(s) should:
  \begin{itemize}
    \item provide easy means for making copies
    \item encourage the growth of a mirror network (like ArXiv did)
  \end{itemize}
\end{alertblock}
\end{frame}

\begin{frame}{Bits from the drawing board}
\begin{block}<+->{Free an Open Source Software is crucial}
\itshape
you have to do [digital preservation] with open-source software; closed-source
preservation has the same fatal "just trust me" aspect that closed-source
encryption (and cloud storage) suffer from.\\
% http://blog.dshr.org/2014/09/plenary-talk-at-3rd-eudat-conference.html
\hfill D. Rosenthal, EUDAT, 9/2014 % http://www.eudat.eu/3rd-eudat-conference
\end{block}

\begin{alertblock}<+->{recommendation}
  our preferred platform(s) should:
  \begin{itemize}
    \item provide full details on their architecture
    \item make available all the source code used
    \item use open standards
    \item encourage a collaborative development process
  \end{itemize}
\flushright Unfortunately, this is not (yet?) the case for HAL or Zenodo
\end{alertblock}
\end{frame}

\begin{frame}{Bits from the drawing board}
\begin{block}<+->{Web links \emph{are not} permanent (even \emph{permalinks})}
\itshape
   Users should beware that there is no general guarantee that a URL
   which at one time points to a given object continues to do so, 
   and does not even at some later time point to a different object
   due to the movement of objects on servers.\\
   \hfill T. Berners-Lee et al. Uniform Resource Locators. RFC 1738.
  %
  % half life of web references is 4 years
  %
  % \htill Diomidis Spinellis. The Decay and Failures of URL References. Communications of the ACM, 46(1):71-77, January 2003. 
  % The general problem of persistence and disappearance requires a combination of
  %   technical solutions and peer policies. Ideally, all cited materials—but
  %   especially those important to building on or verifying research— would be
  %   available from a stable site mirrored world-wide such as the Netlib
  %   Repository (http://www.netlib.org/).\\
  %   \hfill Lawrence, S. et al. Persistence of Web References in Scientific Research, IEEE Computer, Volume 34, Number 2, pp. 26–31, 2001.
  % 
\end{block}

\begin{alertblock}<+->{recommendation}
  our preferred platform(s) should:
  \begin{itemize}
    \item provide \emph{intrinsic} resource identifiers
    \item \emph{avoid} intermediate index approaches like DOI
  \end{itemize}
\end{alertblock}
\end{frame}

\begin{frame}{Conclusions}
  \begin{itemize}
    \item \structure{Long term preservation} is the \emph{unspoken assumption} of all scientific reproductibility efforts
    \item We need to preserve scientific articles, scientific data \emph{and} software (\structure{magic triangle})
    \item \structure{Digital preservation} is on the rise, mostly thanks to librarians, with almost no computer scientists involved
    \item \structure{Software preservation} is \alert{not yet there} and is in dire need of attention
    \item We have a great opportunity to seize... let's do it!
  \end{itemize}    
\end{frame}

\end{document}

\begin{frame}{Sample blocks}

  \begin{picblock}[pic=rdc]{Block with figure on the right}
      lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet
  \end{picblock}

  \begin{picblock}[pic=rdc,leftpic]{Block with figure on the left}
  \begin{center}
  \begin{tabular}{l!{\vrule}cccc} % what's the idea behind !\vrule here, can we avoid it?
  Class & A & B & C & D \\\midrule % \midrule instead of the old \hline
  X & 1 & 2 & 3 & 4 \pause\\
  Y & 3 & 4 & 5 & 6 \pause\\
  Z & 5 & 6 & 7 & 8 \\ % double backslash was added as it was not there in the manual
  \end{tabular}
  \end{center}
  \end{picblock}
\end{frame}


\end{document}

Y2K--The Year 2000 Bug Crisis

 it is estimated that 40\% of companies have either lost, or thrown away the original source code for their systems
