Title: Archiving, assessing and attributing research software: towards
   software as a first class citizen in the scholarly world
   
   Abstract:

   Software is a fundamental pillar of modern scientific research, across all
   fields and disciplines. However, there is a general lack of adequate means to
   archive, reference and cite software.  In this talk, we will survey the main
   issues that make this task difficult, ranging from the specificity of the
   persistent identifiers needed for reproducibility to the complexity of
   determining software authorship and authority, especially for long running
   projects, which are needed for proper software attribution and credit.  We
   report on recent contributions to the ongoing efforts to develop proper
   processes, guidelines and recommendations for software reference and
   software citation, building upon the internal experience of Inria and the
   emerging Software Heritage infrastructure.
