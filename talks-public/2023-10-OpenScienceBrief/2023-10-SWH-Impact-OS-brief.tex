\documentclass[a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{qrcode}
\usepackage{graphicx}
\usepackage{geometry}
\usepackage{float}
\usepackage{layout}
\usepackage[absolute,overlay]{textpos}

\graphicspath{%
{pics/}{images/}{./}%
}

\geometry{margin=0.7in}
\marginparwidth 0pt
\marginparsep 0pt
\textwidth 520pt

\begin{document}
\begin{textblock*}{4cm}(1cm,1cm) % {block width} (coords) 
    \includegraphics[width=4cm]{software-heritage-logo-title.512px}
\end{textblock*}

\begin{textblock*}{4cm}(\dimexpr\paperwidth-5cm\relax,1cm) 
    \includegraphics[width=4cm]{inria-logo-new}
\end{textblock*}

\begin{textblock*}{3cm}(1cm,2.5cm)
    \includegraphics[width=3cm]{2022-report-swh}
\end{textblock*}

\begin{textblock*}{2cm}(\dimexpr\paperwidth-4cm\relax,3.5cm) 
    \qrcode{https://www.softwareheritage.org/wp-content/uploads/2023/02/SoftwareHeritageReport_2022.pdf}
\end{textblock*}


\title{Software Heritage pour la Science Ouverte}
\author{Roberto Di Cosmo\\
  director, Software Heritage\\
  \href{https://dicosmo.org}{https://dicosmo.org}\hspace{3em}\href{mailto:roberto@dicosmo.org}{roberto@dicosmo.org}}
\date{October 2023}
\maketitle
% \layout
\begin{minipage}{0.9\textwidth}
  Le logiciel est un pilier de la Science Ouverte. Software Heritage a ouvré depuis 2016 pour fournir l'infrastructure de référence pour les codes sources issue de la recherche, et cela à tous les niveaux. On présente ici une sélection des actions et de leurs impact au niveau français, européen et international.
\end{minipage}

\section{France}
\subsection{Intégration avec HAL (2016-\ldots)}
    \begin{minipage}{0.75\textwidth}
        Intégration de Software Heritage avec HAL pour permettre le \href{https://doc.archives-ouvertes.fr/deposer/deposer-le-code-source/}{dépôt de codes sources de la recherche}. Collaboration commencée avec le CCSD en 2016, et intensifiée depuis, avec un deploiement national basé sur le \href{https://swhid.org}{Software Hash Identifier (SWHID)}, le format de metadonnées CodeMeta, et le \href{https://ctan.org/pkg/biblatex-software}{style BibLaTeX \texttt{biblatex-software}}.
    \end{minipage}
    \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://doc.archives-ouvertes.fr/deposer/deposer-le-code-source/}
    \end{minipage}

\subsection{Plan National pour la Science Ouverte (2021-2024)}
    \begin{minipage}{0.2\textwidth}
        \includegraphics[width=.8\textwidth]{PNSO2-cover}
    \end{minipage}
    \hfill
    \begin{minipage}{0.5\textwidth}
        Identification de l'archivage et le référencement des codes sources de la recherche comme un élément essentiel du pilier logiciel de la Science Ouverte. La         \href{https://www.enseignementsup-recherche.gouv.fr/fr/le-plan-national-pour-la-science-ouverte-2021-2024-vers-une-generalisation-de-la-science-ouverte-en-48525}{plan national pour la Science Ouverte} s'appuie sur Software Heritage pour repondre à ces besoins au niveau de la France entière.
    \end{minipage}
        \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://www.enseignementsup-recherche.gouv.fr/fr/le-plan-national-pour-la-science-ouverte-2021-2024-vers-une-generalisation-de-la-science-ouverte-en-48525}
    \end{minipage}

\subsection{Recommandation ANR (2023-\ldots)}
    \begin{minipage}{0.75\textwidth}
      L'Agence Nationale pour la Recherche (ANR), inclut désormais une recommandation d'archivage des codes sources dévéloppés avec son financement, en utilisant Software Heritage (voir \href{https://anr.fr/fileadmin/aap/2023/aapg-2023-V2.0_MAJ20220921.pdf}{page 17 du guide des appels à projet}).
    \end{minipage}
    \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://anr.fr/fileadmin/aap/2023/aapg-2023-V2.0_MAJ20220921.pdf}
    \end{minipage}

\subsection{Stratégie Nationale des Infrastructures de Recherche (2021-\ldots)}
    \begin{minipage}{0.2\textwidth}
        \includegraphics[width=.8\textwidth]{french-ri-roadmap-2021}
    \end{minipage}
    \hfill
    \begin{minipage}{0.5\textwidth}
      Inscription de Software Heritage dans \href{https://www.enseignementsup-recherche.gouv.fr/fr/la-strategie-nationale-des-infrastructures-de-recherche-46112}{la feuille de route nationale des infrastructures de recherche}, publiée en
      Mars 2022. Il s'agit de la première infrastructure au service du logiciel à apparaître dans ce type de document:
      le logiciel n'est plus juste un outil au service d'infrastructures de recherche.
    \end{minipage}
        \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://www.enseignementsup-recherche.gouv.fr/fr/la-strategie-nationale-des-infrastructures-de-recherche-46112}
    \end{minipage}

\clearpage
\section{Niveau Européen}

\subsection{Rapport EOSC SIRS (2020)}
    \begin{minipage}{0.2\textwidth}
        \includegraphics[width=.7\textwidth]{EOSC-SIRS-report}
    \end{minipage}
    \hfill
    \begin{minipage}{0.5\textwidth}
      Publication en 2020 du
      \href{https://data.europa.eu/doi/10.2777/28598}{rapport EOSC SIRS}
      (Scholarly Infrastructures for Research Software), qui identifie en
      Software Heritage l'infrastructure mutualisée sur laquelle les depôts
      institutionnels, les éditeurs et les aggregateurs doivent s'appuyer pour
      l'archivage et le réferencement du logiciel de recherche.
    \end{minipage}
        \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://data.europa.eu/doi/10.2777/28598}
    \end{minipage}

\subsection{Projet FAIRCORE4EOSC (2022-2025)}
    \begin{minipage}{0.25\textwidth}
        \includegraphics[width=\textwidth]{fc4e-rsac}
    \end{minipage}
    \hfill
    \begin{minipage}{0.5\textwidth}
      Le projet Européen FAIRCORE4EOSC, financé à hauteur de 9 millions d'euros,
      contient un workpackage spécifiquement dédié à l'implémentation des
      recommendations du rapport EOSC SIR, par le
      \href{https://faircore4eosc.eu/eosc-core-components/eosc-research-software-apis-and-connectors-rsac}{dévéloppement
        de connecteurs et API spécifiques}
    \end{minipage}
        \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://faircore4eosc.eu/eosc-core-components/eosc-research-software-apis-and-connectors-rsac}
    \end{minipage}

\subsection{Mirrors (2024-\ldots)}
    \begin{minipage}{0.2\textwidth}
        \includegraphics[width=\textwidth]{fc4e-swhm}
    \end{minipage}
    \hfill
    \begin{minipage}{0.55\textwidth}
      Dans le cadre du projet Européen FAIRCORE4EOSC, on travaille à la construction d'un miroir complet de l'archive de Software Heritage au service de la science ouverte en Europe. Une fois mis en place, le miroir sera maintenu par GRNET, l'infrastructure qui centralise la gestion du réseau et du calcul pour tout l'ecosystème académique grec.
        \href{https://faircore4eosc.eu/eosc-core-components/eosc-software-heritage-mirror}{Lien vers les mirrors}
    \end{minipage}
        \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://faircore4eosc.eu/eosc-core-components/eosc-software-heritage-mirror}
    \end{minipage}

\section{Niveau International}

\subsection{Appel de Paris sur le Code Source des Logiciels (2018, UNESCO)}
    \begin{minipage}{0.15\textwidth}
        \includegraphics[width=\textwidth]{paris_call_ssc_cover}
    \end{minipage}
    \hfill
    \begin{minipage}{0.55\textwidth}
      Issu d'un groupe d'experts internationaux réuni à l'invitation de l'UNESCO
      et d'Inria , on y retrouve ce passage: ``promote software development as a
      \emph{valuable research activity}, and research software as a key enabler
      for Open Science/Open Research, [\ldots{}] recognising in the careers of
      academics their contributions to \emph{high quality software development},
      in all their forms''.
    \end{minipage}
        \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://en.unesco.org/foss/paris-call-software-source-code}
    \end{minipage}

\subsection{Normalisation des Identifiants SWHID (2023-\ldots)}
    \begin{minipage}{0.3\textwidth}
        \includegraphics[width=\textwidth]{SWHID-v1.4_3}
    \end{minipage}
    \hfill
    \begin{minipage}{0.45\textwidth}
      Software Heritage utilise un identifiant intrinséque cryptographique, le SWHID, pour les 30+ milliards d'artifacts archivés. Cet identifiant est maintenant appelé ``Software Hash Identifier'' et fait l'objet d'une \href{https://swhid.org}{spécification publique qui va déboucher vers une soumission pour normalisation ISO}.
    \end{minipage}
    \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://swhid.org}
    \end{minipage}

    \subsection{Contribution au Schéma de Métadonnées Logicielles \emph{Codemeta} (2020-\ldots)}
    \begin{minipage}{0.2\textwidth}
        \includegraphics[width=\textwidth]{codemetaproject}
    \end{minipage}
    \hfill
    \begin{minipage}{0.55\textwidth}
        Software Heritage collecte des métadonnées sur les logiciels archivés.
        Il a identifié \href{https://github.com/codemeta/codemeta}{CodeMeta}
        comme schéma de metadonnées de référence, et le fichier
        \texttt{codemeta.json} comme format pivot, machine readable. Software
        Heritage joue un rôle clé dans l'évolution et la maintenance de ce
        format, visant son intégration dans \texttt{schema.org}
    \end{minipage}
    \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://github.com/codemeta/codemeta}
    \end{minipage}

\end{document}
