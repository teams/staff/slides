Title: Preserving source code in Software Heritage: a foundation for reproducibility

Abstract:

Software is, together with articles and data, a key ingredient of academic
research, in all fields, hence preservation of, and access to the source code of
computer programs is a pillar of Open Science, and a necessary step to
understand, verify, adapt and reuse the tools that we produce and use in the
scholarly world.

We have at our hand a very large choice of platforms and repositories to develop
collaboratively and share source code, but they fall short when it comes to
support reproducibility in the long term.

In this talk, we will present Software Heritage, a universal long term archive
specifically designed for software source code that provides intrinsic
identifiers for all the artifacts it stores, at all granularitiess, down to the
level of the line of code.

Though live demos, we will see how Software Heritage provides a foundation for
reproducibility, and how easy it is to leverage it in academic work.
