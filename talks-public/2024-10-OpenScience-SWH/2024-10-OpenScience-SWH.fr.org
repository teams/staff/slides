#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Software Heritage
#+SUBTITLE: défis et opportunités
# #+AUTHOR: Roberto Di Cosmo
# #+EMAIL: roberto@dicosmo.org @rdicosmo @swheritage
#+BEAMER_HEADER: \date[10/2024]{Octobre 2024}
#+BEAMER_HEADER: \title[Software Pillar of Open Science]{Logiciel, pilier de la science ouverte}
#+BEAMER_HEADER: \author[R. Di Cosmo~~~~ roberto@dicosmo.org ~~~~ (CC-BY 4.0)]{Roberto Di Cosmo}
#+BEAMER_HEADER: \institute[Software Heritage]{Director, Software Heritage\\Inria and Universit\'e de Paris Cit\'e}
# #+BEAMER_HEADER: \setbeameroption{show notes on second screen}
#+BEAMER_HEADER: \setbeameroption{hide notes}
#+KEYWORDS: software heritage legacy preservation knowledge mankind technology
#+LATEX_HEADER: \usepackage{tcolorbox}\usepackage{fontawesome5}
#+LATEX_HEADER: \definecolor{links}{HTML}{2A1B81}
#+LATEX_HEADER: \definecolor{links}{HTML}{0ADB11}
#+LATEX_HEADER: \hypersetup{colorlinks,linkcolor=,urlcolor=links}
#+LATEX_HEADER: \hypersetup{colorlinks,linkcolor=,urlcolor=cyan}
#+LATEX_HEADER: \usepackage{pax}
#+LATEX_HEADER: \usepackage{pdfpages}

#
# prelude.org contains all the information needed to export the main beamer latex source
# use prelude-toc.org to get the table of contents
#

#+INCLUDE: "../../common/modules/prelude-toc.org" :minlevel 1
#+INCLUDE: "../../common/modules/169.org"
#+INCLUDE: "../../common/modules/highcontrast.org"

* Introduction
** Courte biographie : Roberto Di Cosmo
Professeur d'informatique à Paris, actuellement à INRIA\\
- /35+ ans/ de recherche (Informatique théorique, Programmation, Ingénierie logicielle, Numéro d'Erdos : 3)\\
- /25+ ans/ de Logiciel Libre et Open Source\\
- /15+ ans/ à construire et diriger des structures pour le bien commun\\

\mbox{}\\
   \begin{minipage}[c]{0.18\linewidth}
     \includegraphics[width=1.0\linewidth]{rdc}
   \end{minipage}
   \begin{minipage}[c]{0.8\linewidth}
     \begin{description}
%       \item[1998] \emph{Cybersnare} -- voix du Logiciel Libre français
       \item[1999] \emph{DemoLinux} -- première distribution GNU/Linux live
%       \item[2004] \emph{EDOS} -- vérification des dépendances de paquets
       \item[2007] \emph{Groupe thématique Logiciel Libre}\\
                   %\tiny{\url{http://www.systematic-paris-region.org/fr/logiciel-libre}}\\
                   ~150 membres ~40 projets ~200Me
       \item[2008] \emph{Projet Mancoosi} \url{www.mancoosi.org}
       \item[2010] \emph{IRILL} \url{www.irill.org}
       \item[2015] \emph{Software Heritage} à INRIA
       \item[2018] \emph{Comité National pour la Science Ouverte}, France
       \item[2021] \emph{EOSC Task Force sur les Infrastructures pour le Logiciel}, Union Européenne
     \end{description}
   \end{minipage}
* Logiciel et Code Source
** Pourquoi la Science Ouverte ?                                :skipinshort:
#+beamer: \vspace{-.5em}
*** Science Ouverte ([[https://www.ouvrirlascience.fr/wp-content/uploads/2021/10/Second_French_Plan-for-Open-Science_web.pdf][Second Plan National pour la Science Ouverte]], France, 2021)
/Diffusion/ sans entrave des résultats, méthodes et produits de la recherche scientifique.\\
Elle repose sur /l'opportunité offerte par les récents progrès numériques/
pour développer l'/accès ouvert/ aux /publications/ et – autant que possible – aux /données/, au /code source/ et aux /méthodes de recherche/.
#+beamer: \pause
#+beamer: \vspace{-.3em}
*** Jean-Eric Paquet (UE DGRI, [[https://www.eosc.eu/sites/default/files/EOSC-SRIA-V1.0_15Feb2021.pdf][sur l'objectif de la Science Ouverte]])
“Améliorer la /qualité scientifique/, le /rythme des découvertes et du développement technologique/, ainsi que la /confiance de la société dans la science/.”
#+beamer: \pause
#+beamer: \vspace{-.1em}
*** Mariya Gabriel ([[https://www.s4d4c.eu/insights-from-commissioner-mariya-gabriel-towards-the-european-union-science-diplomacy/][Commissaire Européenne]] pour la Recherche)
La crise du COVID-19 a également montré que la coopération au niveau international en matière de recherche et d'innovation est plus importante que jamais, notamment par l’/accès ouvert aux données et aux résultats/. /Aucune nation, aucun pays ne peut affronter seul ces défis mondiaux/.
#+beamer: \pause
#+beamer: \vspace{-.3em}
*** Yuval Noah Harari (sur le COVID 19)
\hfill /“Le véritable antidote [à l'épidémie] est/ la connaissance scientifique /et/ la coopération mondiale.”
** Le logiciel /code source/ est un savoir précieux
*** Harold Abelson, Structure and Interpretation of Computer Programs (1ère éd.) \hfill 1985
/“Les programmes doivent être écrits pour être lus par des humains, et seulement accessoirement pour être exécutés par des machines.”/
#+beamer: \pause
*** Code source d’Apollo 11 ([[https://archive.softwareheritage.org/swh:1:cnt:64582b78792cd6c2d67d35da5a11bb80886a6409;origin=https://github.com/virtualagc/virtualagc;visit=swh:1:snp:3c074afad81ad6b14d434b96e705e01d184752cf;anchor=swh:1:rev:007c2b95f301f9438b8b74d7993b7a3b9a66255b;path=/Luminary099/THE_LUNAR_LANDING.agc;lines=245-261/][extrait]])
    :PROPERTIES:
    :BEAMER_col: 0.48
    :BEAMER_env: block
    :END:
#+latex: \includegraphics[width=\linewidth]{apollo-11-cranksilly.png}

#+beamer: \pause
*** Code source de Quake III ( [[https://archive.softwareheritage.org/swh:1:cnt:bb0faf6919fc60636b2696f32ec9b3c2adb247fe;origin=https://github.com/id-Software/Quake-III-Arena;visit=swh:1:snp:4ab9bcef131aaf449a7c01370aff8c91dcecbf5f;anchor=swh:1:rev:dbe4ddb10315479fc00086f08e25d968b4b43c49;path=/code/game/q_math.c;lines=549-572/][extrait]] )
    :PROPERTIES:
    :BEAMER_col: 0.45
    :BEAMER_env: block
    :END:
#+latex: \includegraphics[width=\linewidth]{quake-carmack-sqrt-1.png}

#+beamer: \pause
*** 							    :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** Len Shustek, Musée de l'Histoire de l'Informatique \hfill 2006
\hfill /“Le code source donne un aperçu de l'esprit du concepteur.”/
** Le logiciel est un pilier de la Science Ouverte
#+beamer: \vspace{-.5em}
*** Le logiciel alimente la recherche moderne
    :PROPERTIES:
    :BEAMER_opt: pic=french_os_monitor-use-2024, leftpic=true, width=.8\linewidth
    :BEAMER_env: picblock
    :BEAMER_COL: .65
    :END:
#+begin_quote
\scriptsize Plus de 20% des articles utilisant des logiciels /toutes disciplines confondues/ les partagent\\
\mbox{}\hfill [[https://frenchopensciencemonitor.esr.gouv.fr/software/fields?id=disciplines.partage][2024 Observatoire Français de la Science Ouverte]]
\vspace{.5em}
#+end_quote

#+beamer: \pause
*** Pilier clé : logiciel
    :PROPERTIES:
    :BEAMER_COL: .36
    :BEAMER_env: block
    :END:
#+latex: \begin{center}
file:../../common/modules/preservation_triangle_color.png
#+latex: \end{center}
#+beamer: \pause
\hfill Les liens sont *importants*
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+beamer: \pause
*** Nota Bene
le logiciel peut être un /outil/, un /résultat de recherche/ et un /objet de recherche/\pause\\
\hfill l'accès au /code source/ est essentiel !
#+beamer: \pause
*** 
\hfill Préserver (l'historique du) code source est nécessaire pour la /reproductibilité/
** Le code source est /spécial/ (le logiciel n'est /pas/ une donnée)
  # Was: #+INCLUDE: "../../common/modules/swh-ardc.org::#swnotdata" :only-contents t :minlevel 3
*** Le logiciel /évolue/ au fil du temps
- les projets peuvent durer des décennies
- l'/historique de développement/ est la clé de sa /compréhension/
#+beamer: \pause
*** Complexité                                                   :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=python3-matplotlib.pdf, width=.6\linewidth
    :END:
- /millions/ de lignes de code
- large /toile de dépendances/
  - facile à casser, difficile à maintenir
  - /logiciel de recherche/ une fine couche supérieure
- communautés de développeurs /sophistiquées/
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEAMER: \pause
*** Le côté humain
conception, algorithme, code, test, documentation, communauté, financement\\
\hfill et tant d'autres facettes ...
** Comment gérons-nous notre logiciel ?                         :skipinshort:
*** Reproductibilité, maintenance dans le milieu académique
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    #+ATTR_LATEX: :width .9\linewidth
    file:rsissues.pdf
    (articles : [[https://www.nature.com/articles/s41597-022-01143-6][ici]], [[https://ieeexplore.ieee.org/document/8816763][ici]], [[https://www.quantamagazine.org/crucial-computer-program-for-particle-physics-at-risk-of-obsolescence-20221201/][ici]] et [[http://reproducibility.cs.arizona.edu/tr.pdf][ici]])
    #+beamer: \pause
*** Sécurité, intégrité, traçabilité dans l'industrie
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[width=.85\linewidth]{supplychainwoes}
\end{center}
#+END_EXPORT
Peuvent-ils suivre le logiciel qu'ils
- expédient, utilisent, acquièrent
- présente ce bug ou cette vulnérabilité
#+beamer: \pause
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** 
\hfill la sensibilisation monte au niveau des politiques publiques
** Points forts internationaux                                  :skipinshort:
*** espace
\vspace{-0.5em}    
*** [[https://en.unesco.org/foss/paris-call-software-source-code][Appel de Paris sur le Code Source]] (2019, UNESCO)
     :PROPERTIES:
     :BEAMER_env: picblock
     :BEAMER_OPT: pic=paris_call_ssc_cover.jpg, leftpic=true, width=.2\textwidth
     :END:
40 experts internationaux appellent à]] 
#+latex: {\it
“promouvoir le développement logiciel comme une \emph{activité de recherche précieuse},
et les logiciels de recherche comme un élément clé pour la Science Ouverte/Recherche Ouverte,
[...] en reconnaissant dans les carrières des universitaires leurs
contributions au \emph{développement logiciel de haute qualité}, sous toutes ses formes”
#+latex: }\\[0.5em]
\mbox{}\hfill \HandRight \hfill Open Source à l'UNESCO [[https://unesdoc.unesco.org/ark:/48223/pf0000378381.locale=en][recommandations]] pour la Science Ouverte, 2021
*** spacepause
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
\vspace{-0.5em}\pause    
*** Le Logiciel dans l'EOSC                                      :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
     :BEAMER_OPT: pic=eosc-sirs-architecture-swh.png, leftpic=true, width=.3\textwidth
    :END:
2020 [[https://data.europa.eu/doi/10.2777/28598][EOSC SIRS]] connecte l'écosystème scientifique via Software Heritage\\
2021 [[https://www.eosc.eu/advisory-groups/infrastructures-quality-research-software][EOSC Task Force]] sur les Infrastructures pour le Logiciel de Recherche\\
2022 [[https://faircore4eosc.eu/][Projet FAIRCORE4EOSC]] WP6 implémente le rapport SIRS\\
*** spacepause
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
\vspace{-0.5em}\pause    
*** Et bien plus encore
Piste logiciel dans [[https://osec2022.eu/program/][OSEC 2022]], groupe de travail sur le logiciel lancé à Science Europe,\\
DFG ajoute le logiciel [[https://www.dfg.de/en/research_funding/announcements_proposals/2022/info_wissenschaft_22_61/index.html][au modèle de CV (9/22)]], NASA dévoile [[https://science.nasa.gov/researchers/science-data/science-information-policy][politique de Science Ouverte (12/22)]], ...
** Ce qui est en jeu
*** ARDC
    :PROPERTIES:
    :BEAMER_COL: .33
    :BEAMER_env: block
    :END:
- *Archiver* pour récupération (/reproductibilité/)
- *Référencer* pour identification (/reproductibilité/)
- *Décrire* pour découverte et réutilisation
- *Citer/Créditer* pour crédit et évaluation
  #+beamer: \pause
*** Avant ARDC
    :PROPERTIES:
    :BEAMER_COL: .33
    :BEAMER_env: block
    :END:
- *Pratiques de développement* et outils (VCS, système de construction, suites de tests, CI, qualité du code, ...)
- *Ouverture* vers une communauté (documentation, organisation, communication)
Besoin de formation, d'outils, d'infrastructures, de bonnes pratiques
#+beamer: \pause
*** Au-delà de l'ARDC
    :PROPERTIES:
    :BEAMER_COL: .33
    :BEAMER_env: block
    :END:
- *Politiques* (diffusion, réutilisation, carrières, ...)
- *Durabilité* (juridique, financier, etc.)
- Transfert technologique
- Technologies et outils avancés (qualité, traçabilité, etc.)
*** 
\hfill un défi humble et complexe (nous ne sommes pas dans un vide)
* La France montre la voie
** Plan national pour la science ouverte, 2021-2024
*** 
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_COL: .5
    :END:
#+beamer: \includegraphics[page=1,width=\linewidth]{PNSO2-sw.pdf}
#+beamer: \includegraphics[page=2,width=\linewidth]{PNSO2-sw.pdf}
#+beamer: \pause
*** 
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_COL: .5
    :END:
#+beamer: \includegraphics[page=3,width=\linewidth]{PNSO2-sw.pdf}
#+beamer: \includegraphics[page=4,width=\linewidth]{PNSO2-sw.pdf}
** Collège Logiciel dans le CoSO
*** Cinq axes d'action (voir [[https://www.ouvrirlascience.fr/research-software-as-a-pillar-of-open-science/][détails en ligne]])
- Identifier et mettre en valeur la production de logiciels de recherche
- Outils techniques et sociaux et meilleures pratiques
- Valorisation et durabilité
- Liaison et animation aux niveaux national, européen et international
- Reconnaissance et carrières
  #+beamer: \pause
*** Introduction au Code Source
    :PROPERTIES:
    :BEAMER_COL: .38
    :BEAMER_OPT: pic=2023-source-code-booklet.png,width=.35\linewidth,leftpic=true,qrurl=https://www.ouvrirlascience.fr/source-code-and-software/
    :BEAMER_env: picblock
    :END:
concepts clés
- pour les étudiants
- pour les enseignants
- pour les chercheurs
*** Rapport sur les forges logicielles
    :PROPERTIES:
    :BEAMER_COL: .35
    :BEAMER_OPT: pic=2023-forges-esr.png,width=.3\linewidth,leftpic=true,qrurl=https://www.ouvrirlascience.fr/higher-education-and-research-forges-in-france-definition-uses-limitations-encountered-and-needs-analysis/
    :BEAMER_env: picblock
    :END:
dans le milieu académique (FR) :
- besoins
- options
- limitations
*** Prix annuel
    :PROPERTIES:
    :BEAMER_COL: .3
    :BEAMER_env: block
    :END:
/Établir un prix national pour les logiciels de recherche/. Open Research Europe 2023
\qrcode[height=.2\linewidth]{http://dx.doi.org/10.12688/openreseurope.16069.1}
** Prix science ouverte pour le Logiciel Libre en France : 2022 et 2023
*** Première édition, [[https://www.enseignementsup-recherche.gouv.fr/fr/remise-des-prix-science-ouverte-du-logiciel-libre-de-la-recherche-83576][prix 2022]]
    :PROPERTIES:
    :BEAMER_COL: .56
    :BEAMER_env: block
    :END:
file:../../common/modules/PNSO2-award.png
**** 
:PROPERTIES:
:BEAMER_env: column
:BEAMER_COL: .35
:END:
- *129* projets
- *4* prix
- *6* accessits
- *1ère* édition
**** 
:PROPERTIES:
:BEAMER_env: column
:BEAMER_COL: .65
:END:
- [[https://coq.inria.fr/][Coq]] assistant de preuve
- [[https://scikit-learn.org/][Scikit-Learn]] ML/IA
- [[https://faust.grame.fr/][Faust]] musique
- [[https://gammapy.org/][Gammapy]] astronomie
#+beamer: \pause
*** Deuxième édition, [[https://www.enseignementsup-recherche.gouv.fr/fr/remise-des-prix-science-ouverte-du-logiciel-libre-de-la-recherche-2023-93732][prix 2023]]
    :PROPERTIES:
    :BEAMER_COL: .42
    :BEAMER_env: block
    :END:
file:../../common/modules/PNSO2-awards-2023.png

- *66* projets
- *4* récompenses
- *4* "espoirs"
- *désormais annuel*
** Plan et données de la première édition (2021-2022)
\vspace{-.5em}
*** Plans et analyses disponibles
    :PROPERTIES:
    :BEAMER_col: .4
    :BEAMER_env: block
    :END:
****                                                               :B_column:
     :PROPERTIES:
     :BEAMER_env: column
     :BEAMER_COL: .01
     :END:
****                                                               :B_column:
     :PROPERTIES:
     :BEAMER_env: column
     :BEAMER_COL: .8
     :END:
    #+ATTR_LATEX: :width \linewidth
    file:2023-ORE-foss-award.png
****                                                               :B_column:
     :PROPERTIES:
     :BEAMER_env: column
     :BEAMER_COL: .2
     :END:
\qrcode[height=1.8em]{https://doi.org/10.12688/openreseurope.16069.1}
****                                                        :B_ignoreheading:
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:
- objectifs, décisions de conception
- défis et solutions
- leçons apprises
- *données détaillées*
#+beamer: \pause
*** Un aperçu des données
    :PROPERTIES:
    :BEAMER_col: .6
    :BEAMER_env: block
    :END:
    #+ATTR_LATEX: :width .9\linewidth
file:../../common/modules/2022-foss-os-awards-stats.png
#+beamer: \pause
***                                                         :B_ignoreheading:
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:
\vspace{-0.5em}
*** L'émulation fonctionne
\hfill Australie : [[https://ardc.edu.au/article/new-research-software-award-joins-the-eureka-prizes/][ici]] et [[https://www.ecolsoc.org.au/awards/ardc-award-for-new-developers-of-open-source-software-in-ecology/][ici]] ; Allemagne : [[https://os.helmholtz.de/en/open-research-software/helmholtz-incubator-software-award/][Helmholtz]] ; Commission européenne : [[https://www.horizon-europe.gouv.fr/public-recognition-scheme-open-source-csa-32611][une CSA]] ; ...
** Enquête nationale sur les logiciels de recherche (2023)
*** Contexte
L'article 163 de la loi n° 2022-217 du 21 février 2022 exigeait
un rapport sur la production et l'impact des logiciels issus de la recherche effectuée dans
les entités financées par des fonds publics (universités, organismes de recherche, etc.)
*** Processus et résultats sélectionnés
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=mesr-sw-report.png, leftpic=true, width=.37\linewidth
    :END:
Enquête ouverte (1331 réponses détaillées), et échanges approfondis avec les bureaux de transfert de technologie
- 50% des logiciels ont plus de 9 ans
- 36% ont plus de 100 utilisateurs
- 62% ont un impact en dehors du milieu académique
- la majorité est en Logiciel Libre, 10% propriétaire
- 23% font l'objet d'un transfert technologique
\hfill tous les détails (en français) à \qrcode[height=2em]{https://www.enseignementsup-recherche.gouv.fr/fr/presentation-du-rapport-sur-l-etat-des-lieux-des-logiciels-issus-de-la-recherche-publique-francaise-93726}
** Le baromètre français de la science ouverte...
#+beamer: \includegraphics[page=1,width=.9\linewidth]{bso-context.pdf}
#+latex: \mbox{}\\\tiny \hfill Crédits : Laetitia Bracco et l'équipe du BSO
** ... inclut désormais le logiciel ...
#+beamer: \includegraphics[page=1,width=.9\linewidth]{bso-software.pdf}
#+latex: \qrcode[height=2em]{https://frenchopensciencemonitor.esr.gouv.fr/software/general}
#+latex: \mbox{}\\\tiny \hfill Crédits : Laetitia Bracco et l'équipe du BSO\\
\mbox{}\hfill Utilise une version améliorée de SoftCite par rapport à [[https://medium.com/czi-technology/new-data-reveals-the-hidden-impact-of-open-source-in-science-11cc4a16fea2][l'étude CZI 2022 en biomédecine]]
** ... donne une vue précieuse, par discipline
*** Utilisation des logiciels
    :PROPERTIES:
    :BEAMER_opt: pic=french_os_monitor-use-2024, leftpic=true, width=\linewidth
    :BEAMER_env: picblock
    :END:
Les logiciels sont [[https://frenchopensciencemonitor.esr.gouv.fr/software/fields?id=disciplines.utilisation][massivement utilisés dans toutes les disciplines]]
\mbox{}\\
\mbox{}\hfill \qrcode[height=3em]{https://frenchopensciencemonitor.esr.gouv.fr/software/fields?id=disciplines.utilisation}
** ... donne une vue précieuse, par discipline, suite
*** Partage des logiciels
    :PROPERTIES:
    :BEAMER_opt: pic=french_os_monitor-share-2024, leftpic=true, width=\linewidth
    :BEAMER_env: picblock
    :END:
Plus de 20 % des articles mentionnant la création de logiciels les [[https://frenchopensciencemonitor.esr.gouv.fr/software/fields?id=disciplines.partage][partagent réellement]]\\
\mbox{}\\
\mbox{}\hfill \qrcode[height=3em]{https://frenchopensciencemonitor.esr.gouv.fr/software/fields?id=disciplines.partage}

* Répondre aux besoins de l'ARDC
** Ce qui est en jeu
*** Archivage
Les artefacts des logiciels de recherche doivent être correctement *archivés*\\
\hfill s'assurer que nous pouvons les /retrouver/ (/reproductibilité/)
#+beamer: \pause
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
\vspace{-.5em}
*** Référencement
Les artefacts des logiciels de recherche doivent être correctement *référencés*\\
\hfill s'assurer que nous pouvons les /identifier/ (/reproductibilité/)
#+beamer: \pause
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
\vspace{-.5em}
*** Description
Les artefacts des logiciels de recherche doivent être correctement *décrits*\\
\hfill faciliter leur /découverte/ et leur /réutilisation/ (/visibilité/)
#+beamer: \pause
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
\vspace{-.5em}
*** Citer/Créditer
Les artefacts des logiciels de recherche doivent être correctement *cités* /(ce n'est pas la même chose que référencer !)/\\
\hfill donner du /crédit/ aux auteurs (/évaluation/!)
** Software Heritage : /un/ seul archive logiciel, une /infrastructure partagée/ ...
#+beamer: \transdissolve
*** 
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_COL: .25
    :END:
*Une* infrastructure\\
*ouverte* et *partagée*
*** 
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_COL: .5
    :END:
#+latex: \begin{center}
file:../../common/modules/SWH-as-foundation-slim.png
#+latex: \end{center}
***                                                                :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_COL: .25
    :END:
#+latex: \centering
   #+ATTR_LATEX: :width .5\linewidth
file:../../common/modules/inria-unesco.png
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
   #+BEAMER: \pause
   \mbox{}\\
#+latex: \centering{Le plus grand archive jamais construit}
*** 
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_COL: .6
    :END:
#+latex: \centering
file:../../common/modules/archive-growth.png
#+beamer: \pause
#+beamer: \transdissolve
***                                                                :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_COL: .4
    :END:
   #+latex: \centering
   #+ATTR_LATEX: :width \linewidth
   file:sponsors.pdf
** ... meilleure expérience pour les chercheurs, dans le monde entier
   #+ATTR_LATEX: :width \linewidth
file:../../common/modules/hal-swh-overview.png
*** 
\hfill En savoir plus sur https://softwareheritage.org/ \hfill demo à la demande
* Indicateurs d'adoption et appel à l'action
\includepdf[pages=1]{adoption-global.pdf}
#+latex: \addtocounter{framenumber}{1}
** Vers un impact global
*** Institutionnel : OSMF
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    #+ATTR_LATEX: :width .8\linewidth
file:../../common/modules/unesco-osfm.png
Effort sous l'[[https://www.ouvrirlascience.fr/building-an-open-science-monitoring-framework-with-open-technologies-unesco-workshop-19-12-23/][impulsion de l'UNESCO et de la France]] pour construire un "cadre de suivi de la science ouverte" compatible avec les moniteurs à travers les pays
*** Infrastructure : SOFair
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    #+ATTR_LATEX: :width .75\linewidth
file:../../common/modules/so-fair_workflow.png
Effort pour identifier les mentions de logiciels dans *toute la littérature en accès ouvert*, et ajouter des liens vers l'[[https://softwareheritage.org][archive Software Heritage]]
\hfill \qrcode[height=2em]{https://www.softwareheritage.org/2023/12/08/connecting-publications-software-sofair/}
** Plans de gestion des logiciels : il y a plus qu'il n'y paraît ! :skipinshort:
*** Durabilité
- modèle économique
- communauté et gouvernance
- licence
#+beamer: \pause
*** Évaluation et partage des profits
- rôles et pertinence des contributeurs
#+beamer: \pause
*** Technique
- infrastructure, outils, processus, assurance qualité
#+beamer: \pause
*** 
Une licence n'est pas un modèle économique, une forge n'est pas une communauté\\
\hfill Cédric Thomas, PDG d'OW2
** Approche en deux volets : 1, Décrire et Suivre
\vspace{-0.3em}
*** Construire un /catalogue uniforme/ des logiciels de recherche
- métadonnées et PID pour le logiciel (utiliser CodeMeta et SWHID)
- point d'entrée unique entrer et extraire des informations (intégration HAL+SWH)
- informations sur tous les logiciels de recherche, ouverts **ou fermés**
- certaines informations sont privées (par exemple, détails de valorisation)
#+beamer: \pause
*** espace                                                     :B_ignoreheading:
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:
\vspace{-0.8em}    
*** Nous avons
    :PROPERTIES:
    :BEAMER_COL: .4
    :BEAMER_env: block
    :END:
- HAL et SWH : dépôt modéré /pour le code ouvert/ avec /métadonnées publiques/
- poussé à l'international (via EOSC, RDA, Force11)
#+beamer: \pause
*** Nous avons besoin (et les actions)
    :PROPERTIES:
    :BEAMER_COL: .6
    :BEAMER_env: block
    :END:
- **moderateurs des dépôts logiciel** dans chaque institution, formation via HAL/CCSD.
- étendre le catalogue pour couvrir /code fermé et informations privées/
- collaboration avec les équipes de valorisation
*** 
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
\vspace{-0.5em}    
*** 
**Comment participer** \hfill Contact : [[mailto:sabrina.granger@inria.fr][sabrina.granger@inria.fr]]
** Appel à l'action : promouvoir les bonnes pratiques pour l'ARDC
#+beamer: \vspace{-.5em}
*** Archiver et référencer
*Tout code source* utilisé dans la recherche (/même les petits scripts !/) \hfill     /reproductibilité/
**** 
     :PROPERTIES:
     :BEAMER_COL: .45
     :END:
- sauver dans Software Heritage
- ajouter le *SWHID* dans les articles
**** 
     :PROPERTIES:
     :BEAMER_COL: .25
     :END:
Voir [[https://www.softwareheritage.org/howto-archive-and-reference-your-code/][guide détaillé en ligne]] \\
**** 
     :PROPERTIES:
     :BEAMER_COL: .2
     :END:
\centering
\qrcode[height=.4\linewidth]{https://www.softwareheritage.org/howto-archive-and-reference-your-code/}
#+beamer: \pause
*** Décrire et Citer/Créditer
Pour *les logiciels que l'on souhaite mettre en avant* (/CV, rapports, etc., obtenir citations et du crédit/),
suivre les *étapes supplémentaires* suivantes : \hfill tutoriels vidéo
**** 
     :PROPERTIES:
     :BEAMER_COL: .8
     :END:
- ajouter un *codemeta.json* (voir le [[https://codemeta.github.io/codemeta-generator/][générateur codemeta]])
- référencer dans HAL ([[https://doc.archives-ouvertes.fr/en/deposit/deposit-software-source-code/][documentation en ligne HAL]])
- citer avec [[https://ctan.org/pkg/biblatex-software][biblatex-software]] (dans CTAN et TeXLive)
**** 
     :PROPERTIES:
     :BEAMER_COL: .2
     :END:
\centering
\qrcode[height=.4\linewidth]{https://www.youtube.com/watch?v=-Ggn98sR3Eg&list=PLD2VqrZz2-u3bOWtoCoBIh5Flt6iYXsq3}
#+beamer: \pause
*** 
#+beamer: \vspace{-.5em}
***** 
      :PROPERTIES:
      :BEAMER_col: .01
      :BEAMER_env: column
      :END:
***** 
      :PROPERTIES:
      :BEAMER_col: .4
      :BEAMER_env: column
      :END:
former les étudiants et collègues
***** 
      :PROPERTIES:
      :BEAMER_col: .6
      :BEAMER_env: column
      :END:
impliquer les revues, conférences, sociétés savantes
***** 
      :PROPERTIES:
      :BEAMER_env: ignoreheading
      :END:
rejoindre le groupe d'intérêt Software Heritage ALIG \hfill \qrcode[height=3em]{https://www.softwareheritage.org/support/members-alig/}
** Approche en deux volets : 2, Processus et Expertise
*** Développer une stratégie pour aborder l'ensembler des questions
- construire un corpus de connaissances partagées
  - travaux en cours aux "Ateliers de la donnée"
- construire un réseau d'expertise
  - se connecter avec d'autres institutions
  - se connecter avec des experts du logiciel libre et des OSPO
- développer un arbre de décision pour les chercheurs
- inclure le logiciel dans la politique Science Ouverte d'établissement
#+beamer: \pause
*** Comment procéder
- le réseau des ADAC
- les ateliers de la donnée
- le Collège logiciel dans le CoSO
** La parole est à vous
*** 
#+latex: \centering{\large travaillons ensemble pour mener la vague}\\[1em]
*** Références
  #+BEGIN_EXPORT latex
  \begin{thebibliography}{Foo Bar, 1969}
  \footnotesize
%  \bibitem{SwForumEu2021} R. Di Cosmo, \emph{A revolutionary infrastructure for Open Source}, 2021, EU Software Forum \href{https://annex.softwareheritage.org/public/talks/2021/2021-03-24-SwForum.pdf}{(slides)} \href{https://youtu.be/AwY527kDMfM?t=178}{(video)}
  \bibitem{UNESCOOS} UNESCO, \emph{Draft recommendations on Open Science}
  \newblock 2021, \href{https://unesdoc.unesco.org/ark:/48223/pf0000378381.locale=en}{(online)}
  \bibitem{PNSO2} French Ministry of Research, \emph{Second National Plan for Open Science}
  \newblock 2021, \href{https://www.enseignementsup-recherche.gouv.fr/cid159131/le-plan-national-pour-la-science-ouverte-2021-2024-vers-une-generalisation-de-la-science-ouverte-en-france.html}{(online)}
  \bibitem{EOSCSirs2020} EOSC SIRS Task Force, \emph{Scholarly Infrastructures for Research Software}
  \newblock 2020, Publications office of the European Commission, \href{https://doi.org/10.2777/28598}{(10.2777/28598)}
  \bibitem{DiCosmo2020d} R. Di Cosmo, \emph{Archiving and Referencing Source Code with Software Heritage}
  \newblock International Conference on Mathematical Software 2020 \href{https://dx.doi.org/10.1007/978-3-030-52200-1_36}{(10.1007/978-3-030-52200-1\_36)}
%  \bibitem{DiCosmo2019} R. Di Cosmo, M. Gruenpeter, S. Zacchiroli\newblock
%  \emph{Referencing Source Code Artifacts: a Separate Concern in Software Citation},\newblock
%  CiSE 2020 \href{https://dx.doi.org/10.1109/MCSE.2019.2963148}{(10.1109/MCSE.2019.2963148)}
%  \href{https://hal.archives-ouvertes.fr/hal-02446202}{(hal-02446202)}
%  \bibitem{alliez:hal-02135891} P. Alliez, R. Di Cosmo, B. Guedj, A. Girault, M.-S. Hacid, A. Legrand and N. Rougier\newblock
%  \emph{Attributing and referencing (research) software: Best practices and outlook from Inria}, \newblock
%  CiSE 2020 \href{https://doi.ieeecomputersociety.org/10.1109/MCSE.2019.2949413}{(10.1109/MCSE.2019.2949413)}
%  \href{https://hal.archives-ouvertes.fr/hal-02135891}{(hal-02135891)}
  \bibitem{Abramatic2018} J.F. Abramatic, R. Di Cosmo, S. Zacchiroli,
  \emph{Building the Universal Archive of Source Code}
  \newblock CACM, October 2018 \href{https://doi.org/10.1145/3183558}{(10.1145/3183558)}
  \end{thebibliography}
  #+END_EXPORT
