Title: Software Heritage: the universal source code archive

Abstract:

 Software source code is everywhere, with tens of millions of developers
 worldwide, and it is of paramount importance to provide a universal
 archive and reference system for all its applications. Software Heritage
 has taken over this task.

 Since software source code has been recently recognised as an important
 asset also in the field of scientific research, complementing publications
 and research data, Software Heritage is now providing the infrastructure
 for depositing and referencing software source code, in collaboration with
 national and international open access portals.

 In this short presentation we will provide an overview of the available
 functionalities, and an update on the latest collaborations.
