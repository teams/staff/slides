#+COLUMNS: %40ITEM %10BEAMER_ENV(Env) %9BEAMER_ENVARGS(Env Args) %9BEAMER_ACT(Act) %4BEAMER_COL(Col) %10BEAMER_EXTRA(Extra) %8BEAMER_OPT(Opt)
#+TITLE: Software Heritage
#+SUBTITLE: a revolutionary infrastructure for Open Source and Open Science
#+BEAMER_HEADER: \title{Software Heritage}
#+AUTHOR: Roberto Di Cosmo
#+EMAIL: roberto@dicosmo.org
#+BEAMER_HEADER: \date{March 2025}
#+BEAMER_HEADER: \title[Contact: roberto@dicosmo.org]{Software Heritage}
#+BEAMER_HEADER: \author[Software Heritage \hspace{2em}www.softwareheritage.org\hspace{2em}@swheritage]{Roberto Di Cosmo\\Director, Software Heritage\\Inria and Université Paris Cité}
# #+BEAMER_HEADER: \setbeameroption{show notes on second screen}
#+BEAMER_HEADER: \setbeameroption{hide notes}
#+KEYWORDS: software heritage legacy preservation knowledge mankind technology
#+LATEX_HEADER: \hypersetup{colorlinks,linkcolor=,urlcolor=links}
#+LATEX_HEADER: \definecolor{links}{HTML}{2A1B81}
#+LATEX_HEADER: \usepackage{pdfpages}
#+LATEX_HEADER: \usepackage{pax}
#+LATEX_HEADER: \usepackage{qrcode}

#
# prelude.org contains all the information needed to export the main beamer latex source
# use prelude-toc.org to get the table of contents
#

#+INCLUDE: "../../common/modules/prelude-toc.org" :minlevel 1

#+INCLUDE: "../../common/modules/169.org"

#+INCLUDE: "../../common/modules/highcontrast.org"


* Introduction
  #+INCLUDE: "../../common/modules/rdc-bio.org::#main" :only-contents t :minlevel 2
** Software /Source Code/ is Precious Knowledge
   #+INCLUDE: "../../common/modules/source-code-different-short.org::#softwareisdifferent-covidsim" :only-contents t :minlevel 3
** What is at stake
   #+INCLUDE: "../../common/modules/swh-ardc.org::#compactstakes" :only-contents t :minlevel 3
*** 
    \hfill a humbling challenge, and a complex one (we are not in a vacuum)
** French National plan for Open Science, 2021-2024                
   #+INCLUDE: "../../common/modules/policyactions.org::#PNSO2-official" :only-contents t :minlevel 3
*** 
    \hfill let's focus on the ARDC part
* Software Heritage in the big picture
#
# One slide motivation + goals
#
# Phases
#
** Software Heritage in a nutshell \hfill www.softwareheritage.org
#+BEAMER: \transdissolve
  #+latex: {\bf \emph{Unveiled in 2016}\vspace{-1em}}
  #+latex: \begin{center}
  #+ATTR_LATEX: :width \extblockscale{.8\linewidth}
  file:../../common/modules/SWH-logo+motto.pdf
  #+latex: \end{center}
*** Collect, preserve and share /all/ software source code
    \hfill Preserving our heritage, enabling better software and better science for all
#+BEAMER: \pause
*** Reference catalog 
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .3
    :END:
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[width=.6\linewidth]{myriadsources}
\end{center}
#+END_EXPORT 
    *find* and *reference* all software source code
#+BEAMER: \pause
*** Universal archive
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .3
    :END:
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[width=.6\linewidth]{fragilecloud}
\end{center}
#+END_EXPORT 
    *preserve and share* all software source code
#+BEAMER: \pause
*** Research infrastructure                                                             :B_block:
    :PROPERTIES:
    :BEAMER_COL: .3
    :BEAMER_env: block
    :END:
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[width=.7\linewidth]{atacama-telescope}
\end{center}
#+END_EXPORT 
    *enable analysis* of all software source code
** A /universal/ software archive, as a shared infrastructure
***                                                                :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_COL: .25
    :END:
    *One* infrastructure\\
    *open* and *shared*
***                                                                :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_COL: .5
    :END:
   #+latex: \begin{center}
   #+ATTR_LATEX: :width \linewidth
   file:../../common/modules/SWH-as-foundation-slim.png
   #+latex: \end{center}
***                                                                :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_COL: .25
    :END:
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
   #+BEAMER: \pause
   \mbox{}\\
   #+latex: \centering{The largest archive ever built}
***                                                                :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_COL: .6
    :END:
   #+latex: \centering
   #+ATTR_LATEX: :width \linewidth
   file:../../common/modules/archive-growth.png
   #+BEAMER: \pause
   #+BEAMER: \transdissolve
***                                                                :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_COL: .4
    :END:
   #+latex: \centering
   #+ATTR_LATEX: :width \linewidth
   file:../../common/modules/archive-breakdown.png
** An international, non profit initiative\hfill built for the long term
*** vspace                                                  :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+latex: \vspace{-0.5em}
#+INCLUDE: "../../common/modules/support+sponsors.org::#support+sponsors" :only-contents t :minlevel 3
*** vspace                                                  :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+latex: \vspace{-0.5em}
** The archive under the hood
      #+BEAMER: \begin{center}
      #+BEAMER:   \mode<beamer>{\only<1>{\includegraphics[width=\extblockscale{\textwidth}]{swh-dataflow-merkle-listers.pdf}}}
      #+BEAMER:   \only<2-3>{\includegraphics[width=\extblockscale{\textwidth}]{swh-dataflow-merkle.pdf}}
      #+BEAMER: \end{center}
      #+BEAMER: \pause
      #+BEAMER: \pause
      /Global development history/ *permanently archived* in a *uniform data model*
      - over *20 billion* unique source files from over *300 million* software projects
      - *~2PB* (compressed) blobs, *~50 B* nodes, *~700 B* edges
** The Software Hash persistent identifier (SWHID)
   #+latex: \vspace{-0.5em}
*** Software Hash Identifiers (SWHID) \hfill [[https://swhid.org][see swhid.org]]          :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :END:
  50+B [[https://www.softwareheritage.org/2020/07/09/intrinsic-vs-extrinsic-identifiers/][intrinsic, decentralised, cryptographically strong identifiers, SWHIDs]]
# #+INCLUDE: "../../common/modules/swh-id-syntax.org::#swh-id-syntax" :only-contents t :minlevel 3
  #+LATEX: \centering%\forcebeamerstart
  #+LATEX: \mode<beamer>{\only<1>{\includegraphics[width=0.9\linewidth]{SWHID-v1.4_1.png}}}
  #+LATEX: \mode<beamer>{\only<2>{\includegraphics[width=0.9\linewidth]{SWHID-v1.4_2.png}}}
  #+LATEX: \only<3->{\includegraphics[width=0.9\linewidth]{SWHID-v1.4_3.png}}
  #+LATEX: %\forcebeamerend
*** vspace                                                  :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+latex: \vspace{-0.8em}
*** 
    :PROPERTIES:
    :BEAMER_act: <4->
    :BEAMER_env: block
    :END:
    Examples: [[https://archive.softwareheritage.org/swh:1:cnt:64582b78792cd6c2d67d35da5a11bb80886a6409;origin=https://github.com/virtualagc/virtualagc;lines=245-261/][Apollo 11 AGC]], [[https://archive.softwareheritage.org/swh:1:cnt:bb0faf6919fc60636b2696f32ec9b3c2adb247fe;origin=https://github.com/id-Software/Quake-III-Arena;lines=549-572/][Quake III rsqrt]]
*** vspace                                                  :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+latex: \vspace{-0.8em}
*** 
    :PROPERTIES:
    :BEAMER_act: <5->
    :BEAMER_env: block
    :END:
    In [[https://spdx.github.io/spdx-spec/appendix-VI-external-repository-identifiers/#persistent-id][SPDX 2.2]]; IANA registered ="swh:"=; WikiData [[https://www.wikidata.org/wiki/Property:P6138][P6138]]\\
    \hfill Standardisation ongoing [[https://www.swhid.org/][DIS 18670]] 
*** vspace                                         :B_ignoreheading:noexport:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+latex: \vspace{-0.5em}
*** Full fledged /source code references/ for traceability, integrity and reproducibility :B_block:noexport:
    :PROPERTIES:
    :BEAMER_act: <5->
    :BEAMER_env: block
    :END:
    Examples: [[https://archive.softwareheritage.org/swh:1:cnt:64582b78792cd6c2d67d35da5a11bb80886a6409;origin=https://github.com/virtualagc/virtualagc;lines=245-261/][Apollo 11 AGC]], [[https://archive.softwareheritage.org/swh:1:cnt:bb0faf6919fc60636b2696f32ec9b3c2adb247fe;origin=https://github.com/id-Software/Quake-III-Arena;lines=549-572/][Quake III rsqrt]]; Guidelines available: [[https://www.softwareheritage.org/howto-archive-and-reference-your-code/][HOWTO]] and [[https://dx.doi.org/10.1007/978-3-030-52200-1_36][ICMS 2020]] 
** Addressing ARDC: an example is worth 1000 words
*** Software metadata: codemeta.json
    - [[https://archive.softwareheritage.org/swh:1:cnt:0be13d6d1d0c0b377cd6e9e152d24c4e0b08d37d;origin=https://github.com/rdicosmo/parmap;visit=swh:1:snp:e4b14ad0619db71bb86ebac7edc7c6c8ab234ee9;anchor=swh:1:rev:963608763589e03de38e744d359884d491e65460;path=/codemeta.json][example from Parmap]], created with [[https://codemeta.github.io/codemeta-generator/][Codemeta generator]]
    - software citation (see [[https://www.softwareheritage.org/how-to-archive-reference-code/][detailed HOWTO]]) and [[https://ctan.org/pkg/biblatex-software?lang=en][biblatex-software]]
    #+BEAMER: \pause      
*** Integration with the HAL national french open access archive
    - [[https://doc.hal.science/deposer/deposer-le-code-source/][Curated deposit]]: metadata quality due to moderation
    #+BEAMER: \pause      	
    - examples: [[https://hal.archives-ouvertes.fr/hal-02130801][LinBox]], [[https://hal.archives-ouvertes.fr/hal-01897934][SLALOM]], [[https://hal.archives-ouvertes.fr/hal-02130729][Givaro]], [[https://hal.archives-ouvertes.fr/hal-02137040][NS2DDV]], [[https://hal.archives-ouvertes.fr/lirmm-02136558][SumGra]], [[https://hal.archives-ouvertes.fr/hal-02155786][Coq proof]], ...
    - generation of reports, cv, web pages: [[https://haltools.archives-ouvertes.fr/Public/afficheRequetePubli.php?struct=inria&&typdoc=(%27SOFTWARE%27)&CB_auteur=oui&CB_titre=oui&CB_article=oui&CB_resume=oui&langue=Anglais&tri_exp=annee_publi&tri_exp2=typdoc&tri_exp3=date_publi&ordre_aff=TA&Fen=Aff&css=../css/VisuRubriqueEncadre.css][for Inria]], [[https://haltools.archives-ouvertes.fr/Public/afficheRequetePubli.php?struct=cnrs&typdoc=(%27SOFTWARE%27)&CB_auteur=oui&CB_titre=oui&CB_article=oui&CB_resume=oui&langue=Anglais&tri_exp=annee_publi&tri_exp2=typdoc&tri_exp3=date_publi&ordre_aff=TA&Fen=Aff&css=../css/VisuRubriqueEncadre.css][for CNRS]], [[https://haltools.archives-ouvertes.fr/Public/afficheRequetePubli.php?struct=cnes&typdoc=(%27SOFTWARE%27)&CB_auteur=oui&CB_titre=oui&CB_article=oui&CB_resume=oui&langue=Anglais&tri_exp=annee_publi&tri_exp2=typdoc&tri_exp3=date_publi&ordre_aff=TA&Fen=Aff&css=../css/VisuRubriqueEncadre.css][for CNES]], [[https://haltools.archives-ouvertes.fr/Public/afficheRequetePubli.php?struct=LIRMM&typdoc=(%27SOFTWARE%27)&CB_auteur=oui&CB_titre=oui&CB_article=oui&CB_resume=oui&langue=Anglais&tri_exp=annee_publi&tri_exp2=typdoc&tri_exp3=date_publi&ordre_aff=TA&Fen=Aff&css=../css/VisuRubriqueEncadre.css][for LIRMM]] or [[https://haltools.archives-ouvertes.fr/Public/afficheRequetePubli.php?auteur_exp=remi%2C+gribonval&struct=cnrs&typdoc=(%27SOFTWARE%27)&CB_auteur=oui&CB_titre=oui&CB_article=oui&CB_resume=oui&langue=Anglais&tri_exp=annee_publi&tri_exp2=typdoc&tri_exp3=date_publi&ordre_aff=TA&Fen=Aff&css=../css/VisuRubriqueEncadre.css][for Rémi Gribonval]] using [[https://haltools.archives-ouvertes.fr/?action=export&lang=fr][HalTools]]
    #+BEAMER: \pause      	
*** Software Heritage + a /curated/ metadata repository allows to address all needs ...
    - /researcher, engineer/: archival, reference, credit, CV etc. /with a little effort from them/
    - /labs, organizations/: track and report software production, /curated/ catalog
* Demo time
** Get Swhid for a code snippet in the archive
   #+latex: \begin{center}
   #+latex: \href{run:swh-getswhid_snippet.mp4?poster=swh-getswhid_snippet-poster.png}{%
   #+latex:   \includegraphics[width=.8\linewidth, keepaspectratio]{swh-getswhid_snippet-poster.png}%
   #+latex: }
   #+latex: \end{center}
** Get a citation for an archived code
   #+latex: \begin{center}
   #+latex: \href{run:swh-citation.mp4?poster=swh-citation-poster.png}{%
   #+latex:   \includegraphics[width=.8\linewidth, keepaspectratio]{swh-citation-poster.png}%
   #+latex: }
   #+latex: \end{center}
** Use the updateswh browser extension
   #+latex: \begin{center}
   #+latex: \href{run:swh-updateswh.mp4?poster=swh-updateswh-poster.png}{%
   #+latex:   \includegraphics[width=.8\linewidth, keepaspectratio]{swh-updateswh-poster.png}%
   #+latex: }
   #+latex: \end{center}
** Some adoption indicators
   #+INCLUDE: "../../common/modules/swh-adoption-academic.org::#indicators" :only-contents t :minlevel 3
** Software Heritage: key infrastructure for Open Science
*** [[https://data.europa.eu/doi/10.2777/28598][EOSC SIRS report]]: Software Source Code and Open Science, 2020 :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=eosc-sirs-architecture-swh.png, leftpic=true, width=.5\textwidth
    :END:
    Connect scholarly ecosystem with the whole software ecosystem\\
    See e.g. [[https://code.gouv.fr/#/repos][the French public administration open source catalog]] 
*** Ongoing work: [[https://faircore4eosc.eu/][FAIRCORE4EOSC]]
    A full workpackage:
     - connectors with InvenioRDM (Zenodo), episcience, Dagstuhl, swMath, etc.
     - Software Heritage mirror for the European Open Science Cloud (EOSC)
     - standardisation of CodeMeta and SWHID
** A revolutionary infrastructure
*** 
    Modern "Library of Alexandria", /international, non profit, long term/ initiative\\
    \pause\hfill addressing the needs of /industry, research, culture and society as a whole/
*** /Software Graph/
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .18
    :END:
    #+ATTR_LATEX: :width .8\linewidth
    file:git-merkle/merkle-vertical.png
*** /Software Blockchain/
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .3
    :BEAMER_act:
    :END:
    #+ATTR_LATEX: :width \linewidth
    file:merkle.png
*** /Open Science/ pillar
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .28
    :END:
    #+ATTR_LATEX: :width \linewidth
    file:PreservationTriangle.png
*** /Big Code/
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .2
    :BEAMER_act:
    :END:
    #+ATTR_LATEX: :width \linewidth
    file:universal.png
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** 
   \centering /One/ infrastructure, /shared/: more efficient, less waste ...
*** 
   \centering ... supporting revolutionary products!
*** \hfill Cybersecurity, AI, Compliance, Software Engineering ... see [[https://www.softwareheritage.org/2024/11/19/software-heritage-2025-symposium-summit/][UNESCO Symposium 2025]]   
** Opportunity: your Institutional Portal!
*** Monitor and showcase your institution's software production                                                             :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=InstitutionalPortal.png, leftpic=true, width=.38\linewidth, caption={\tiny DALL-E's view of an instutional portal}
    :END:
     + Curated metadata under institutional authority
     + Persistent reference
     + Uniform citation
     + Automated extraction of reports and indicators
    *Why SWH?*
     + platform agnostic, metadata from multiple institutions
     + benefit from Software Heritage's future developments
#+BEAMER: \pause       
*** Get involved in the portal specification!
    + curation workflow\\
      \hfill researcher initiated (swhid deposit) vs institution initiated (metadata deposit)
    + product deployment: on premise vs SaaS
    + design of reports and extraction formats
* Conclusion
** A unique opportunity \hfill https://softwareheritage.org
*** Software Heritage is                                            :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    - *vendor neutral*
    - *open source*
    - a *worldwide* initiative 
    - a *long term* initiative
#+BEAMER: \pause
*** Software Heritage enables                                       :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    - long term *archival*
    - *reference* for reproducibility
    - a *global software knowledge base*
    - *mutualisation* of cost
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEAMER: \pause
*** Call to action
    - support Software Heritage as shared open infrastructure, join the ALIG group
    - get involved with the new applications
*** vspace                                                  :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+latex: \vspace{-0.5em}      
*** Annual report 2024
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .3
    :END:
    #+latex: \href{https://annex.softwareheritage.org/public/annual-reports/2024/SoftwareHeritageReport_2024.pdf}{\includegraphics[width=.3\linewidth]{2024-report-swh.png}}
    \hfill
    #+latex: \raisebox{2.1em}{\qrcode[height=4em]{https://annex.softwareheritage.org/public/annual-reports/2024/SoftwareHeritageReport_2024.pdf}}
*** 5 years in 5 minutes
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .3
    :END:
    #+latex: \href{https://www.youtube.com/watch?v=Ez4xKTKJO2o}{\includegraphics[width=.6\linewidth]{5years-video.png}}
    \hfill
    #+latex: \raisebox{1.8em}{\qrcode[height=4em]{https://www.youtube.com/watch?v=8RdA5TxOAGo}}
* Appendix                                                          :B_appendix:
:PROPERTIES:
:BEAMER_env: appendix
:END:
* Selected highlight: Source Code Compliance                      
** Source Code Tracking for... --- Open Compliance                
   # (Open Compliance, /noun/ --- the reason we are gathered here today)\\
   # More seriously, here is
   #+BEAMER: \begin{definition}[Open Compliance]
   The *pursuit of compliance* with /license obligations/ and other /best
   practices/ for the management of open source software components, *using
   only open technologies* such as: _open source_ software, _open data_
   information, and _open access_ documentation.
   #+BEAMER: \end{definition}
  #+BEAMER: \pause
*** Why
    Reduced lock-in risks, lower total cost of ownership (TCO), crowdsourcing,
    alignment with FOSS community ethos.
  #+BEAMER: \pause
*** 
    #+BEAMER: \bfseries
    We still lack a source code scanning tool that is compliant with Open
    Compliance principles and addresses industry practical needs.
    #+BEAMER: \\[2ex]
    Can we build one on top of Software Heritage?

** Tech preview: swh-scanner
  #+BEAMER: \vspace{-1mm}
*** Problem
    - detect *unknown files* in an open source project
    - detect *proprietary files* leaked on a public forge
  #+BEAMER: \pause  
*** Solution
    =swh-scanner= : *open source* and *open data* source code scanner for
    *compliance* workflows, backed by the *largest public archive* of public source code.
  #+BEAMER: \pause
*** Design 
   - Software Heritage archive as *source of truth* about public code
   - Merkle DAG model and SWHIDs for maximum scanning efficiency
#     - e.g., no need to scan files of a known directory
   - file-level granularity
   - output: source tree partition into known (= published before) v. unknown
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#    Code: [[https://forge.softwareheritage.org/source/swh-scanner/][forge.softwareheritage.org/source/swh-scanner]] (GPL 3+)\\
    Package: [[https://pypi.org/project/swh.scanner/][pypi.org/project/swh.scanner]]   (GPL 3+)
** swh-scanner --- Example                                         :noexport:
   #+BEAMER: \scriptsize \vspace{-1mm}
   #+BEGIN_SRC
   $ pip install swh.scanner

   $ swh scanner scan -f json /srv/src/linux/kernel
   {
     [...]
     "/srv/src/linux/kernel/auditsc.c": {
	 "known": true,
	 "swhid": "swh:1:cnt:814406a35db163080bbf937524d63690861ff750" },
     "/srv/src/linux/kernel/backtracetest.c": {
	 "known": true,
	 "swhid": "swh:1:cnt:a2a97fa3071b1c7ee6595d61a172f7ccc73ea40b" },
     "/srv/src/linux/kernel/bounds.c": {
	 "known": true,
	 "swhid": "swh:1:cnt:9795d75b09b2323306ad6a058a6350a87a251443" },
     "/srv/src/linux/kernel/bpf": {
	 "known": true,
	 "swhid": "swh:1:dir:fcd9987804d26274fee1eb6711fac38036ccaee7" },
     "/srv/src/linux/kernel/capability.c": {
	 "known": true,
	 "swhid": "swh:1:cnt:1444f3954d750ba685b9423e94522e0243175f90" },
     [...]
   }
   0,53s user 0,61s system 145% cpu 1,867 total
   #+END_SRC

** swh-scanner --- Example (cont.)                                 :noexport:
   #+BEAMER: \scriptsize
   #+BEGIN_SRC
   $ du -sh --exclude=.git /srv/src/linux
   1,1G	/srv/src/linux

   $ time swh scanner scan -f json -x *.git /srv/src/linux
   {
     [...]
     "/srv/src/linux/arch": {
	 "known": true,
	 "swhid": "swh:1:dir:590c329d3548b7d552fc913a51965353f01c9e2f" },
     [...]
     "/srv/src/linux/scripts/kallsyms.c": {
	 "known": true,
	 "swhid": "swh:1:cnt:0096cd9653327584fe62ce56ba158c68875c5067" },
     "/srv/src/linux/scripts/kconfig": {
	 "known": false,
	 "swhid": "swh:1:dir:548afc93bd01d2fba0dfcc0fd8c69f4b082ab8c6" },
     "/srv/src/linux/scripts/kconfig/.conf.o.cmd": {
	 "known": false,
	 "swhid": "swh:1:cnt:0d8be19e430c082ece6a3803923ad6ecb9e7d413" },
     [...]
   }
   20,84s user 1,52s system 103% cpu 21,540 total
   #+END_SRC

** swh-scanner --- Example
   #+BEAMER: \footnotesize
   #+BEGIN_SRC
   $ swh scanner scan -f sunburst -x *.git /srv/src/linux
   #+END_SRC
   #+BEAMER: \begin{center} \includegraphics[width=0.5\linewidth]{swh-scanner-sunburst} \end{center}
*** Towards a /product/
    ongoing work to make swh-scanner /production-ready/.
    Planned features:
    - provenance information \hfill $\to$ Software Heritage crawling info
    - license information \hfill $\to$ in-house scanning 

* Selected highlight: Improving Open Source Security with SWH     
** Is my FOSS code (known to be) vulnerable?
:PROPERTIES:
:CUSTOM_ID: vuln-check
:END:
*** 
/"The Common Vulnerabilities and Exposures (CVE) system provides a reference-method for publicly known information-security vulnerabilities and exposures."/
  - e.g., CVE-2014-0160 (AKA: [[https://en.wikipedia.org/wiki/Heartbleed][Heartbleed]]), CVE-2021-44228 (AKA: [[https://en.wikipedia.org/wiki/Log4Shell][Log4Shell]])
*** Tooling
- A number of state-of-the-art tools in FOSS compliance can:
  1. *Scan* your local code base,
  2. Compare it with a *knowledge base* that knows about CVEs,
  3. Emit *warnings* like: "you include/depend on code affected by the
     following CVEs: …".
- CVE \leftrightarrow code matching heuristics are based for the most part on *package
  metadata* (in particular: version numbers)
** Example --- osv.dev
:PROPERTIES:
:CUSTOM_ID: osv-dev
:END:
***  
- [[https://osv.dev/][OSV.dev]]: /"A distributed vulnerability database for Open Source. An open, precise, and distributed approach to producing and consuming vulnerability information for open source."/
- Service (operated by Google) and data format (standardized by OpenSSF) that:
  - Crawls vulnerability information from many places (GitHub, distros, package
    manager repos, …).
  - Provides efficient APIs to query the information and integrate it into
    compliance toolchains.
*** Is my code affected by a known CVE?
# By version:
# #+BEGIN_SRC
# curl -X POST -d \
#   '{"version": "2.4.1", "package": {"name": "jinja2", "ecosystem": "PyPI"}}' \
#   "https://api.osv.dev/v1/query"
# #+END_SRC
By commit hash:
#+BEAMER: \scriptsize
#+BEGIN_SRC
$ curl -X POST -d \
  '{"commit": "6879efc2c1596d11a6a6ad296f80063b558d5e0f"}' \
  "https://api.osv.dev/v1/query"
{"vulns":[{"id":"OSV-2020-484","summary":"Heap-buffer-overflow in AAT...
#+END_SRC
** We need a /global view/ of public code
:PROPERTIES:
:CUSTOM_ID: global-view
:END:
*** 
:PROPERTIES:
:BEAMER_env: block
:BEAMER_COL: .3
:END:
#+BEAMER: \centering\includegraphics[height=0.85\textheight]{cve-swh.png}
*** 
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .7
    :END:
    - OSV.dev is a great and useful service! ... but it doesn't know about
      /all/ code out there.
    - It crawls all the repos of projects affected by known vulnerabilities,
      ... but not all their forks (which can span multiple forges!).
    #+BEAMER: \end{block} \vspace{-1mm} \begin{block} \small
    Real-world example (one out of many we have identified): 
    - Linux kernel vulnerability: GSD-2022-1004193
    - Example of vulnerable commit: b13baccc3850ca8b8cccbf8ed9912dbaa0fdf7f3
    - Fix commit: a92d44b412e75dd66543843165e46637457f22cc
    - False negative commit in fork (Raspberry PI Linux):
      c7c7a1a18af4c3bb7749d33e3df3acdf0a95bbb5
** An universal knowledge base about public code vulnerabilities
:PROPERTIES:
:CUSTOM_ID: univ-kb
:END:
*** Vision
- *Software Heritage* is the perfect (and only) place where to build an
  universal knowledge base that maps known vulnerabilities to public code
  artifacts.
- We can provide an *open data API mapping SWHIDs to CVEs*, that knows about
  /all public code commits/ and can be leveraged to increase software security
  for everybody.
*** Roadmap
- Current status: working prototype that processes OSV.dev data and use it to
  "color" the entire SWH commit graph (~4 billion commits) with vulnerability
  information.
- Upcoming feature of the archive. (See: [[https://swhsec.github.io/][swhsec.github.io]])
* Selected highlight: building transparent LLMs                   
  \includepdf[pages=1-2]{AI-slides.pdf}
** Breaking news: CodeCommons \hfill BPI funded
*** Vision: create the world reference for LLMs for code
:PROPERTIES:
:BEAMER_ACT: +-
:END:
      + massive, transparent and up to date, with qualified information
      + traceability of the contents, code attribution
*** How: quantum leap for the Software Heritage archive
:PROPERTIES:
    :BEAMER_act: <2->
:END:
      + licence and language information, project and code indicators
      + big data infrastructure
      + ... and much more
*** Consortium
:PROPERTIES:
    :BEAMER_act: <2->
:END:
     \hfill Inria, CEA, Software Heritage, Tweag, ...
*** Opportunity
:PROPERTIES:
    :BEAMER_act: <3->
:END:
     \hfill membership of the user advisory board will open soon!
* Conclusion
** A unique opportunity \hfill https://softwareheritage.org
*** Software Heritage is                                            :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    - *vendor neutral*
    - *open source*
    - a *worldwide* initiative 
    - a *long term* initiative
#+BEAMER: \pause
*** Software Heritage enables                                       :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    - long term *archival*
    - *reference* for reproducibility
    - a *global software knowledge base*
    - *mutualisation* of cost
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEAMER: \pause
*** Call to action
    - support Software Heritage as shared open infrastructure, join the ALIG group
    - get involved with the new applications
*** vspace                                                  :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+latex: \vspace{-0.5em}      
*** Annual report 2023
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .3
    :END:
    #+latex: \href{https://annex.softwareheritage.org/public/annual-reports/2023/SoftwareHeritageReport_2023.pdf}{\includegraphics[width=.3\linewidth]{2023-report-swh.png}}
    \hfill
    #+latex: \raisebox{2.1em}{\qrcode[height=4em]{https://annex.softwareheritage.org/public/annual-reports/2023/SoftwareHeritageReport_2023.pdf}}
*** 5 years in 5 minutes
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .3
    :END:
    #+latex: \href{https://www.youtube.com/watch?v=Ez4xKTKJO2o}{\includegraphics[width=.6\linewidth]{5years-video.png}}
    \hfill
    #+latex: \raisebox{1.8em}{\qrcode[height=4em]{https://www.youtube.com/watch?v=8RdA5TxOAGo}}

* Background on key concepts on identifiers
** Identification vs Location \hfill an example is worth a thousand words
*** Identification of a book                                                             :B_block:
    :PROPERTIES:
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
    \vspace{.85em}
    #+ATTR_LATEX: :width .8 \linewidth
    file:ISBN.png
    \vspace{1em}
    Goal: identify a book
    - one ISBN number per published book
    - ISO 2108 standard specification
    #+BEAMER: \pause
*** Location of (a copy of) a book                                                             :B_block:
    :PROPERTIES:
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
    #+ATTR_LATEX: :width .8 \linewidth
    file:bookshelf.png
    Goal: find (a copy of) a book
    - many locations (locations can change!)
    - many approaches for call numbers
    #+BEAMER: \pause
  # Dewey is a classification schema, not a location schema
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** 
    \hfill we are interested in *identification*, not in location
** Extrinsic vs Intrinsic identifiers in a nutshell                :noexport:
*** Extrinsic identifiers: no /per se/ relation with the designated Object
     A /register/ keeps the correspondence between the identifier and the object
     - pre-internet era :: passport number, social security number, ISBN, ISSN, etc.
     - internet era :: DOI, Handle, Ark, PURLs, RRID, etc.\pause
*** Intrinsic identifiers: derived from the designated Object
   /No register/ needed to keep the correspondence between the identifier and the object
   - pre-internet era :: musical notation, chemical notation (/NaCl/ is table salt)\pause
   - internet era :: cryptographic hashes for distributed software development, Bitcoin\pause
*** 
   \hfill more in [[https://www.softwareheritage.org/2020/07/09/intrinsic-vs-extrinsic-identifiers/][this dedicated blog post]] (with pointers to literature)
** Extrinsic vs Intrinsic identifiers
*** In a nutshell \hfill (for more info see [[https://www.softwareheritage.org/2020/07/09/intrinsic-vs-extrinsic-identifiers/][this dedicated blog post]])
    Main difference: how the /relation/ between /identifier/ and /designated
    object/ is created and maintained. \pause /Persistence/ is a key desired property.\pause
#+BEGIN_EXPORT latex
\begin{center}
\begin{tabular}{|r|c||c|}
\cline{2-3}
\multicolumn{1}{c|}{} & \multicolumn{1}{c||}{\bf Extrinsic} & \multicolumn{1}{c|}{\bf Intrinsic}\\
\hline
relation & register & convention\\
\hline
persistence & external\footnote{"persistence... is a function of \emph{administrative} care" \href{https://tools.ietf.org/html/rfc3650}{RFC 3650 (Handle System Overview, 2003)}}
            & internal\\
\hline
\hline
pre-internet & passport number, & Music/Chemistry notations\\
             & ISBN, SSN, etc.  & \emph{e.g. NaCl is table salt}\\
\hline
internet era & DOI, Handle, Ark, etc. & cryptographic hashes\\
             &                        & \emph{e.g.: git, bitcoin, SWHID}\\
\hline
\end{tabular}
\end{center}
#+END_EXPORT
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
    #+BEAMER: \pause
*** 
   \vspace{-3em}
   \hfill Software development has /massively adopted/ intrinsic identifiers /since 2006/\pause\\
   \hfill Git, GitHub, GitLab, pull requests, Guix, Nix, etc. /all rely on/ intrinsic identifiers\pause\\
   \hfill replicability/reproducibility need intrinsic identifiers\pause\\
   \hfill SWHID is /directly compatible/ with git!
** Choosing identifiers: a matter of granularity \hfill [[http://doi.org/10.15497/RDA00053][10.15497/RDA00053]]
  #+LATEX: \centering\forcebeamerstart
  #+LATEX: \only<1>{\includegraphics[width=0.8\linewidth]{Granularity-Level-animated-0.png}\\\centering Top concept layers vs. bottom artifact layers}
  #+LATEX: \only<2>{\includegraphics[width=0.8\linewidth]{Granularity-Level-animated-1.png}\\\centering Extrinsic identifiers are key for the concept layers}
  #+LATEX: \only<3>{\includegraphics[width=0.8\linewidth]{Granularity-Level-animated-2.png}\\\centering Intrinsic identifiers are key for the artifact layers}
#  #+LATEX: \only<4>{\includegraphics[width=0.8\linewidth]{Granularity-Level-animated-3.png}\\\centering In some cases, extrinsic identifiers can be added too, even if redundant}
  #+LATEX: \forcebeamerend
  
** SWHID computation: a worked example
   # variant of #+INCLUDE: "../../common/modules/under-the-hood-pictures.org::#merkleanimation" :only-contents t :minlevel 3
    #+LATEX: \centering\forcebeamerstart
    #+LATEX: \only<1>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/merkle_1.pdf}}}
    #+LATEX: \only<2>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/contents.pdf}}}
    #+LATEX: \only<3>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/merkle_2_contents.pdf}}}
    #+LATEX: \only<4>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{directories-pre.pdf}}}
    #+LATEX: \only<5>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/directories.pdf}}}
    #+LATEX: \only<6>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/merkle_3_directories.pdf}}}
    #+LATEX: \only<7>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/revisions.pdf}}}
    #+LATEX: \only<8>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/merkle_4_revisions.pdf}}}
    #+LATEX: \only<9>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/releases.pdf}}}
    #+LATEX: \only<10>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/merkle_5_releases.pdf}}}
    #+LATEX: \only<11>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/snapshots.pdf}}}
    #+LATEX: \forcebeamerend

* END
\end{document}
* Extrinsic identifiers are fragile
#
# URLS are not good tracers
#
#+INCLUDE: "../../common/modules/software-fragile-plain.org::#urlhalflife" :minlevel 2

#
# DOI is not a solution
#
#+INCLUDE: "../../common/modules/doi-analysis.org::#doiexplained" :minlevel 2
* Why Software 
** Software Heritage is /radically different/
   #+INCLUDE: "../../common/modules/under-the-hood-pictures.org::#automation" :only-contents t :minlevel 3
** Software Heritage is /radically different/, cont'd
*** A quick tour as a user
    - <+-> *designed for source code:* [[https://archive.softwareheritage.org][Browse]] (e.g. [[https://archive.softwareheritage.org/swh:1:cnt:64582b78792cd6c2d67d35da5a11bb80886a6409;origin=https://github.com/virtualagc/virtualagc;visit=swh:1:snp:3c074afad81ad6b14d434b96e705e01d184752cf;anchor=swh:1:rev:007c2b95f301f9438b8b74d7993b7a3b9a66255b;path=/Luminary099/THE_LUNAR_LANDING.agc;lines=245-261/][Apollo 11 excerpt]], see also [[https://www.softwareheritage.org/2019/07/20/archiving-and-referencing-the-apollo-source-code/][Apollo 11 blog post]]) like on a developer platform, not a document archive!
    - <+-> *reference source code:* all granularities, using SWHIDs ([[https://docs.softwareheritage.org/devel/swh-model/persistent-identifiers.html][full specification available online]])
      + compare Fig. 1 and conclusions in [[http://www.dicosmo.org/Articles/2012-DaneluttoDiCosmo-Pcs.pdf][the 2012 version]] and [[https://www.dicosmo.org/share/parmap_swh.pdf][the updated version]]
      + SWHID in [[https://www.dicosmo.org/Articles/2020-ReScienceC.pdf][a replication experiment]]
      + [[https://www.softwareheritage.org/howto-archive-and-reference-your-code/][guidelines]] and [[https://dx.doi.org/10.1007/978-3-030-52200-1_36][a full article]]
      + SWHIDs /guarantee integrity/ like in /blockchains/\\
	demo if time left:
	  1. download a version of a project for a given SWHID
	  2. compute locally the SWHID with swh-identify
	  3. check that the computed id match the given one
** Software Heritage is /radically different/, cont'd
*** Getting software archived
    - <+-> *[[https://softwareheritage.org/features/][automated harvesting]]:* over [[https://archive.softwareheritage.org/][200 million software origins]], your researchers' work [[https://twitter.com/gabrielaltay/status/1300218789762662401][may already be there]] (actually, [[https://archive.softwareheritage.org/browse/search/?q=bitbucket+galtay&with_visit=true&with_content=true][here]])!
    - <+-> *universal archive:* /all/ source code [[https://archive.softwareheritage.org/][from /all/ platforms]] (BitBucket, GitHub, GitLab, your own forge, etc.)
      + [[https://save.softwareheritage.org][trigger archival]] of /any code/ in one click with [[https://www.softwareheritage.org/browser-extensions/][the *updateswh* browser extension]] 
      + [[https://archive.softwareheritage.org/api/1/][use webhooks]] to automatically archive /your code/ (a [[https://github.com/marketplace/actions/save-to-software-heritage][GitHub action]] is available too)
      + [[https://archive.softwareheritage.org/][journals, libraries, open access portals]] may /deposit sourcecode and metadata/
	* Example [[http://www.ipol.im/pub/art/2020/300/][article from IPOL]]
	* Example [[https://elifesciences.org/articles/82535][article from eLife]]
** A look at some adoption indicators
*** From [[https://www.canal-u.tv/sites/default/files/medias/fichiers/2022/02/5.3.%20Logiciels.%20Harrison.pdf][Melissa Harrison's OSEC 2022 talk]]                          :B_block:
    :PROPERTIES:
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
   #+ATTR_LATEx: :width .8 \linewidth
   file:osec-numbers.png
   #+BEAMER: \pause	
*** [[https://replicabilitystamp.org][replicabilitystamp.org]]
    :PROPERTIES:
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
   #+ATTR_LATEx: :width .8\linewidth
   file:replicabilitystamp-preview.png
   #+BEAMER: \pause	
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** 
    HAL+SWH [[https://www.ouvrirlascience.fr/wp-content/uploads/2022/10/Passeport_Codes-et-logiciels_WEB.pdf][in the Open Science software booklet]]
*** Funding agencies recommendations [[https://anr.fr/fileadmin/aap/2023/aapg-2023-V2.0_MAJ20220921.pdf][ANR 2023 guidelines]] (p. 17)
   #+ATTR_LATEx: :width .8 \linewidth
   file:anr-recommendations.png
