#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Software Heritage: a Brief Update
#+AUTHOR: Roberto Di Cosmo & Stefano Zacchiroli
#+BEAMER_HEADER: \date[LLW 2018]{18 April 2018\\LLW\\Barcelona}
#+DATE: 20 April 2018

#+INCLUDE: "../../common/modules/prelude.org"
#+INCLUDE: "../../common/modules/169.org"

#+BEAMER_HEADER: \institute[Software Heritage]{Software Heritage, roberto@dicosmo.org, zack@upsilon.cc}

* Software Heritage
  #+INCLUDE: "../../common/modules/swh-goals-oneslide-vertical.org::#goals" :minlevel 2
** Architecture (simplified)                                       :noexport:
    #+BEAMER: \begin{center}
    #+BEAMER:   \mode<beamer>{\only<1>{\includegraphics[width=\extblockscale{.9\textwidth}]{swh-dataflow-merkle-listers.pdf}}}
    #+BEAMER:   \only<2-3>{\includegraphics[width=\extblockscale{.9\textwidth}]{swh-dataflow-merkle.pdf}}
    #+BEAMER: \end{center}
#+BEAMER: \pause
#+BEAMER: \pause
   - crawling + normalizing
   - full development history permanently archived
   - origins: GitHub (automated), Debian (automated), Gitorious, Google Code, GNU
   - ~150Tb raw contents, ~10Tb graph (7+Bn nodes, 60+Bn edges)
  #+INCLUDE: "../../common/modules/status-extended.org::#dataflow" :minlevel 2
  #+INCLUDE: "../../common/modules/status-extended.org::#archive" :minlevel 2
** News
*** Features
    - direct deposit (use case: complete & corresponding source)
    - batch download
    - archive browsing / way back machine
      https://archive.softwareheritage.org \\
      username/pwd: fosdem/2018
*** Institutional
    Software Heritage Foundation
    - public charity
    - being created right now (ETA: weeks)
** Growing support
  :PROPERTIES:
  :CUSTOM_ID: support
  :END:
*** Landmark Inria Unesco agreement, April 3rd, 2017
 #+BEGIN_EXPORT latex
    \includegraphics[width=\extblockscale{.25\linewidth}]{inria-logo-new} \hfill
    \includegraphics[width=\extblockscale{.35\linewidth}]{unesco-accord} \hfill
    \includegraphics[width=\extblockscale{.2\linewidth}]{unesco}\\[1em]
 #+END_EXPORT
*** Sharing the vision                                              :B_block:
  :PROPERTIES:
  :CUSTOM_ID: endorsement
  :BEAMER_COL: .5
  :BEAMER_env: block
  :END:
   #+LATEX: \begin{center}\includegraphics[width=\extblockscale{1.4\linewidth}]{support.pdf}\end{center}
*** Contributing to the mission                                     :B_block:
    :PROPERTIES:
    :CUSTOM_ID: sponsors
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
    #+LATEX: \begin{center}\includegraphics[width=\extblockscale{.4\linewidth}]{inria-logo-new}\end{center}
   #+LATEX: \begin{center}
   #+LATEX: \includegraphics[width=\extblockscale{.2\linewidth}]{sponsors-levels.pdf}
   #+LATEX: \includegraphics[width=\extblockscale{1.1\linewidth}]{sponsors.pdf}
   #+LATEX: \end{center}
#   - sponsoring / partnership :: \hfill \url{sponsorship.softwareheritage.org}
