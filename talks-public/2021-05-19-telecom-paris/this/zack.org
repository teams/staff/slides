
** About me
   - Associate Professor (/Maître de conférences/), Université de Paris
   - on leave (/délégation/) at Inria
   - Free/Open Source Software activist (20+ years)
   - Debian Developer & Former 3x Debian Project Leader
   - Former Open Source Initiative (OSI) director
   - Software Heritage co-founder & CTO

  #+BEAMER: \vfill \pause

*** Research path
    1) Formal methods for ensuring the quality of software upgrades (Mancoosi
       project)

       Industry adoption: Debian, OPAM, Eclipse P2
    2) Formal methods for automated upgrade planning in the cloud (Aeolus
       project)

       Industry adoption: Mandriva, Kyriba
    3) Large-scale software evolution analysis (Debsources platform)
    4) Very-large-scale source code analysis and preservation (Software
       Heritage)

       → this talk
