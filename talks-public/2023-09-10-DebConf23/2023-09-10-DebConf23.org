#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Software Heritage
#+SUBTITLE: building a community to safeguard the Software Commons
# #+AUTHOR: Roberto Di Cosmo
# #+EMAIL: roberto@dicosmo.org @rdicosmo @swheritage
#+BEAMER_HEADER: \date[2023-09-10]{September 10th 2023\\DebConf 23}
#+BEAMER_HEADER: \title[Software Heritage~~~softwareheritage.org]{Software Heritage}
#+BEAMER_HEADER: \institute[Software Heritage]{Software Heritage}
#+BEAMER_HEADER: \author[Nicolas Dandrimont~~~~ olasd@softwareheritage.org ~~~~ @olasd@mastodon.opportunis.me ~~~~ (CC-BY 4.0)]{Nicolas Dandrimont}
# #+BEAMER_HEADER: \setbeameroption{show notes on second screen}
#+BEAMER_HEADER: \setbeameroption{hide notes}
#+KEYWORDS: software heritage legacy preservation knowledge mankind technology
#+LATEX_HEADER: \usepackage{tcolorbox}
#+LATEX_HEADER: \definecolor{links}{HTML}{2A1B81}
#+LATEX_HEADER: \hypersetup{colorlinks,linkcolor=,urlcolor=links}

#
# prelude.org contains all the information needed to export the main beamer latex source
# use prelude-toc.org to get the table of contents
#

#+INCLUDE: "../../common/modules/prelude-toc.org" :minlevel 1
#+INCLUDE: "../../common/modules/169.org"

# +LaTeX_CLASS_OPTIONS: [aspectratio=169,handout,xcolor=table]

#+LATEX_HEADER: \usepackage{bbding}
#+LATEX_HEADER: \DeclareUnicodeCharacter{66D}{\FiveStar}

#
# If you want to change the title logo it's here
#
# +BEAMER_HEADER: \titlegraphic{\includegraphics[width=0.5\textwidth]{SWH-logo}}

# aspect ratio can be changed, but the slides need to be adapted
# - compute a "resizing factor" for the images (macro for picblocks?)
#
# set the background image
#
# https://pacoup.com/2011/06/12/list-of-true-169-resolutions/
#
#+BEAMER_HEADER: \pgfdeclareimage[height=90mm,width=160mm]{bgd}{swh-world-169.png}
#+BEAMER_HEADER: \setbeamertemplate{background}{\pgfuseimage{bgd}}
#+LATEX: \addtocounter{framenumber}{-1}
* Software and Source Code
** Software is all around us
  :PROPERTIES:
  :CUSTOM_ID: softwareispervasive
  :END:
   #+latex: \begin{center}
   #+ATTR_LATEX: :width .5\linewidth
file:software-center.pdf
   #+latex: \end{center}
** Software is built from /Source Code/ 
   #+INCLUDE: "../../common/modules/source-code-different-short.org::#softwareisdifferent" :only-contents t :minlevel 3
** Software source code as a key asset of Humankind
*** :B_column:BMCOL:
    :PROPERTIES:
    :BEAMER_col: .53
    :BEAMER_env: column
    :END:
    #+ATTR_LATEX: :width .7\linewidth
    file:UNESCOParisCallMeeting.png
    UNESCO, Inria, Software Heritage invite\\
    [[https://en.unesco.org/news/experts-call-greater-recognition-software-source-code-heritage-sustainable-development][40 international experts meet in Paris]] ...
    #+BEAMER: \pause
*** :B_column:BMCOL:
    :PROPERTIES:
    :BEAMER_col: .5
    :BEAMER_env: column
    :END:
    #+ATTR_LATEX: :width .65\linewidth
    file:paris_call_ssc_cover.jpg
    [[https://en.unesco.org/foss/paris-call-software-source-code][The call is published on February 2019]]\pause
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** 
    :PROPERTIES:
    :BEAMER_COL: 1.06
    :BEAMER_env: block
    :END:
   \hfill /“Recognise software source code as a fundamental enabler in all aspects of human endeavour"/
** (Open Source) Software is /precious technical and scientific knowledge/
#+BEAMER: \vspace{-.5em}   
#    Deep knowledge embodied in complex software systems
*** Yuval Noah Harari (on COVID 19)
    \hfill /“The real antidote [to the pandemic] is/ scientific knowledge /and/ global cooperation.”
#+BEAMER: \pause
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEAMER: \vspace{-.5em}   
*** Software powers modern scientific research                   :B_picblock:
    :PROPERTIES:
    :BEAMER_opt: pic=papermountain,width=.55\linewidth, leftpic=true
    :BEAMER_env: picblock
    :BEAMER_COL: .5
    :END:      
   \mbox{}\\
The top 100 papers\\
   \mbox{}\\
/[...] the vast majority describe experimental methods or software that have become essential in their fields./\\
\endgraf \hfill [[http://www.nature.com/news/the-top-100-papers-1.16224][Nature, October 2014]]
# http://www.nature.com/news/the-top-100-papers-1.16224
#+BEAMER: \pause   
# https://twitter.com/harari_yuval/status/1238126897587986432?lang=en
*** We can still talk to the early inventors                        :B_picblock:
   :PROPERTIES:
    :BEAMER_opt: pic=KnuthHistory-slim.png,width=.45\linewidth, leftpic=true
    :BEAMER_env: picblock
    :BEAMER_COL: .48
   :END:
   \mbox{}\\
   /"Telling historical stories is the best way to teach. It's much easier to understand something if you know the threads it is connected to."/
   \mbox{}\\
   \mbox{}\\
   \mbox{}\hfill Donald E. Knuth\\
   \mbox{}\hfill Len Shustek\\
   \mbox{}\hfill [[https://doi.org/10.1145/3442377][CACM, January 2021]]
#+BEAMER: \pause   
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEAMER: \vspace{-.5em}   
*** 
    \hfill We need a /dedicated infrastructure/ to preserve and share /all/ this knowledge!
** Enhancing software Reuse, Security and Transparency
#+BEAMER: \vspace{-.5em}   
#    Deep knowledge embodied in complex software systems
*** Software complexity is growing... \hfill it is important to Know Your SoftWare (KYSW)      :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=myriadsources.png,width=.4\linewidth,leftpic=true
    :END:
    \pause *Regulation on Software Updates*\\
            Recording [...] software versions relevant to a vehicle type\\
    \mbox{}\hfill [[https://undocs.org/ECE/TRANS/WP.29/2020/80][UN Regulations on Cybersecurity, June 2020]]\\

    \pause *Politique publique de la donnée, des algorithmes et des codes sources* \\
    ...animer les ecosystèmes des...réutilisateurs du source code \\
    \mbox{}\hfill [[https://www.legifrance.gouv.fr/circulaire/id/45162][Circulaire du Premier Ministre, 27 Avril 2021, France]] \\
    \pause

    *Sec. 4. Enhancing Software Supply Chain Security* \\
    \hfill /ensuring and attesting, to the extent practicable, to the integrity and provenance of open source software/\\
    \mbox{}\hfill [[https://www.whitehouse.gov/briefing-room/presidential-actions/2021/05/12/executive-order-on-improving-the-nations-cybersecurity/][May 2021 POTUS Executive Order]]

*** vspace                                                  :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEAMER: \vspace{-.5em}
#+BEAMER: \pause   
*** 
    \hfill We need a /trusted/ knowledge base with /software provenance/ !
** Software source code is fragile
*** Endangered source code ...                                                          :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=fragilecloud.png, leftpic=true, width=.4\linewidth
    :END:
    - /link rot/: projects are created, moved around, removed
    - /data rot/: physical media with legacy software decay
    - /platform consolidation/ endangers repositories
      + *2015* Google Code and Gitorious.org shutdown: *~1M*
      + *2019* Bitbucket mercurial phase out: *~250.000*
      + *2022* GitLab.com: *remove inactive projects?*
#+BEAMER: \pause
*** ... is endangered knowledge!
    \hfill broken links and missing pieces in the /web of knowledge/ of humankind
#+BEAMER: \pause
*** Bottomline: we need a global, long term effort
    \hfill to build a /universal archive/ of /all software source code/\\
    \hfill and make it /sustainable/
* Software Heritage: a mission at the service of Humankind
** Software Heritage in a nutshell \hfill www.softwareheritage.org
#+BEAMER: \transdissolve
  #+latex: {\bf \emph{Unveiled in 2016}\vspace{-1em}}
  #+INCLUDE: "../../common/modules/swh-goals-oneslide-vertical.org::#goals" :only-contents t :minlevel 3
** Today: a /universal/ software archive, as a shared infrastructure
   #+INCLUDE: "../../common/modules/principles-compact.org::#sharedinfra" :only-contents t :minlevel 3
** An operational, evolving infrastructure
  \vspace{-.9em}
*** Harvest and archive
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    #+BEAMER: \begin{center}
    #+BEAMER:   \mode<beamer>{\only<1>{\includegraphics[width=1\linewidth]{swh-dataflow-merkle-listers.pdf}}}
    #+BEAMER:   \only<2->{\includegraphics[width=1\linewidth]{swh-dataflow-merkle.pdf}}
    #+BEAMER: \end{center}
\vspace{-1em}
#+BEAMER: \pause
#+BEAMER: \pause
   - [[https://save.softwareheritage.org][save.softwareheritage.org]]
   - [[https://deposit.softwareheritage.org][deposit.softwareheritage.org]]
#+BEAMER: \pause
# (HAL, IPOL)
*** Harvest and archive                                            :noexport:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
#+ATTR_LATEX: :width \linewidth
   file:swh-dataflow-merkle.pdf
\vspace{-1em}
#+BEAMER: \pause
   - [[https://save.softwareheritage.org][save.softwareheritage.org]]
   - [[https://deposit.softwareheritage.org][deposit.softwareheritage.org]]
# (HAL, IPOL)
#+BEAMER: \pause
*** Reference (25 billion SWHIDs)                                   :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    [[https://www.softwareheritage.org/2020/07/09/intrinsic-vs-extrinsic-identifiers/][Intrinsic, decentralised, cryptographically strong identifiers]]
\vspace{-1em}
#+ATTR_LATEX: :width 1.02\linewidth
    file:SWHID-v1.4_3.png
    Now [[https://www.softwareheritage.org/2020/05/13/swhid-adoption/][in SPDX 2.2, Wikidata]], ISO is coming

** Growing adoption

*** Adoption in Open Science                               :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .4
    :END:
    reference archive\hfill ([[https://twitter.com/gabrielaltay/status/1300218789762662401][example]])\\
    \hfill for research software
    #+BEAMER: \pause
*** Adoption in Industry and Public Administration         :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .6
    :END:
    reference archive and knowledge base\\
    \hfill for open source software
** A revolutionary infrastructure
   #+INCLUDE: "../../common/modules/swh-as-infrastructure.org::#oneslide" :only-contents t :minlevel 3
** A walkthrough
  \vspace{-.5em}
*** General
   - Browse [[https://archive.softwareheritage.org][the archive]], get and use SWHIDs, e.g. [[https://archive.softwareheritage.org/swh:1:cnt:64582b78792cd6c2d67d35da5a11bb80886a6409;origin=https://github.com/virtualagc/virtualagc;visit=swh:1:snp:3c074afad81ad6b14d434b96e705e01d184752cf;anchor=swh:1:rev:007c2b95f301f9438b8b74d7993b7a3b9a66255b;path=/Luminary099/THE_LUNAR_LANDING.agc;lines=245-261/][Apollo 11 excerpt]], [[https://archive.softwareheritage.org/swh:1:cnt:bb0faf6919fc60636b2696f32ec9b3c2adb247fe;origin=https://github.com/id-Software/Quake-III-Arena;visit=swh:1:snp:4ab9bcef131aaf449a7c01370aff8c91dcecbf5f;anchor=swh:1:rev:dbe4ddb10315479fc00086f08e25d968b4b43c49;path=/code/game/q_math.c;lines=549-572/][Quake III excerpt]]
   - [[https://save.softwareheritage.org][Trigger archival]] in one click with the [[https://www.softwareheritage.org/browser-extensions/][browser extension]]
*** Open Science
   - [[https://doc.archives-ouvertes.fr/en/deposit/deposit-software-source-code/][Curated deposit via HAL]], e.g.: 
      [[https://hal.archives-ouvertes.fr/hal-02130801][LinBox]], [[https://hal.archives-ouvertes.fr/hal-01897934][SLALOM]], [[https://hal.archives-ouvertes.fr/hal-02130729][Givaro]], [[https://hal.archives-ouvertes.fr/lirmm-02136558][SumGra]], [[https://hal.archives-ouvertes.fr/hal-02155786][Coq proof]], ...
   - Cite software [[https://www.softwareheritage.org/2020/05/26/citing-software-with-style/][with the biblatex-software style]], e.g.: [[http://www.ipol.im/pub/art/2020/300/][article from IPOL]]
#   - Example use in a research article: compare Fig. 1 and conclusions 
#      - in [[http://www.dicosmo.org/Articles/2012-DaneluttoDiCosmo-Pcs.pdf][the 2012 version]]
#      - in [[https://www.dicosmo.org/share/parmap_swh.pdf][the updated version]] using SWHIDs and Software Heritage
#   - Example use in a research article: extensive use of SWHIDs in [[https://www.dicosmo.org/Articles/2020-ReScienceC.pdf][a replication experiment]]
*** History of software: rescuing landmark legacy software
    \hfill see [[https://www.softwareheritage.org/swhap/][SWHAP process]], [[https://stories.softwareheritage.org/][Software Stories]], and [[https://www.softwareheritage.org/news/events/swhap_days_2022/][SWHAP Days 2022]]
*** Public code
   \hfill Archived source code from [[https://code.gouv.fr/#/repos][code.gouv.fr]]
** An international, non profit initiative\hfill for the long term
  :PROPERTIES:
  :CUSTOM_ID: support
  :END:
*** Sharing the vision                                              :B_block:
  :PROPERTIES:
  :CUSTOM_ID: endorsement
  :BEAMER_COL: .5
  :BEAMER_env: block
  :END:
   #+LATEX: \begin{center}{\includegraphics[width=\extblockscale{.4\linewidth}]{unesco_logo_en_285}}\end{center}
   #+LATEX: \vspace{-0.8cm}
   #+LATEX: \begin{center}\vskip 1em \includegraphics[width=\extblockscale{1.4\linewidth}]{support.pdf}\end{center}
    #+latex: \small And many more ...\\
    #+latex:\mbox{}~~~~~~~\tiny\url{www.softwareheritage.org/support/testimonials}
#+BEAMER: \pause
*** Donors, members, sponsors                                       :B_block:
    :PROPERTIES:
    :CUSTOM_ID: sponsors
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
   #+LATEX: \begin{center}\includegraphics[width=\extblockscale{.4\linewidth}]{inria-logo-new}\end{center}
   #+LATEX: \begin{center}
   # #+LATEX: \includegraphics[width=\extblockscale{.2\linewidth}]{sponsors-levels.pdf}
   #+LATEX: \colorbox{white}{\includegraphics[width=\extblockscale{1.4\linewidth}]{sponsors.pdf}}
   #+LATEX: \end{center}
#   - sponsoring / partnership :: \hfill \url{sponsorship.softwareheritage.org}
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
* Diving deeper into our Features
** Long-term storage
*** Object Storage
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .55
    :END:
*Challenge*
- Storage of and efficient access to the individual versions of archived source code files
- 15 billion objects, median size of 3 kB

*Implementation*
- multiple backends implementing a [[https://docs.softwareheritage.org/devel/swh-objstorage/][common interface]] (fs, Ceph, public clouds)
- [[https://wiki.softwareheritage.org/wiki/A_practical_approach_to_efficiently_store_100_billions_small_objects_in_Ceph][object packing on regular distributed block storage]]
- maintenance of 3 copies stored at different locations on different backends
#+BEAMER: \pause
*** Graph storage
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .45
    :END:
*Challenge*
- storing the global history of software development
- resilient storage & efficient traversals with >10^10 vertices, >10^11 edges

*Implementation*
- store the vertices and edges for resilience in a [[https://docs.softwareheritage.org/devel/swh-storage/]["simple" Key-Value store]] (backed by Cassandra or PostgreSQL)
- [[https://docs.softwareheritage.org/devel/swh-graph/][compressed in-memory snapshots]] of the graph for traversals

** Mirroring
*** Multiple challenges to the resilience of the infrastructure
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
- intentional or unintentional destruction
- legal framework changes
- permanence of the umbrella organization
*** Building a mirror network
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
- Avoid single point of (organizational) failure
- Host content under different jurisdictions
- Current deployments:
  - [[https://www.enea.it/en/news-enea/news/technology-at-enea-the-first-european-institutional-mirror-of-software-heritage][ENEA (Italy)]]
  - [[https://faircore4eosc.eu/eosc-core-components/eosc-software-heritage-mirror][GRNet for EOSC (Greece)]]
** Archival framework
*** Listers
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
Listing the contents of hosting platforms to make their contents available for archival
- VCS forges (GitHub, GitLab, Gitea, Forgejo, pagure, ...)
- Distributions (Debian-based, Red Hat-based, language ecosystems like PyPI, Rubygems, ...)
- [[https://docs.softwareheritage.org/devel/swh-lister/][Documentation of swh.lister]]
#+BEAMER: \pause
*** Loaders
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
Converting source code, as published, into our common data model and ingesting the data
- VCS (git, Subversion, CVS, bazaar, mercurial), with full development history
- Software releases (as tarballs or individual files) from other distribution platforms
- [[https://archive.softwareheritage.org/coverage/][Archive coverage]]
** On-demand archival
*** Save code now
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
- https://save.softwareheritage.org/
- Separate, autoscaling infrastructure
- Support for all available VCSes
#+BEAMER: \pause
*** Save Code Now APIs
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
- public access: [[https://archive.softwareheritage.org/api/1/origin/save/doc/][Documentation of API]]
- raised rate-limits available to partners
- [[https://www.softwareheritage.org/browser-extensions/][Browser extension]]
** Curated deposit of software
*** Push-based architecture
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
- Based on the interoperable SWORD archival protocol
- Push a set of tarballs, receive a pointer to the SWHID of the archived software
- [[https://docs.softwareheritage.org/devel/swh-deposit/][Documentation of swh.deposit]]
#+BEAMER: \pause
*** Audience
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
- Academic partners: automatic ingestion of source code deposited alongside open access research papers
- Industry partners: deposit of the complete corresponding source code for GPLv3 compliance in products

** The /\href{https://unesdoc.unesco.org/ark:/48223/pf0000371017}{SWHAP}/ process, with UNESCO and University of Pisa
   #+INCLUDE: "../../common/modules/swh-acquisition-process.org::#swhap" :only-contents t :minlevel 3
** Software Stories, with UNESCO, University of Pisa and DataCurrent
   #+INCLUDE: "../../common/modules/swh-acquisition-process.org::#stories" :only-contents t :minlevel 3

** Discovery of new software origins
*** Add Forge now
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
*Challenge*

discover the wealth of smaller scale hosting platforms based on GitLab, Forgejo, Gitea, Gogs and other self-hostable forge software

*Implementation*

Submission form on the main archive website for users to submit new forges they've discovered, worflow for ingestion

[[https://https://archive.softwareheritage.org/add-forge/request/help/][Documentation of Add forge now]]
#+BEAMER: \pause
*** Automation
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
- dedicated infrastructure for sanity checking the submission, performing the first listing and loading of all the contents
- configurable concurrency (to avoid overloading smaller platforms)
- Work in progress automation of the full process through GitLab pipelines
** Large scale analysis
*** Graph compression pipeline
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
*Challenge*

efficient queries on a graph that has hundreds of billions of edges?

*Implementation*

- Compress it and hold that representation in memory!
- From tens of terabytes of raw data, to a data structure that can be held in a few hundred GB of RAM
- [[https://docs.softwareheritage.org/devel/swh-graph/][Documentation of swh.graph]]
#+BEAMER: \pause
*** Dataset exports
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
- Run your own queries on the full graph of software development
- Available for download, or for direct use on Amazon Athena
- [[https://docs.softwareheritage.org/devel/swh-dataset/][Documentation of swh.dataset]]
- [[https://www.softwareheritage.org/legal/bulk-access-terms-of-use/][Ethical charter for bulk access]]
* Software Heritage Infrastructure
** From humble beginnings

#+ATTR_LATEX: :width 0.7\linewidth
file:server-swh.jpg
** To a very large scale deployment

#+ATTR_LATEX: :width \linewidth
file:202302-server-racks.jpg

** A very large scale deployment
*** Our main storage requirements in a nutshell
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
- More than 1PB of source code files (replicated 3 times by Software Heritage)
- More than 100 TB used for (resilient) storage of the graph
- Infrastructure support for mirroring: 100 TB kafka deployment (~30TB of data used)
#+BEAMER: \pause
*** Infrastructure components
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
- on-site:
  - 4 racks of servers, all running Debian
    - proxmox cluster
    - kubernetes clusters (bare metal and VMs)
    - special purpose clusters (PostgreSQL, kafka, Cassandra, elasticsearch, ...)
  - 1 rack of network equipment
#+BEAMER: \pause
- off-site:
  - Ceph objstorage cluster: 2 racks
  - Azure resources (blob storage, bare VMs, AKS clusters)
  - AWS resources (S3 for objstorage and dataset exports, Athena for queries)

** Automation
*** Low-level deployments
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
- Everything running Debian, of course
- terraform for provisioning [[https://gitlab.softwareheritage.org/swh/infra/swh-sysadmin-provisioning/-/tree/master/proxmox/terraform?ref_type=heads][virtual machines]] and [[https://gitlab.softwareheritage.org/swh/infra/swh-sysadmin-provisioning/-/tree/master/azure/terraform?ref_type=heads][cloud resources]]
- puppet for deployment of OS components
#+BEAMER: \pause
*** Kubernetes
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
- Using [[https://www.rancher.com/][Rancher]] and [[https://docs.rke2.io/][rke2]] as K8s distribution
- [[https://argo-cd.readthedocs.io/en/stable/][ArgoCD]] for continuous deployment ([[https://gitlab.softwareheritage.org/swh/infra/ci-cd/k8s-clusters-conf/][configuration]])
- [[https://gitlab.softwareheritage.org/swh/infra/ci-cd/swh-charts][Helm charts for deploying the Software Heritage stack]]
- Jenkins pipelines for image building and Helm chart updates
- Images pushed to the GitLab container registry
** Team organization
*** A regular Free Software project
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
- Organized in public
- [[https://gitlab.softwareheritage.org/][Browse our GitLab CE instance on gitlab.softwareheritage.org]]
- [[https://jenkins.softwareheritage.org/][Jenkins]] for CI automation (WIP: GitLab CI)
- [[https://www.softwareheritage.org/community/developers][real-time discussions]] on IRC bridged to matrix; open mailing lists
- [[https://docs.softwareheritage.org/][Browse our documentation on docs.softwareheritage.org]]
#+BEAMER: \pause
*** Infrastructure as code
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
- All our deployment manifests are published in the open as well
- [[https://docs.softwareheritage.org/sysadm/getting-started/setup-repositories.html][Repository access info]]
** A growing and active community
  #+BEAMER: \vspace{-.9em}
*** [[https://softwareheritage.org/people/][Team]]
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    #+ATTR_LATEX: :width \linewidth
    file:2023-team-large.jpg
    #+BEAMER: \pause \vspace{2mm}
*** [[https://softwareheritage.org/ambassadors/][Ambassadors]]
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    #+ATTR_LATEX: :width .8\linewidth
    file:ambassadors-2023.jpg
    #+BEAMER: \pause
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
    \vspace{-.5em}
*** [[https://softwareheritage.org/grants/][Foundations and grantees]]                            :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_opt: pic=sloan-nlnet-vertical.png,width=.33\linewidth, leftpic=true
    :BEAMER_COL: .5
    :END:
#    #+ATTR_LATEX: :width .48\linewidth
#    file:sloan-nlnet.png
    - Castalia, CottageLabs
    - EasterEggs, OcamlPro
    - Octobus, Sperling, Tweag.io
    - DataCurrent
 #+BEAMER: \pause \vspace{-2mm}
*** [[https://softwareheritage.org/mirrors/][Mirrors and storage partners]]
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    /“Let us save what remains: ... by such a/
     /multiplication of copies, as shall place them beyond the reach of accident.”/\\
    \hfill  — Thomas Jefferson\\
    \vspace{.5em}
     Enea, GRNET, ... \hfill CEA, RedHat
*** vspace                                                  :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
    #+BEAMER: \vspace{-2mm}
* Concluding remarks
** A call to realize a grand vision
*** Bring together academia, industry, civil society and governments to build
    \hfill /"a global infrastructure for open and better software at the service of humankind"/
#+BEAMER: \pause
*** Software Heritage is the first brick ...                        :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    - *vendor neutral*, multi-stakeholder
    - *open source*, *non profit*
    - a *worldwide* initiative 
    - a *long term* initiative
#+BEAMER: \pause
*** ... that will enable                                            :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    - *archival*, *reference*, *integrity*
    - *qualification*, *sharing* and *reuse*
    - a *global software knowledge base*
    - test and deploy *world class tooling*
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
    #+BEAMER: \pause
*** A lot more is needed
    \hfill Software Heritage can be the /catalyser/ of a way bigger undertaking
    #+BEAMER: \pause
*** You can help!    
    \hfill use, adopt, advocate, contribute, fund, support, join
** A call to realize a grand vision
   #+BEAMER: \transdissolve
*** Bring together academia, industry, civil society and governments to build
    \hfill /"a global infrastructure for open and better software at the service of humankind"/
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEGIN_EXPORT latex
 \begin{center}
 \includegraphics[width=.6\linewidth]{SWH-logo.pdf}
 \end{center}
 \begin{center}
 {\large \url{www.softwareheritage.org} \hspace{4em} \url{@swheritage}}
 \end{center}
#+END_EXPORT
*** The Library of Alexandria of code                            :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_COL: 0.42
    :BEAMER_OPT: pic=clock-spring-forward.png,width=.45\linewidth,leftpic=true
    :END:
    - recover the past
    - structure the future
    - rebuild trust in science
*** The Very Large Telescope for Source code                     :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_COL: 0.5
    :BEAMER_OPT: pic=atacama-telescope.jpg,width=.5\linewidth,leftpic=true
    :END:
    - explore and reuse
    - better, more secure software
    for society as a whole
