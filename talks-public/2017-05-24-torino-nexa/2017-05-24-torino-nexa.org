#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Software Heritage
#+SUBTITLE: Archiving the Software Commons for Fun and Social Benefit
#+BEAMER_HEADER: \date[24/05/2017, Nexa]{24 May 2017\\ Nexa Center for Internet \& Society --- Turin, Italy}
#+AUTHOR: Stefano Zacchiroli
#+DATE: 24 May 2017
#+EMAIL: zack@upsilon.cc

#+INCLUDE: "../../common/modules/prelude.org" :minlevel 1
#+INCLUDE: "../../common/modules/169.org"
#+BEAMER_HEADER: \titlegraphic{\includegraphics[width=\extblockscale{0.7\textwidth}]{SWH-logo+motto}}
#+BEAMER_HEADER: \institute[Software Heritage]{University Paris Diderot \& Inria\\\href{mailto:zack@upsilon.cc}{\tt zack@upsilon.cc}}

#+LATEX_HEADER: \usepackage{bbding}
#+LATEX_HEADER: \DeclareUnicodeCharacter{66D}{\FiveStar}
#
* The Software Commons
** Software is everywhere
   #+latex: \begin{center}
   #+ATTR_LATEX: :width 0.7\linewidth
   file:software-center.pdf
   #+latex: \end{center}
   #+INCLUDE: "../../common/modules/source-code-different-long.org::#thesourcecode" :minlevel 2
   #+INCLUDE: "../../common/modules/source-code-different-short.org::#softwareisdifferent" :minlevel 2
** Our Software Commons
   #+INCLUDE: "../../common/modules/foss-commons.org::#commonsdef" :only-contents t
   #+BEAMER: \pause
*** Source code is /a precious part/ of our commons
     \hfill are we taking care of it?
* Software Heritage
  #+INCLUDE: "../../common/modules/swh-motivations-foss.org::#main" :only-contents t :minlevel 2
** The (sad) state of software-related reproducibility
*** Reproducibility                                           :BMCOL:B_block:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :BEAMER_env: block
    :END:
    - how are we doing, regarding reproducibility, in \emph{software}?\\
      #+BEAMER: \footnotesize
      e.g., Collberg et al., Measuring reproducibility in computer systems
      research (2014)
      #+BEAMER: \normalsize
    - not a CS-only issue, software is everywhere in science
    - all round reproducibility is difficult; source code availability a
      requirement
*** Scientific knowledge preservation                         :BMCOL:B_block:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :BEAMER_env: block
    :END:
    #+latex: \begin{center}
    #+ATTR_LATEX: :width \extblockscale{\linewidth}
    file:PreservationTriangle.png
    #+latex: \end{center}
*** Legenda (links are important!)
    :PROPERTIES:
    :BEAMER_act: +-
    :END:      
    - articles: ArXiv, HAL, ...
    - data: Zenodo, ...
    - software: ?
  #+INCLUDE: "../../common/modules/swh-overview-sourcecode.org::#mission" :minlevel 2
  #+INCLUDE: "../../common/modules/swh-functional-architecture.org::#phases" :minlevel 2
  #+INCLUDE: "../../common/modules/vision.org::#foundations" :only-contents t :minlevel 2
** Our principles
   #+latex: \begin{center}
   #+ATTR_LATEX: :width 0.7\linewidth
   file:SWH-as-foundation-slim.png
   #+latex: \end{center}
#+BEAMER: \pause
*** Open approach 					      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.4
    :BEAMER_env: block
    :END:
    - 100% FOSS
    - transparency
*** In for the long haul 				      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.4
    :BEAMER_env: block
    :END:
    - replication
    - non profit

* Current status
** The archive is ready and growing
    #+BEAMER: \begin{center}\includegraphics[width=\extblockscale{1.2\linewidth}]{growth.png}\end{center}
*** Our current sources
    - GitHub
    - Debian, GNU
    - WIP: Gitorious, Google Code, Bitbucket
  #+BEAMER: \pause
*** 
    \hfill The biggest source code archive already, ... and growing daily!
  #+INCLUDE: "../../common/modules/status-extended.org::#features" :minlevel 2
* Community
  #+INCLUDE: "../../common/modules/endorsement.org::#endorsement" :minlevel 2
  #+INCLUDE: "../../common/modules/swh-sponsors.org::#sponsors" :minlevel 2
** Going global
*** April 3rd, 2017: landmark UNESCO/Inria agreement...
#+BEGIN_EXPORT latex
  \includegraphics[width=\extblockscale{.25\linewidth}]{inria-logo-new} \hfill
  \includegraphics[width=\extblockscale{.35\linewidth}]{unesco-accord} \hfill
  \includegraphics[width=\extblockscale{.2\linewidth}]{unesco}\\[1em]
  \mbox{}\hfill
  \includegraphics[width=\extblockscale{.2\linewidth}]{rdc-fh-ib} \hfill
  \includegraphics[width=\extblockscale{.15\linewidth}]{SWH-logo_share} \hfill
  \includegraphics[width=\extblockscale{.2\linewidth}]{swh-team-2017-04-03}\hfill
  \mbox{}\\
  \begin{center}
  \footnotesize
  \url{www.softwareheritage.org/?p=11623}
  \end{center}
#+END_EXPORT
*** 
    *Next step:* 27-28 Sep 2017: UNESCO/Inria conference in Paris\hfill
** You can help!
    #+BEAMER: \vspace{-1mm}
*** Coding
    - \url{www.softwareheritage.org/community/developers/}
    - \url{forge.softwareheritage.org} --- *our own code*
    #+BEAMER: \vspace{-3mm}
*** Current development priorities                                 :noexport:
    | ٭٭٭ | listers for unsupported forges, distros, pkg. managers |
    | ٭٭٭ | loaders for unsupported VCS, source package formats    |
    | ٭٭  | Web UI: GitHub/Internet Archive-like browsing UI       |
    | ٭   | content indexing and search                            |
    #+BEAMER: \pause \vspace{-1mm}
*** Working groups
    - \url{wiki.softwareheritage.org/index.php?title=Working_groups} ---
      *working groups*
      - ELIE (Ethical and Legal Issues and Environment) WG: legal and policy
        issues
      - copyright exceptions for archival, privacy issues in preserving source
        code and development metadata, etc.
*** Join us
    - \url{www.softwareheritage.org/jobs} --- *job openings*
    - \url{wiki.softwareheritage.org/index.php?title=Internships} --- *internships*
* Conclusion
** Conclusion
*** Software Heritage is
    - a /reference archive/ of /all/ FOSS ever written
    # - a fantastic new tool for /research/ software
    - a unique /complement/ for /development platforms/
    - an international, open, nonprofit, /mutualized infrastructure/
    - at the service of our community, at the service of society
*** Come in, we're open!
    \url{www.softwareheritage.org} --- /sponsoring/, /job openings/ \\
    \url{wiki.softwareheritage.org} --- /internships/, /working groups/ \\
    \url{forge.softwareheritage.org} --- /our own code/
    #+BEAMER:  \vfill \flushright {\Huge Questions?} \vfill
* Appendix                                                      :B_appendix:
  :PROPERTIES:
  :BEAMER_env: appendix
  :END:
** Archiving goals
   Targets: VCS repositories & source code releases (e.g., tarballs)
*** We DO archive
    - file *content* (= blobs)
    - *revisions* (= commits), with full metadata
    - *releases* (= tags), ditto
    - where (*origin*) & when (*visit*) we found any of the above
    # - time-indexed repo *snapshots* (i.e., we never delete anything)
    … in a VCS-/archive-agnostic *canonical data model*
*** We DON'T archive
    # - diffs → derived data from related contents
    - homepages, wikis
    - BTS/issues/code reviews/etc.
    - mailing lists
    Long term vision: play our part in a /"semantic wikipedia of software"/
* Technical details
  #+INCLUDE: "../../common/modules/status-extended.org::#architecture" :only-contents t
  #+INCLUDE: "../../common/modules/status-extended.org::#merkletree" :minlevel 2
  #+INCLUDE: "../../common/modules/status-extended.org::#merklerevision" :only-contents t
  #+INCLUDE: "../../common/modules/status-extended.org::#giantdag" :only-contents t
  #+INCLUDE: "../../common/modules/status-extended.org::#api" :only-contents t
