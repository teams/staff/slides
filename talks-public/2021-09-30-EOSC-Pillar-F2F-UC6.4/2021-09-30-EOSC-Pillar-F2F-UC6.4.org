#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: UC 6.4: Software Heritage
#+SUBTITLE: Software source code preservation, reference and access.
#+BEAMER_HEADER: \title{Software Heritage at EOSC-Pillar F2F meeting}
#+AUTHOR: David Douard
#+EMAIL: david.douard@softwareheritage.org
#+BEAMER_HEADER: \date[Sepember 30th, 2021]{September 30th, 2021\\[-1em]}
#+BEAMER_HEADER: \title[www.softwareheritage.org]{EOSC-Pillar UC 6.4: Software Heritage}
#+BEAMER_HEADER: \institute[]{\\\href{mailto:david.douard@softwareheritage.org}{\tt david.douard@softwareheritage.org}}
#+BEAMER_HEADER: \author[David Douard]{ David Douard\\[1em]%
#+BEAMER_HEADER: Software engineer\\Inria, Software Heritage\\[-1em]}
# #+BEAMER_HEADER: \setbeameroption{show notes on second screen}
#+BEAMER_HEADER: \setbeameroption{hide notes}
#+KEYWORDS: software heritage legacy preservation knowledge mankind technology deposit

#
# prelude.org contains all the information needed to export the main beamer latex source
# use prelude-toc.org to get the table of contents
#

#+INCLUDE: "../../common/modules/prelude-toc.org" :minlevel 1


#+INCLUDE: "../../common/modules/169.org"

# +LaTeX_CLASS_OPTIONS: [aspectratio=169,handout,xcolor=table]
#+LATEX_HEADER: \usepackage{bbding}
#+LATEX_HEADER: \usepackage{tcolorbox}
#+LATEX_HEADER: \DeclareUnicodeCharacter{66D}{\FiveStar}


#
# If you want to change the title logo it's here
#
#+BEAMER_HEADER: \titlegraphic{\includegraphics[width=0.7\textwidth]{SWH-logo+EOSC-Pillar}}

# aspect ratio can be changed, but the slides need to be adapted
# - compute a "resizing factor" for the images (macro for picblocks?)
#
# set the background image
#
# https://pacoup.com/2011/06/12/list-of-true-169-resolutions/
#
#+BEAMER_HEADER: \pgfdeclareimage[height=90mm,width=160mm]{bgd}{swh-world-eosc-pillar.png}
#+BEAMER_HEADER: \setbeamertemplate{background}{\pgfuseimage{bgd}}

#+LATEX_HEADER: \usepackage{supertabular}
#+LATEX_HEADER: \newcommand{\sponsor}[2]{{\bf #1}, #2}
#+LATEX_HEADER: \newcommand{\teamster}[2]{{\textcolor{red}{#1}}, #2}


* Introduction
** Use Case Description

Leveraging the experience of Software Heritage, this task aimed at design and
pilot a solution for the preservation of massive collections of software source
code (billions of files with links to publications) into EOSC eTDR service
(European Trusted Digital Repository). More specifically, this task will:

#+ATTR_BEAMER: :environment enumerate
- standardize a persistent identifier schema (PID) for referencing billions of
  archived software artifacts,
- develop a deposit API for research software archival,
- develop an access API for retrieval of archived software,
- provide access and help EOSC partners to integrate the above APIs and Services,
- develop a pilot to fully store the software archive onto existing EOSC
  infrastructure.

** Software Heritage in a Nutshell
*** Main Objectives
- *Collect* Save all the (open source) software source code
- *Preserve* Keep all the (archived) software source code *forever*
- *Share* Make every piece of source code identifiable and freely available

** Software is all around EOSC-Pillar

***  Open Science cannot exists without 				      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :BEAMER_env: block
    :END:
    - open source software
    - accessible source code
    - identifiable source code
    - FAIR source code

***  Source is everywhere in EOSC 					      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.45
    :BEAMER_env: block
    :END:
    - data production
    - data processing
    - HPC / compute as a service

#+BEAMER: \pause

*** Is every piece of this software
- clearly identified?
#+BEAMER: \pause
- accessible?
#+BEAMER: \pause
- today?
#+BEAMER: \pause
- in the future?


** Software definition
*** Encyclopædia Britannica
    “Software, instructions that tell a computer what to do.
    Software comprises the entire set of programs, procedures, and routines
    associated with the operation of a computer system.
    The term was coined to differentiate these instructions
    from hardware—i.e., the physical components of a computer system.”
    \hfill *[[https://www.britannica.com/technology/software][link]]*
#+BEAMER: \pause
***  Software as a concept 				      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :BEAMER_env: block
    :END:
    - software project / entity
#+BEAMER: \pause
    - the creators and the community around it
#+BEAMER: \pause
    - the software idea / algorithms / solutions
#+BEAMER: \pause

***  Software artifact 					      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.45
    :BEAMER_env: block
    :END:
    - the executable (or binary) of each version for a specific environment
#+BEAMER: \pause
    - the *software source code* for each revision


** Much more complex than it seems
*** Software is complex
 - Structure :: monolithic/composite; self-contained/external dependencies
 - Lifetime :: one-shot/long term
 - Community :: one man/one team/distributed community
 - Authorship :: complex set of roles
 - Authority :: institutions/organizations/communities/single person
#+BEAMER: \pause
*** Various granularities
 - Exact status of the source code :: for reproducibility, e.g.
#+latex:            \emph{``you can find at \href{https://archive.softwareheritage.org/swh:1:cnt:cdf19c4487c43c76f3612557d4dc61f9131790a4;lines=146-187/}{swh:1:cnt:cdf19c4487c43c76f3612557d4dc61f9131790a4;lines=146-187} the core algorithm used in this article''}

 - (Major) release :: \emph{``This functionality is available in OCaml version 4''}

 - Project :: \emph{``Inria has created OCaml and Scikit-Learn''}.


** The knowledge is in the source code!
#+INCLUDE: "../../common/modules/source-code-different-short.org::#thesourcecode" :only-contents t :minlevel 3
** Source code is /special/
*** /Executable/ and /human readable/ knowledge \hfill copyright law
    /“Programs must be written for people to read, and only incidentally for machines to execute.”/\\
    \hfill Harold Abelson
#+BEAMER: \pause
*** Software /evolves/ over time
    - projects may last decades
    - the /development history/ is key to its /understanding/
#+BEAMER: \pause
*** Complexity :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=python3-matplotlib.pdf, width=.6\linewidth
    :END:
    - /millions/ of lines of code
    - large /web of dependencies/
      + easy to break, difficult to maintain
    - sophisticated /developer communities/

** Software Source Code human readable and executable knowledge
file:NOLI_SE_TANGERE.png

** Software in research has different roles
*** Multiple facets, it can be seen as:
  - a tool
  - a research outcome or result
  - the object of research
#+Beamer: \pause

*** By identifying the software role
\hfill we can decide how to /treat/ it
** What is at stake \hfill in increasing order of difficulty
\vspace{-7pt}
*** Archival
    Research software artifacts must be properly *archived*\\
    \hfill make it sure we can /retrieve/ them (/reproducibility/)
#+BEAMER: \pause
*** Identification
    Research software artifacts must be properly *referenced*\\
    \hfill make it sure we can /identify/ them (/reproducibility/)
#+BEAMER: \pause
*** Metadata
    Research software artifacts must be properly *described*\\
    \hfill make it easy to /discover/ them (/visibility/)
#+BEAMER: \pause
*** Citation
    Research software artifacts must be properly *cited* /(not the same as referenced!)/\\
    \hfill to give /credit/ to authors (/evaluation/!)


* Software Heritage: the universal source code archive
#
# One slide motivation + goals
#+INCLUDE: "../../common/modules/swh-goals-oneslide-vertical.org::#goals" :minlevel 2
#

# Where we are today: endorsement
# ** Our principles \hfill iPres 2017 - \url{http://bit.ly/swhpaper}
# #+INCLUDE: "../../common/modules/principles-compact.org::#principlesstatus" :only-contents t :minlevel 3

**  Our principles \hfill iPres 2017 - \url{http://bit.ly/swhpaper}
  :PROPERTIES:
  :CUSTOM_ID: principlesstatus
  :END:
#+latex: \begin{center}
#+ATTR_LATEX: :width .8\linewidth
file:SWH-as-foundation-slim.png
#+latex: \end{center}
#+latex: \footnotesize\vspace{-3mm}
 #
 # #+BEAMER: \pause

 #+BEAMER: \pause
 #+latex: \centering
 #+ATTR_LATEX: :width \extblockscale{.8\linewidth}
 file:2021-09-archive-growth.png

** Under the hood: Automation, and storage
  :PROPERTIES:
  :CUSTOM_ID: automation
  :END:
   #+BEAMER: \begin{center}
   #+BEAMER:   \mode<beamer>{\only<1>{\includegraphics[width=\extblockscale{\textwidth}]{swh-dataflow-merkle-listers.pdf}}}
   #+BEAMER:   \only<2-3>{\includegraphics[width=\extblockscale{\textwidth}]{swh-dataflow-merkle.pdf}}
   #+BEAMER: \end{center}
   #+BEAMER: \pause
   #+BEAMER: \pause
   /Global development history/ *permanently archived* in a *uniform data model*
   - over *11 billion* unique source files from over *160 million* software projects
   - *~650 TB* (uncompressed) blobs, *~25 B* nodes, *~300 B* edges

* SWHID: Software Heritage Persistent Identifiers

** Much more than an archive!
   # R. C. Merkle, A digital signature based on a conventional encryption
   # function, Crypto '87
   #+BEAMER: \vspace{-3mm}
***** Merkle tree (R. C. Merkle, Crypto 1979)                    :B_picblock:
      :PROPERTIES:
      :BEAMER_opt: pic=merkle, leftpic=true, width=.7\linewidth
      :BEAMER_env: picblock
      :BEAMER_act:
      :END:
      Combination of
       - tree
       - hash function
***** Classical cryptographic construction
      - fast, parallel signature of large data structures
      - widely used (e.g., Git, blockchains, IPFS, ...)
      - *built-in deduplication*

** Our challenges in the PID landscape
  :PROPERTIES:
  :CUSTOM_ID: challenges
  :END:
*** Typical properties of systems of identifiers
   \hfill uniqueness, non ambiguity, persistence, abstraction (opacity)
#+BEAMER: \pause
*** Key needed properties from our use cases
   - gratis :: identifiers are free (billions of objects)
   - integrity :: the associated object cannot be changed (sw dev, /reproducibility/)
   - no middle man :: no central authority is needed  (sw dev, /reproducibility/)
#+BEAMER: \pause
   - we could not find systems with both *integrity* and *no middle man*!
   - Intrinsic, decentralised, cryptographically strong identifiers =  *SWHIDs*

** The SWHID schema
  # TODO: drawing with swh:1:cnt:xxxxxxx "exploded" and explained
  #+LATEX: \centering\forcebeamerstart
  #+LATEX: \only<1>{\includegraphics[width=\linewidth]{SWH-ID-1.png}}
  #+LATEX: \only<2>{\includegraphics[width=\linewidth]{SWH-ID-2.png}}
  #+LATEX: \only<3>{\includegraphics[width=\linewidth]{SWH-ID-3.png}}
  #+LATEX: \forcebeamerend

** (Quick) Demo time
*** Apollo 11 source code
   #+LATEX: \begin{center}
   #+LATEX: \includegraphics[width=0.5\linewidth]{apollo-11-cranksilly.png}
   #+LATEX: \end{center}
   # excerpt of routine that asks astronaut to turn around the LEM
#+BEAMER: \pause
***
    [[https://archive.softwareheritage.org/swh:1:cnt:64582b78792cd6c2d67d35da5a11bb80886a6409][swh:1:cnt:64582b78792cd6c2d67d35da5a11bb80886a6409]]
#+BEAMER: \pause
    [[https://archive.softwareheritage.org/swh:1:cnt:64582b78792cd6c2d67d35da5a11bb80886a6409;origin=https://github.com/virtualagc/virtualagc;lines=245-261/][;origin=https://github.com/virtualagc/virtualagc;lines=245-261]]




* Archiving source code in the Software Heritage Archive

** Ingestion in SWH

- 3 sources of source code software ingestion

#+LATEX: \pause

*** listers and loader
   :PROPERTIES:
   :BEAMER_COL: .5
   :BEAMER_env: block
   :END:
   - pull based
   - best effort
   - reponsibility of SWH
#+LATEX: \pause

*** save code now
   :PROPERTIES:
   :BEAMER_COL: .5
   :BEAMER_env: block
   :END:
   - push based
   - accessible to everyone
   - (light) curation from SWH
#+LATEX: \pause

*** deposit
- push based
- authenticated
- responsibility of the user/partner
- comes with metadata

** Listers and Loaders

 #+latex: \centering
 #+ATTR_LATEX: :width \extblockscale{.8\linewidth}
 file:2021-09-listers.png

** Save Code Now

*** Save code now on \hfill \url{https://archive.softwareheritage.org/save/}
    - git, svn or mercurial
    - intrinsic metadata files
    - complete history


#+latex: \begin{center}
#+ATTR_LATEX: :width \linewidth
file:webui-save-code-now.png
#+latex: \end{center}



** The deposit

- a deposited artifact is provided from one of the SWH partners which is
  regarded as a *trusted authority*,
- a deposited artifact *requires metadata* properties describing the source code
  artifact,
- a deposited artifact has a *codemeta* metadata entry attached to it,
- a deposited artifact has the *same visibility* on the SWH Archive than a
  collected repository,
- a deposited artifacts *can be searched* with its provided url property on the
  SWH Archive,
- the deposit API uses the *SWORD v2* API, thus requires some tooling to send
  deposits to SWH. These tools are provided with this repository.


** The deposit
  :PROPERTIES:
  :CUSTOM_ID: hal
  :END:
*** the deposit workflow
    :PROPERTIES:
    :BEAMER_COL: .5
    :END:
    #+latex: \begin{center}
    #+ATTR_LATEX: :width \linewidth
    file:deposit-communication-with-PID.png
    #+latex: \end{center}
#+LATEX: \pause

*** Deposit software (in HAL) \hfill [[http://hal.inria.fr/hal-01738741][poster]] :B_picblock:
    :PROPERTIES:
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
    *\hspace{1em}Generic mechanism:*
    - SWORD based
    - review process
    - versioning

#+BEAMER: \pause
    *\hspace{1em} How to do it:* \hfill  ([[http://bit.ly/swhdeposithalen][/guide/]])
    - deposit .zip or .tar.gz file with metadata

** Example: The deposit view in HAL
#+latex: \begin{center}
#+ATTR_LATEX: :width .8\linewidth
file:CGAL-3D.png
#+latex: \end{center}

** The metadata challenge - Software ontologies
  :PROPERTIES:
  :CUSTOM_ID: thesourcecode
  :END:

#+BEGIN_QUOTE
   “Ontologies are agreements, made in a social context, to accomplish some objectives.
  It's important to understand those objectives, and be guided by them.\"\\
     \hfill T. Gruber, The Pragmatics of Ontology, 2003
#+END_QUOTE

#+Beamer: \pause
*** What do we want to describe?
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_act: +-
    :END:
      + a software project?
      + a software artifact? a collection of artifacts?
      + With what terms or vocabulary?

** The Software Ontology /Touchstone/
***  Software Citation Principles \tiny (Smith et al. 2016)         :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_opt:
    :END:
    - *Importance* : first class citizen in the scholarly ecosystem
    - *Credit and attribution* : authors, maintainer
    - *Unique identification*: points to
       a unique, specific software version (DOI, Git SHA1 hash, etc..)
    - *Persistence* : identification beyond the lifespan of the software (swh-id)
    - *Accessibility*: url, publisher
    - *Specificity* : version, environment

# metadata landscape (one decomposed slide)
#+INCLUDE: "../../common/modules/metadata-landscape.org::#main" :only-contents t :minlevel 2

* Retrieving Source Code from the Archive

** Browsing the web application

#+latex: \begin{center}
#+ATTR_LATEX: :width .8\linewidth
file:swh-apollo11.png
#+latex: \end{center}

** Using the Vault

#+latex: \begin{center}
#+ATTR_LATEX: :width .8\linewidth
file:vault-download-menu.png
#+latex: \end{center}

#+INCLUDE: "../../common/modules/vault.org::#vault-short" :only-contents t :minlevel 2

** Using public APIs
#+latex: \begin{center}
#+ATTR_LATEX: :width .8\linewidth
file:swh-web-apis.png
#+latex: \end{center}

- freely accessible but with rate limiting
- rate-limit-free access is possible with authentication


* Use Case 6.4 Status

** Current Status

- *Persistent identifiers* for source code artefacts have been specified and
  their usage are being promoted for general adoption in scientific research
  for software citation.

- The *deposit API* for the Software Heritage has been developed and is available
  for registered partners.

- For non-registered partners, the *Save Code Now* feature of the Software
  Heritage Archive allows every one to ask for a public repository (git,
  mercurial, etc.) to be ingested in the Software Heritage Archive.

- *Access APIs* allowing to retrieve source code content from the Software
  Heritage Archive are available and documented.

- The integration of the Software Heritage Archive in the Vitam archiving
  solution provided by the CINES as part of the EOC eTDR service has been
  started recently and is in progress.

** Next Steps

- Document the Software Heritage Archive features and present them to all
  EOSC-Pillar partners to make sure every one is aware of the kind of service
  it provides and has the opportunity to integrate their tools and services
  with the Software Heritage Archive.

- [WIP] Finish the specification of the packages to be deposited in Vitam.

- [WIP] Implement a prototype of an automatic archiving service that consumes
  the (kafka) journal of the Software Heritage Archive to assemble packages to
  be deposited in Vitam.


** Integration with EOSC and EOSC-Pillar partners

- The Software Heritage has been registered in the [[https://marketplace.eosc-portal.eu/services/software-heritage-archive][EOSC catalog]]

- AAI integration has been tested and validated connecting Software Heritage's
  authentication system with Indigo-IAM.

- Some EOSC-Pillar partners (especially UC6.2 and UC6.3) plan to have their
  EOSC-Pillar related source code repositories (eg. D4Science EarthScience VRE
  or GitHub repositories) harvested and archived by Software Heritage.

- Some work is in progress to integrate Software Heritage, via a SWHID, as
  BinderHub / JupyterHub source of easy to run and reproducible Jupyter
  notebooks.


* Conclusion

** Come in, we're open!
*** Software Heritage                                         :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_opt:
    :BEAMER_env: block
    :BEAMER_col: 0.4
    :END:
    - universal source code archive
    - intrinsic identifiers (SWHIDs)
    - open, non profit, long term
    - infrastructure for Open Science
#+BEAMER: \pause
*** You can help                                            :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :BEAMER_env: block
    :END:
    - use SWH and save /relevant/ source code
    - build on SWH (see swmath.org and ipol.im)
    - contribute to SWH- it is /open source/
    - spread the word: become a [[https://www.softwareheritage.org/ambassadors][Software Heritage Ambassador]]
** Thank you

*** Thank you
#+BEGIN_EXPORT latex
 \begin{center}
 \includegraphics[width=.5\linewidth]{SWH-logo.pdf}
 \end{center}
 \begin{center}
 \vfill
 {\Large Thank you!}
 \end{center}
#+END_EXPORT

*** contact: david.douard@softwareheritage.org                                                                :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :END:
  #+BEGIN_EXPORT latex
  \begin{thebibliography}{Foo Bar, 1969}
  \footnotesize
  \bibitem{DiCosmo2020b} P. Alliez, R. Di Cosmo, B. Guedj, A. Girault, M. Hacid, A. Legrand, N. Rougier
  \newblock Attributing and Referencing (Research) Software: Best Practices and Outlook From Inria
  \newblock Computing in Science \& Engineering, 22 (1), pp. 39-52, 2020, ISSN: 1558-366X
  \bibitem{DiCosmo2020a} Roberto Di Cosmo, Morane Gruenpeter, Stefano Zacchiroli
  \newblock Referencing Source Code Artifacts: a Separate Concern in Software Citation
  \newblock Computing in Science \& Engineering, 2020, ISSN: 1521-9615
  \end{thebibliography}
  #+END_EXPORT
