#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: The Software Heritage Filesystem (SwhFS)
#+BEAMER_HEADER: \date[02 Dec 2020]{02 December 2020\\\sout{5} \sout{10} 15 min talk - Inria Paris}
#+AUTHOR: Thibault Allançon
#+DATE: 02 December 2020
#+EMAIL: haltode@gmail.com

#+INCLUDE: "../../common/modules/prelude.org" :minlevel 1
#+INCLUDE: "../../common/modules/169.org"
#+BEAMER_HEADER: \pgfdeclareimage[height=90mm,width=160mm]{bgd}{world-169.png}
#+BEAMER_HEADER: \titlegraphic{}
#+BEAMER_HEADER: \institute[EPITA \& Inria]{EPITA \& Inria Paris}
#+BEAMER_HEADER: \author[Thibault Allançon]{Thibault Allançon\\ {\small\tt haltode@gmail.com\\ @haltode}\\ joint work with Stefano Zacchiroli, and Antoine Pietri}
#+BEAMER_HEADER: \title[The Software Heritage Filesystem (SwhFS)]{The Software Heritage Filesystem (SwhFS)}
#+BEAMER_HEADER: \hypersetup{colorlinks,urlcolor=blue,linkcolor=magenta,citecolor=red,linktocpage=true}


* Introduction
** Motivations
    - Existing SWH archive interface:
        - \url{https://archive.softwareheritage.org/browse/}
        - \texttt{swh-web-client}
        - \texttt{swh-vault}
        - \texttt{swh-graph}
    #+BEAMER: \vfill \pause
    - Existing version control filesystems:
        - \href{https://github.com/AUEB-BALab/RepoFS}{RepoFS}
        - \href{https://github.com/microsoft/VFSForGit}{VFS for Git}
        - \href{https://ieeexplore.ieee.org/document/6261115}{GitOD}
        - ...
    #+BEAMER: \vfill \pause
*** SwhFS in a nutshell
    A user-space filesystem that gives access to the Software Heritage archive
    as a POSIX filesystem.

* SwhFS presentation
** Key features
    1. VCS-agnostic thanks to the Software Heritage archive model
    2. Does not require any local repository copy for exploration
    3. Can mount any type of Software Heritage artifacts using its SWHID

** Architecture
    #+BEAMER: \vspace{-3mm}
#+BEAMER: \begin{center}\includegraphics[width=1\textwidth]{this/arch-container.png}\end{center}
    #+BEAMER: \vspace{-3mm}

** Architecture
    - **Front-end**:
        - \texttt{swh-fuse} user-space daemon
        - FUSE API with pyfuse3 bindings
    - **FS Layout**:
        - top-level directories: archive/, meta/, origin/
        - one unique layout per artifact types
    - **Back-end**:
        - Asyncio
        - Web API, Graph API
        - SQLite local cache

** Demo
    #+BEAMER: \Huge\centering
    Live demo

* Conclusion
** Wrapping up

    - SwhFS: a POSIX user-space filesystem to naviguate SWH archive
    - VCS-agnostic, no local repository copy needed
    -
        #+BEAMER: \small
        #+BEAMER: \texttt{\$ pip install swh.fuse} \\
        #+BEAMER: \texttt{\$ mkdir swhfs} \\
        #+BEAMER: \texttt{\$ swh fs mount swhfs/}

** What's next

    - Large-scale validation of \texttt{swh-fuse} using subgraphs of the archive
    - Once swh-graph has edge labels, use Graph API to efficiently populate entries during intensive queries (e.g.: ls -R)
    - Batch API to query multiple similar objects at once

** Useful links

    - \url{https://forge.softwareheritage.org/source/swh-fuse/}

*** See full paper for more details (under review)
  #+BEGIN_EXPORT latex
  \begin{thebibliography}{Foo Bar, 1969}
  \small \vspace{-2mm}
  \bibitem{swh-fuse-icse2021} Thibault Allançon, Antoine Pietri, Stefano Zacchiroli
  \newblock The Software Heritage Filesystem (SwhFS): Integrating Source Code
  Archival with Development
  \newblock ICSE 2021, 42nd Intl. Conf. on Software Engineering, Demonstration track
  \end{thebibliography}
  draft: {\footnotesize \url{https://upsilon.cc/~zack/stuff/paper-swh-fuse.pdf}}
  #+END_EXPORT

* Appendix                                                       :B_appendix:
  :PROPERTIES:
  :BEAMER_env: appendix
  :END:
