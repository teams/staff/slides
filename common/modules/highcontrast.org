#
# 2024-09 High contrast theme, blank background, minimal footer, remove shadows
#

# REMOVE BACKGROUND
#+BEAMER_HEADER: \setbeamertemplate{background}{}
#+BEAMER_HEADER: \setbeamercolor{block title}{bg=,fg=SwhLightRed}
#+BEAMER_HEADER: \setbeamercolor{block body}{bg=}

# USE DEFAULT BLOCK STYLE

#+latex_header: \setbeamertemplate{blocks}[default]

# MAKE MINIMAL FOOTER
#+latex_header: \setbeamertemplate{footline}
#+latex_header: {
#+latex_header:   \leavevmode%
#+latex_header:   \hspace*{\fill} % Pushes the content to the right
#+latex_header:   \hbox{%
#+latex_header:     \usebeamerfont{author in head/foot}\insertshortauthor%~~\beamer@ifempty{\insertshortinstitute}{}{(\insertshortinstitute)}
#+latex_header:     \usebeamerfont{title in head/foot}\hspace*{2em}%-\hspace*{2em}
#+latex_header:     \usebeamerfont{title in head/foot}\insertshorttitle{}\hspace*{2em}
#+latex_header:     \usebeamerfont{date in head/foot}\insertshortdate{}\hspace*{2em}
#+latex_header:     \usebeamerfont{date in head/foot}\insertframenumber{} / \inserttotalframenumber\hspace*{2ex} 
#+latex_header:   }%
#+latex_header:   \vskip1pt% Minimal vertical space
#+latex_header: }
