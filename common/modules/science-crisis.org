#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1

* The Science Crisis
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** What causes cancer?
   Is everything we eat associated with cancer?\\
   Schoenfeld and Ioannidis, /Amer. Jour. of Clinical Nutrition/, 2013.
   #+latex: \begin{center}
   #+ATTR_LATEX: :width \extblockscale{.6\linewidth}
   file:2013-Ioannidis-cancer-cures-causes.png
   #+latex: \end{center}
   #+BEAMER: \pause
*** Inconsistency
    /an incompatibility between two propositions that cannot both be true/
** Corrupt data!
   Gene name errors are widespread in the scientific literature
   Ziemann, Eren and El-Osta, /Genome Biology/, 2016.
   #+latex: \begin{center}
   #+ATTR_LATEX: :width \extblockscale{.45\linewidth}
   file:2016-genome-excel-01.png
   #+latex: \hfill
   #+ATTR_LATEX: :width \extblockscale{.5\linewidth}
   file:2016-genome-excel-02.png
   #+latex: \end{center}
   #+BEAMER: \pause
*** Corruption
    /The process by which a computer database or program becomes debased by alteration or the introduction of errors/
** Doctored data?
   #+latex: \begin{flushleft}
   #+ATTR_LATEX: :width \extblockscale{.6\linewidth}
   file:2016-09-dukefraud.png
   #+latex: \end{flushleft}
   #+BEAMER: \pause
*** Fraud
    /wrongful or criminal deception intended to result in financial or personal gain/

# J.P. Ioannidis. /Why Most Published Research Findings Are False/ PLoS Med. 2005.
** What are drugs good for?
   #+latex: \begin{flushleft}
   #+ATTR_LATEX: :width \extblockscale{.8\linewidth}
   file:2011-drug-unreliable.png
   #+latex: \end{flushleft}
   #+BEAMER: \pause
*** Non reproducibile results
    \hfill ... and this is just one of the worrying replication studies!
#    *reproducibility*: /the ability of an experiment or study to be duplicated,
#   either by the same researcher or by someone else working independently/
** We face a science crisis
*** Our temple of science is crumbling 				 :B_picblock:
    :PROPERTIES:
    :BEAMER_opt: pic=templescience, leftpic=true, width=.4\linewidth
    :BEAMER_env: picblock
    :END:      
    - inconsistencies
    - data corruption
    - fraud
    - non reproducible findings...
    (picture from Nature, Sep. 2015)
   #+BEAMER: \pause
*** 
    "Sub-prime science"? (Nicholas Humprey)
** The world starts noticing
   #+latex: \begin{center}
   #+ATTR_LATEX: :width \extblockscale{.3\linewidth}
   file:2013-10-19-how-science-goes-wrong-economist-cover.jpg
   #+latex: \hfill
   #+ATTR_LATEX: :width \extblockscale{.6\linewidth}
   file:2016-05-09-at-11.20.56-AM-JohnOliver.png
   #+latex: \hfill \hl{October 2013} \hfill \mbox{} \hfill \hl{John Oliver, \emph{Science} May 2016} \hfill\mbox{}
   #+latex: \end{center}
   #+BEAMER: \pause
*** Time to go back to the basics!
    \hfill what is /science/?
