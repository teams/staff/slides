#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1
* Software Source code evolution
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** ~ 50 years, a lightning fast growth
  :PROPERTIES:
  :CUSTOM_ID: apollolinux
  :END:
   # https://www.wired.com/2015/10/margaret-hamilton-nasa-apollo/
*** Apollo 11 Guidance Computer (~60.000 lines), 1969            :B_picblock:
    :PROPERTIES:
    :BEAMER_opt: pic=Margaret_Hamilton, width=.2\linewidth, leftpic=true
    :BEAMER_env: picblock
    :BEAMER_act:
    :END:      
     "When I first got into it, nobody knew what it was that we were doing. It was like the Wild West."\\
     \mbox{}\hfill Margaret Hamilton\\
#     https://github.com/chrislgarry/Apollo-11
#     https://archive.softwareheritage.org/api/1/origin/git/url/https://github.com/chrislgarry/Apollo-11
 #+BEAMER: \pause
*** Linux Kernel                                                 :B_picblock:
    :PROPERTIES:
    :BEAMER_opt: pic=Linuxlinecount, width=.6\linewidth, leftpic=true
    :BEAMER_env: picblock
    :BEAMER_act:
    :END:      
#    \mbox{}\\
#    \vfil
    \hfil ... now in your pockets!\hfill\mbox{}\\
#    \vfill
#    \pause \hfill \alert{are we taking care of all this?}
