#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1
* Support
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** Growing Support
  :PROPERTIES:
  :CUSTOM_ID: support
  :END:
*** Raising awareness: landmark agreement, 3/4/2017; grand opening, 7/6/2018
 #+BEGIN_EXPORT latex
    \includegraphics[width=\extblockscale{.25\linewidth}]{inria-logo-new} \hfill
    \includegraphics[width=\extblockscale{.3\linewidth}]{unesco-accord} \hfill
    \includegraphics[width=\extblockscale{.35\linewidth}]{unesco-opening} \hfill
    \includegraphics[width=\extblockscale{.2\linewidth}]{unesco}\\[1em]
 #+END_EXPORT
*** Sharing the vision                                              :B_block:
  :PROPERTIES:
  :CUSTOM_ID: endorsement
  :BEAMER_COL: .55
  :BEAMER_env: block
  :END:
   #+LATEX: \begin{center}\includegraphics[width=\extblockscale{1.4\linewidth}]{support.pdf}\end{center}
*** See more                                                       :noexport:
    \hfill\tiny\url{http:://www.softwareheritage.org/support/testimonials}
*** Sponsoring our work                                             :B_block:
    :PROPERTIES:
    :CUSTOM_ID: sponsors
    :BEAMER_COL: .42
    :BEAMER_env: block
    :END:
    #+LATEX: \begin{center}\includegraphics[width=\extblockscale{.5\linewidth}]{inria-logo-new}\end{center}
    #+LATEX: \begin{center}\includegraphics[width=\extblockscale{1.3\linewidth}]{sponsors.pdf}\end{center}
#   - sponsoring / partnership :: \hfill \url{sponsorship.softwareheritage.org}
