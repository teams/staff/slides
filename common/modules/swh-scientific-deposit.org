#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1
* Software Heritage for Scientific Publishing
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** The research software (deposit) use case
  :PROPERTIES:
  :CUSTOM_ID: hal
  :END:
*** Deposit software in HAL \hfill \url{http://hal.inria.fr/hal-01738741} :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=deposit-communication.png,width=.63\linewidth,leftpic=true
    :END:
#+LATEX: \pause
    *\hspace{1em}Generic mechanism:*
    - SWORD based
    - review process
    - versioning
#    - /industry chimes in/ (details on demand)
#+BEAMER: \pause
    *\hspace{1em} How to do it:*
    - *today*: deposit .zip or .tar.gz file ([[http://bit.ly/swhdeposithalen][/guide/]])
    - *tomorrow*:
      - provide /SWH id/ and metadata
      - include /metadata file/ for automatic metadata extraction
      - ...
#+BEAMER: \pause
***
   September 2018: *open to all* on \url{https://hal.archives-ouvertes.fr/}
