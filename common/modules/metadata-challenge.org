#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1
* Explore the metadata landscape
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** Preliminary questions
  :PROPERTIES:
  :CUSTOM_ID: thesourcecode
  :END:

#+BEGIN_QUOTE
   “Ontologies are agreements, made in a social context, to accomplish some objectives.
  It's important to understand those objectives, and be guided by them.\"\\
     \hfill T. Gruber, The Pragmatics of Ontology, 2003
#+END_QUOTE

#+Beamer: \pause
*** /Software Ontology/
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_act: +-
    :END:
      + What is software ?
      + With what terms should we describe a /software artifact/?
      + What about /software source code/ ?

** The metadata challenge
#+latex: \begin{center} \huge{What is software ?} \end{center}
#+BEAMER: \pause
***  Software as a concept 				      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :BEAMER_env: block
    :END:
    - software project / entity
#+BEAMER: \pause
    - the creators and the community around it
#+BEAMER: \pause

***  Software artifact 					      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :BEAMER_env: block
    :END:
    - the binaries for different environments
#+BEAMER: \pause
    - the *software source code* for each version

** This is /software/?
#+latex: \begin{center}
#+ATTR_LATEX: :width .38\linewidth
file:this-is-not-a-pipe.png
#+latex: \end{center}

\hfill  What about **/software source code/** ?

** Metadata about Software Source Code
  :PROPERTIES:
  :CUSTOM_ID: softwareisdifferent
  :END:
*** Software metadata objectives
    manage, share, discover, archive /software source code/
#+BEAMER: \pause

*** Use cases 				      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col:
    :BEAMER_env: block
    :END:
    - *semantic search*: find software by author, version, keywords
    - browse /source code/ with context information
    - cite and be cited
#+BEAMER: \pause

*** 							    :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** LOV- Linked open vocabularies
    \hfill /“Vocabularies provide the *semantic glue* enabling data to become *meaningful data*. ”/


** Where is the metadata available ?
  :PROPERTIES:
  :CUSTOM_ID: where-is-the-metadata-1
  :END:
*** Catalogs and registries 					    :B_block:
    :PROPERTIES:
    :BEAMER_COL: .4
    :BEAMER_env: block
    :END:
   #+ATTR_LATEX: :width \extblockscale{\linewidth}
    - libraries.io
    - OpenHub
    - OntoSoft


*** Publisher's repositories 					    :B_block:
    :PROPERTIES:
    :BEAMER_COL: .4
    :BEAMER_env: block
    :END:
   #+ATTR_LATEX: :width \extblockscale{\linewidth}
    - GitHub
    - Bitbucket
    - SourceForge

#+BEAMER: \pause
*** advantages and drawbacks 					    :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_opt:
    :END:

      |              | registries              | repositories                   |
      |--------------+-------------------------+--------------------------------|
      | accuracy     | - not created by author | + added by authors/maintainers |
      | completeness | + very detailed         | - not a priority               |
      | longevity    | - depends on registry   | - depends on publisher         |

**  Where is the metadata available ?
  :PROPERTIES:
  :CUSTOM_ID: where-is-the-metadata-2
  :END:
*** in the /software source code/ itself 			    :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_opt:
    :END:
  - package management file
  - CITATION file
  - .About file
  - codemeta.json file

#+BEAMER: \pause
*** advantages and drawbacks 					    :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_opt:
    :END:
  |              | metadata file                                         |
  |--------------+-------------------------------------------------------|
  | accuracy     | + created by author and evolves with code             |
  | completeness | - depends on the authors knowledge of metadata        |
  | longevity    | + not dependent on platform (repository or registry ) |

#+BEAMER: \pause
***
    \hfill *Bottomline:* to insure the archival of metadata, keep it *in* the data
