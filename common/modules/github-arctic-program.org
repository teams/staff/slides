#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1

* GitHub Arctic Program- a strategy for archiving code
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
**  Joining forces in the urgent effort to preserve humankind’s source code.
# - The partners in the Github arctic project
***          :B_block:BMCOL:
  :PROPERTIES:
  :BEAMER_col: 0.3
  :END:
#+BEGIN_EXPORT latex
  \includegraphics[width=\extblockscale{1.5\linewidth}]{gihhubarchivepartners.jpg}
#+END_EXPORT
#+BEAMER: \pause

***         :B_block:BMCOL:
  :PROPERTIES:
  :BEAMER_col: 0.7
  :END:
  - A **testament to the importance** of software source code preservation
#+BEAMER: \pause
  - A **multi-partners** strategy for archiving code
#+BEAMER: \pause
  - A range of **storage solutions**, from real-time to long-term storage
