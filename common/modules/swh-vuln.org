#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1

* swh-vuln
:PROPERTIES:
:CUSTOM_ID: swh-vuln
:END:

** Is my FOSS code (known to be) vulnerable?
:PROPERTIES:
:CUSTOM_ID: vuln-check
:END:
*** 
/"The Common Vulnerabilities and Exposures (CVE) system provides a reference-method for publicly known information-security vulnerabilities and exposures."/
  - e.g., CVE-2014-0160 (AKA: [[https://en.wikipedia.org/wiki/Heartbleed][Heartbleed]]), CVE-2021-44228 (AKA: [[https://en.wikipedia.org/wiki/Log4Shell][Log4Shell]])
*** Tooling
- A number of state-of-the-art tools in FOSS compliance can:
  1. *Scan* your local code base,
  2. Compare it with a *knowledge base* that knows about CVEs,
  3. Emit *warnings* like: "you include/depend on code affected by the
     following CVEs: …".
- CVE \leftrightarrow code matching heuristics are based for the most part on *package
  metadata* (in particular: version numbers)

** Example --- osv.dev
:PROPERTIES:
:CUSTOM_ID: osv-dev
:END:
***  
- [[https://osv.dev/][OSV.dev]]: /"A distributed vulnerability database for Open Source. An open, precise, and distributed approach to producing and consuming vulnerability information for open source."/
- Service (operated by Google) and data format (standardized by OpenSSF) that:
  - Crawls vulnerability information from many places (GitHub, distros, package
    manager repos, …).
  - Provides efficient APIs to query the information and integrate it into
    compliance toolchains.
*** Is my code affected by a known CVE?
# By version:
# #+BEGIN_SRC
# curl -X POST -d \
#   '{"version": "2.4.1", "package": {"name": "jinja2", "ecosystem": "PyPI"}}' \
#   "https://api.osv.dev/v1/query"
# #+END_SRC
By commit hash:
#+BEAMER: \scriptsize
#+BEGIN_SRC
$ curl -X POST -d \
  '{"commit": "6879efc2c1596d11a6a6ad296f80063b558d5e0f"}' \
  "https://api.osv.dev/v1/query"
{"vulns":[{"id":"OSV-2020-484","summary":"Heap-buffer-overflow in AAT...
#+END_SRC

** We need a /global view/ of public code
:PROPERTIES:
:CUSTOM_ID: global-view
:END:
*** 
:PROPERTIES:
:BEAMER_env: block
:BEAMER_COL: .3
:END:
#+BEAMER: \centering\includegraphics[height=0.85\textheight]{cve-swh.png}
*** 
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .7
    :END:
    - OSV.dev is a great and useful service! ... but it doesn't know about
      /all/ code out there.
    - It crawls all the repos of projects affected by known vulnerabilities,
      ... but not all their forks (which can span multiple forges!).
    #+BEAMER: \end{block} \vspace{-1mm} \begin{block} \small
    Real-world example (one out of many we have identified): 
    - Linux kernel vulnerability: GSD-2022-1004193
    - Example of vulnerable commit: b13baccc3850ca8b8cccbf8ed9912dbaa0fdf7f3
    - Fix commit: a92d44b412e75dd66543843165e46637457f22cc
    - False negative commit in fork (Raspberry PI Linux):
      c7c7a1a18af4c3bb7749d33e3df3acdf0a95bbb5

** An universal knowledge base about public code vulnerabilities
:PROPERTIES:
:CUSTOM_ID: univ-kb
:END:
*** Vision
- *Software Heritage* is the perfect (and only) place where to build an
  universal knowledge base that maps known vulnerabilities to public code
  artifacts.
- We can provide an *open data API mapping SWHIDs to CVEs*, that knows about
  /all public code commits/ and can be leveraged to increase software security
  for everybody.
*** Roadmap
- Current status: working prototype that processes OSV.dev data and use it to
  "color" the entire SWH commit graph (~4 billion commits) with vulnerability
  information.
- Upcoming feature of the archive. (See: [[https://swhsec.github.io/][swhsec.github.io]])
