#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#
# Software is all around us
#
#+INCLUDE: "prelude.org" :minlevel 1
* How Software Heritage changes the world
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** Software Heritage in a nutshell
  :PROPERTIES:
  :CUSTOM_ID: mission
  :END:
# #+latex: \begin{center}
#+ATTR_LATEX: :width \extblockscale{.4\linewidth}
file:SWH-logo+motto.pdf
# #+latex: \end{center}
*** /Collect, preserve and share/ the /source code/ of /all the software/
    \hfill Preserving our heritage, enabling better software and better science for all
#+BEAMER: \pause
*** Collect
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .3
    :END:
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[width=.3\linewidth]{SWH-logo_collect.png}
\end{center}
#+END_EXPORT 
    - open, online: 
       - /automation/
    - open, offline:
       - /crowdsourcing/
    - closed, online:
       - /escrow/
    - closed, offline:
       - /focused search/
#+BEAMER: \pause
*** Preserve
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .3
    :END:
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[width=.3\linewidth]{SWH-logo_protect.png}
\end{center}
#+END_EXPORT 
    - open source
    - traceability
    - uniform data model
      - deduplication
      - intrinsic identifiers
    - replication
      - mirror network
      - technos
      - partners
#+BEAMER: \pause
*** Share                                                           :B_block:
    :PROPERTIES:
    :BEAMER_COL: .3
    :BEAMER_env: block
    :END:
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[width=.3\linewidth]{SWH-logo_share.png}
\end{center}
#+END_EXPORT 
      - browse
      - download
      - search
      - upload
      - big data
      - ...
