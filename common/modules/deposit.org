#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1

# Requirements:
#
#+LATEX_HEADER_EXTRA: \usepackage{tikz}
#+LATEX_HEADER_EXTRA: \usetikzlibrary{arrows,shapes}
#+LATEX_HEADER_EXTRA: \definecolor{swh-orange}{RGB}{254,205,27}
#+LATEX_HEADER_EXTRA: \definecolor{swh-red}{RGB}{226,0,38}
#+LATEX_HEADER_EXTRA: \definecolor{swh-green}{RGB}{77,181,174}

* Deposit
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** Depositing source code to Software Heritage
   :PROPERTIES:
   :CUSTOM_ID: overview
   :END:
**** Deposit service
     - complement regular (pull) crawling of forges and distributions
     - restricted access (i.e., not a warez dumpster!)
     - *[[https://deposit.softwareheritage.org/][deposit.softwareheritage.org]]*
**** Tech bits
     - [[http://swordapp.org/][*SWORD 2.0*]] compliant server, for digital repositories interoperability
     - RESTful API for deposit and monitoring, with CLI wrapper
** Deposit preparation
   :PROPERTIES:
   :CUSTOM_ID: prepare
   :END:
*** Prepare source code tarball
#+BEGIN_SRC
$ tar caf software.tar.gz /path/to/software/
#+END_SRC
#+BEAMER: \pause
*** Associate metadata
#+BEAMER: \scriptsize
#+BEGIN_SRC
$ cat > software.tar.gz.metadata.xml
<?xml version="1.0"?>
<entry xmlns="http://www.w3.org/2005/Atom"
       xmlns:codemeta="https://doi.org/10.5063/SCHEMA/CODEMETA-2.0">
  <title>Je suis GPL</title>
  <codemeta:url>https://forge.softwareheritage.org/source/jesuisgpl/</codemeta:url>
  <codemeta:author>
    <codemeta:name>Stefano Zacchiroli</codemeta:name>
    <codemeta:jobTitle>Maintainer</codemeta:jobTitle>
  </codemeta:author>
</entry>
^D
#+END_SRC
** Send a deposit
   :PROPERTIES:
   :CUSTOM_ID: send
   :END:
*** 
#+BEGIN_SRC
$ swh-deposit --username 'name' --password 'pass' \
              --archive software.tar.gz
#+END_SRC
#+BEAMER: \pause
#+BEGIN_SRC
{
  'deposit_id': '11',
  'deposit_status': 'deposited',
  'deposit_date': 'Jan. 30, 2018, 9:37 a.m.'
}
#+END_SRC
** Send + status                                                   :noexport:
   :PROPERTIES:
   :CUSTOM_ID: send+status
   :END:
*** 
#+BEAMER: \footnotesize
#+BEGIN_SRC
$ swh-deposit --username 'name' --password 'pass' \
              --archive software.tar.gz
#+END_SRC
#+BEGIN_SRC
{
  'deposit_id': '11',
  'deposit_status': 'deposited',
  'deposit_date': 'Jan. 30, 2018, 9:37 a.m.'
}
#+END_SRC
#+BEAMER: \pause
*** 
#+BEAMER: \footnotesize
#+BEGIN_SRC
$ swh-deposit --username 'name' --pass 'secret' \
              --deposit-id '11' --status
#+END_SRC
#+BEGIN_SRC
{
  'id': 11,
  'status': 'done',
  'status_detail': The deposit has been successfully loaded 
                     into the Software Heritage archive',
  'swhid': 'swh:1:rev:a86747d201ab8f8657d145df4376676d5e47cf9f'
}
#+END_SRC
** Multipart deposit
   :PROPERTIES:
   :CUSTOM_ID: multipart
   :END:
*** 
#+BEAMER: \scriptsize
#+BEGIN_SRC
  $ swh-deposit --username 'name' --password 'secret' \
                --partial \
                --archive the-bulk-of-it.tar.gz
#+END_SRC
#+BEAMER: \pause
#+BEGIN_SRC
{
  'deposit_id': '11',
  'deposit_status': 'partial',
  'deposit_date': 'Jan. 30, 2018, 9:37 a.m.'
}
#+END_SRC
#+BEAMER: \pause
*** 
#+BEAMER: \scriptsize
#+BEGIN_SRC
  $ swh-deposit --username 'name' --password 'secret' \
                --partial \ # omit this to finalize
                --deposit-id 11 --archive addendum.tar.gz
#+END_SRC
#+BEAMER: \pause
*** 
#+BEAMER: \scriptsize
#+BEGIN_SRC
  $ swh-deposit --username 'name' --password 'secret' \
                --deposit-id 11 --archive last-touch.tar.gz
#+END_SRC
** Replace previous archive and/or metadata
   :PROPERTIES:
   :CUSTOM_ID: override
   :END:
*** 
#+BEAMER: \scriptsize
#+BEGIN_SRC
$ swh-deposit --username 'name' --password 'secret' \
              --deposit-id 11 \
              --archive updated-software.tar.gz \
              --replace 
#+END_SRC
#+BEAMER: \pause
#+BEAMER: \scriptsize
#+BEGIN_SRC
{
  'deposit_id': '11',
  'deposit_status': 'deposited',
  'deposit_date': 'Jan. 30, 2018, 9:37 a.m.'
}
#+END_SRC
** Ingestion status
   :PROPERTIES:
   :CUSTOM_ID: status
   :END:
\tikzstyle{fail} = [draw, thin, fill=swh-red!80!swh-orange, minimum height=1.5em]
\tikzstyle{processing} = [draw, thin, fill=swh-orange!100, minimum height=1.5em]
\tikzstyle{success} = [draw, thin, fill=swh-green!80!swh-orange, minimum height=1.5em]

\begin{figure}
\begin{tikzpicture}[node distance=1.8cm, auto,>=latex', thick]
    % We need to set at bounding box first. Otherwise the diagram
    % will change position for each frame.
    %\path[use as bounding box] (-1,0) rectangle (10,-2);
    \path[->]<1-> node[processing] (partial) {partial}
    node[success, right of=partial] (deposited) {deposited}
   (partial) edge node {} (deposited)
   node[processing, right of=deposited] (checking) {}
   (deposited) edge node {} (checking)
   node[success, right of=checking] (verified) {verified}
   node[fail, below of=checking] (rejected) {rejected}
    (checking) edge node {} (verified)
   edge node[swap] {} (rejected)
   node[processing, right of=verified] (loading) {}
   node[success, right of=loading] (done) {done}
   node[fail, below of=loading] (failed) {failed}
  (verified) edge node {} (loading)
  (loading) edge node {} (failed)
  (loading) edge node {} (done);
\end{tikzpicture}
\end{figure}
#+BEAMER: \pause
*** 
#+BEAMER: \footnotesize
#+BEGIN_SRC
$ swh-deposit --username 'name' --pass 'secret' \
              --deposit-id '11' --status
#+END_SRC
#+BEAMER: \pause
#+BEGIN_SRC
{
  'deposit_id': 11,
  'deposit_status': 'done',
  'deposit_status_detail': The deposit has been successfully loaded 
                           into the Software Heritage archive',
  'deposit_swh_id': 'swh:1:rev:a86747d201ab8f8657d145df4376676d5e47cf9f'
}
#+END_SRC
** Access a deposit
   :PROPERTIES:
   :CUSTOM_ID: access
   :END:
*** 
   After ingestion a deposit becomes an integral, permanent part of the
   archive.
   - it has a *persistent identifier*
     - e.g., =swh:1:rev:a86747d201ab8f8657d145df4376676d5e47cf9f=
   - it can be *browsed* online at *\url{archive.softwareheritage.org}*
     - e.g.,
       https://archive.softwareheritage.org/browse/swh:1:rev:a86747d201ab8f8657d145df4376676d5e47cf9f
   # - it can be *downloaded* using the Software Heritage Vault
  #+BEAMER: \pause \vfill
*** Software Heritage Identifiers (SWHIDs)
    - *adoption*: Wikidata, SPDX, Identifiers.org, US NTIA (under
      consideration), IANA (ongoing submission)
    - Di Cosmo, Gruenpeter, Zacchiroli. /Identifiers for Digital Objects: the
      Case of Software Source Code Preservation/. In proceedings of iPRES 2018.
    - https://docs.softwareheritage.org/devel/swh-model/persistent-identifiers.html

** Deposit use case: CCS compliance                                :noexport:
   :PROPERTIES:
   :CUSTOM_ID: ccs-compliance
   :END:
  #+BEAMER: \vspace{-1mm}
*** Complete Corresponding Source (CCS) requirement
   #+BEGIN_QUOTE
   For an executable work, complete source code means all the source code for
   all modules it contains, plus any associated interface definition files,
   plus the scripts used to control compilation and installation of the
   executable.\hfill --- /GPLv2/
   #+END_QUOTE

  #+BEAMER: \vspace{-1mm}
*** CCS management in the real world
    - CCS tarballs published at release time; URLs included in user manuals
    - IT reorganizations → link rot (e.g., 404 on CCS URLs) → out of compliance

  #+BEAMER: \pause
  #+BEAMER: \vspace{-1mm}
*** A better approach \hfill (Intel+SWH prototype)
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .48
    :END:
    Delegate CCS hosting to an archive:
    1. prepare CCS tarball
    2. deposit it to Software Heritage
    3. include SWHID in user manuals

  #+BEAMER: \pause
*** Is it compliant?
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .55
    :END:
    - TL;DR: yes! (with agreement with hoster)
    - Cf. GPL FAQ /Can I put the binaries on my Internet server and put the
      source on a different Internet site?/

