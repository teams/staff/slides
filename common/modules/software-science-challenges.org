* Challenges and opportunities
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** Software is Knowledge
  :PROPERTIES:
  :CUSTOM_ID: softwareisknowledge
  :END:
*** /Key mediator/ for accessing /all/ information (c) Banski	 :B_picblock:
    :PROPERTIES:
    :BEAMER_opt: pic=Banski-Information-Pillar-Society-Small, leftpic=true
    :BEAMER_env: picblock
    :END:

Information is *a main pillar* of our modern societies.
#+BEGIN_QUOTE
Absent an ability to correctly interpret digital information, we are left
with [...] "rotting bits" [...] of no value.

\mbox{}\hfill Vinton G. Cerf IEEE 2011
#+END_QUOTE
#+BEAMER: \pause
*** Software is /an essential component/ of modern scientific research  :B_picblock:
#    Deep knowledge embodied in complex software systems
    :PROPERTIES:
    :BEAMER_opt: pic=papermountain,width=.25\linewidth
    :BEAMER_env: picblock
    :END:      
#+BEGIN_QUOTE
[...] the vast majority describe experimental methods or software that have become essential in their fields.
#+END_QUOTE
\mbox{}\hfill Top 100 papers (Nature, October 2014)
# http://www.nature.com/news/the-top-100-papers-1.16224

** So many challenges and opportunities: let's name a few
  :PROPERTIES:
  :CUSTOM_ID: challengesopportunities
  :END:
*** Scientific (non) reproducibility                             :B_picblock:
    :PROPERTIES:
    :BEAMER_opt: pic=templescience, leftpic=true, width=.4\linewidth
    :BEAMER_env: picblock
    :END:      
    - inconsistencies
    - data corruption
    - fraud
    - non reproducible findings...
    (picture from Nature, Sep. 2015)
*** (Lack of) Security and Quality of Software
     - vulnerabilities and bugs in systems...
     - ... become critical with IoT
*** (Potential) Citizen empowerment
     \mbox{}\hfill we need to have *access* to the /software that governs our life/
** Zoom on reproducibility
*** Reproducibility (Wikipedia)
    the ability of an entire experiment or study to be \emph{reproduced}\\
    \hfill either by the researcher or \emph{by someone else working independently}.
*** Why we want it
    - foundation of the scientific method
    - accelerator of research: allows to build upon previous work
    - transparency of results eases acceptance
    - reproducibility is \emph{the essence} of \emph{industry}!
*** For an experiment involving software, we need
   - open access :: to the scientific article describing it
   - open data sets :: used in the experiment
   - source code :: of all the components
   # - environment :: of execution
   # - stable references :: between all this

* Software source code is essential
#+INCLUDE: "../../common/modules/source-code-different-short.org::#main" :only-contents t :minlevel 2

