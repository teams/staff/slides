#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Presentation at Google (confidential)
#+AUTHOR: Roberto Di Cosmo
#+DATE: 12 Nov 2015
#+EMAIL: roberto@dicosmo.org
#+DESCRIPTION: Preserving the technological knowledge of mankind
#+KEYWORDS: software heritage legacy preservation knowledge mankind technology
#+BEAMER_HEADER_EXTRA: \title[Preserving Knowledge]{Preserving and sharing the technological knowledge of mankind}

#
# Prelude contains all the information needed to export the main beamer latex source
#

#+INCLUDE: "prelude.org" :minlevel 1

#
# why software is important (2 slides)
#
#+INCLUDE: "software-all-around-us.org::#main" :minlevel 1

#
# what SWH does (mission, collect protect share, basic infrastructure schema)
#
#+INCLUDE: "swh-overview.org::#main" :minlevel 1

#
# Key design questions
#
#+INCLUDE: "swh-design.org" :minlevel 1

#
# Better society, better industry, better science
# 
#+INCLUDE: "vision.org::#main" :minlevel 1

#
# How we want to work
#
#+INCLUDE: "bits-drawing-board.org::#main" :minlevel 1

#
# What they say of SWH
#
#+INCLUDE: "support-quotes-short.org::#main" :minlevel 1

* Making it happen
** Status
*** Initial support by Inria
    French national institute for research /on computer science/, with 2700 collaborators,
    contributed to the birth of the W3C, and hosts many prestigious scientists.\\
    Inria is fully supporting the bootstrap phase of Software Heritage.
*** We need to go global
    Software Heritage cannot and must not be an Inria-only project.
*** Our ambition
    A sustainable, multistakeholder, non for profit organisation for
    - organising the technological development
    - coordinating a network of peers
    - structuring the collaboration
** We would like Google to help
*** Financial							    
   Contribute to an endowment fund to
   - accelerate development and take up
   - ensure stable operation in the first 5 years
*** Technical    
   Contribute
   - big data infrastructure (disk, bandwidth, cpu, DB)
   - engineering resources
*** Help get together all the involved parties
    - grow a network of peers
    - sponsor workshop(s) on Software Heritage
    - sponsor international conference on software preservation
* Conclusion
** Contact us
   /email:/ roberto@dicosmo.org\vfill
   /upcoming website:/ http://www.softwareheritage.org
