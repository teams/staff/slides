#!/bin/bash
#
# expand custom ids in org module and produce appropriate org snippets to import them in a global summary document
#
# Example usage:  genfull.sh swh-ardc.org
#

for f in $@; do
    for tag in `fgrep CUSTOM $f | grep -v ':CUSTOM_ID: main' | sed 's/.*:CUSTOM_ID: //'`; do
	echo "** modules/$f::#$tag"
	echo "#+INCLUDE: ../../common/modules/$f::#$tag :only-contents t :minlevel 3"
    done
done
