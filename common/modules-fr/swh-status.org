#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1
* Point d'étape
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** Avancement du projet						    :B_frame:
   :PROPERTIES:
   :BEAMER_env: frame
   :END:
*** Le rôle d'INRIA 						    :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_act: +-
    :END:
   une évidence
   - le /Logiciel/ est l'/ADN de l'Institut/
   - des compétences au meilleur niveau international
*** Aujourd'hui ... un milliard 				    :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_act: +-
    :END:
   - phase d'amorçage portée par INRIA
   - prototype en état avancé de dévéloppement
#+latex: \begin{center}
#+ATTR_LATEX: :width \extblockscale{.9\linewidth}
file:swh-archive-stats.png
#+latex: \end{center}

** Notre objectif						    :B_frame:
   :PROPERTIES:
   :BEAMER_env: frame
   :END:
*** Une organisation non for profit 				    :B_block:
   :PROPERTIES:
   :BEAMER_env: block
   :BEAMER_act: +-
   :END:
   sur une perspective de long terme:
   - organiser le dévéloppement technique
   - coordonner un réseau international
   - structurer la collaboration
*** Une approche partenariale
   - on cherche des /cofondateurs/ pour l'annonce publique
   - on ouvrira ensuite à d'autres /partenaires/
*** Oui, c'est ouvert!						    :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :END:
   ouverture des specifications, du code source, du processus
