#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)
#+INCLUDE: "prelude.org" :minlevel 1
* Des soutiens multiples
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** Ils disent de Software Heritage
   :PROPERTIES:
   :BEAMER_opt: allowframebreaks
   :END:      
*** Serge Abiteboul (Academie de Science)
     # Software embodies a large part of all the technical and scientific knowledge
     # that is being developed today.

     Software Heritage will create ...
     # the largest public archive of software artifacts, which is 
     an essential infrastructure to grow and disseminate a universal body of knowledge.
*** Jean-Francois Abramatic (ancien directeur du W3C)
     # Software is the key to universal access to our web of information.\\
     # Software has become an essential component of our daily lives.\\
     # Software must be collected, organized, preserved and shared.\\
     /We need the Software Heritage project/ to make this goal a reality.
*** Mike Milinkovich (Executive Director, Eclipse Foundation)
    Software is the fabric which binds our personal, social, industrial, and digital lives. ...

    # It is an important part of our cultural heritage, as well as a significant
    # portion of the intellectual efforts of humanity since the invention of the
    # digital computer.

    The Eclipse Foundation is pleased to support this important effort to
    preserve and protect humanity's software legacy.
*** Nenad Medvidovic (Chair, ACM Sigsoft)
    # Software is now present everywhere. It is a pillar of our modern societies, and the source code
    # of this software embodies our scientific and technological knowledge. In a word, it is our DNA.
    We fully support the objective of the Software Heritage project to create a comprehensive public
    archive of software artifacts.
*** Diomidis Spinellis (Editor-in-Chief, IEEE Software)
    I wish to express my wholehearted support for the Software Heritage
    project. 
    # The vision of IEEE Software is building the community of leading
    # software practitioners. The long-term preservation of our community’s work
    # and achievements can be an important element for realizing our vision.
*** Carlo Ghezzi (President, Informatics Europe)
    We would like to express Informatics Europe’s support to the “Software
    Heritage” project.  

    # Informatics Europe fully support the projects’ mission
    # of collecting, organising, preserving and sharing all the software that lies
    # at the heart of our culture and our society by creating a largest, curated,
    # and public archive of software artifacts, together with extensive metadata.
*** Simon Phipps (ancien président d'OSI)
    # Open source software is now the assumed default for the IT industry and for
    # every computer user. It is also here to stay for the foreseeable future.  
    We now
    need to secure past open source achievements, not only because they are part of
    human history, but also because they will come in handy centuries from now to
    read today's data.  The Software Heritage project will be the key actor in
    making such preservation possible.
