#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)
#+INCLUDE: "prelude.org" :minlevel 1
#
# Some key design decisions we made, and why
#
* Quelques élements de conception
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** Le logiciel libre est essentiel
*** D. Rosenthal, EUDAT, 9/2014 
    :PROPERTIES:
    :BEAMER_act: +-
    :END:      
#+latex:  \begin{quote}
you have to do [digital preservation] with open-source software; closed-source
preservation has the same fatal "just trust me" aspect that closed-source
encryption (and cloud storage) suffer from.
#+latex: \end{quote}
*** recommendation
    :PROPERTIES:
    :BEAMER_act: +-
    :END:      
  nous devons:
    - publier tous les détais de notre architecture
    - rendre disponible tout le logiciel que nous écrivons
    - utiliser des standards ouverts
    - encourager un processus de dévéloppement ouver et collaboratif
** Le liens web /ne sont pas/ permanents (même les /permalinks/)
*** T. Berners-Lee et al. Uniform Resource Locators. RFC 1738.
    :PROPERTIES:
    :BEAMER_act: +-
    :END:      
#+latex:  \begin{quote}
   Users should beware that there is no general guarantee that a URL
   which at one time points to a given object continues to do so, 
   and does not even at some later time point to a different object
   due to the movement of objects on servers.
#+latex: \end{quote}
*** The Decay and Failures of URL References
    :PROPERTIES:
    :BEAMER_act: +-
    :END:      
#+latex:  \begin{quote}
   la démi-vie d'une référence Web est de 4 ans\\
   \hfill Diomidis Spinellis, CACM 2003
#+latex: \end{quote}
*** recommendation
    :PROPERTIES:
    :BEAMER_act: +-
    :END:      
   nous devons:
     - fournir des identifiants uniques /intrinséques/
     - /éviter/ des approches basées sur des intermediaires comme le DOI
** La replication est essentielle
*** Thomas Jefferson, February 18, 1791
    :PROPERTIES:
    :BEAMER_act: +-
    :END:      
#+latex:  \begin{quote}
  ...let us save what remains: not by vaults and locks which fence them
  from the public eye and use in consigning them to the waste of time,
  but by such a multiplication of copies, as shall place them beyond
  the reach of accident.
#+latex: \end{quote}
*** recommendation
    :PROPERTIES:
    :BEAMER_act: +-
    :END:      
   nous devons:
   - faciliter la réalisation de copies de l'archive
   - faire naître un réseau planétaire de miroirs
# *** replication, not confusion!
#    :PROPERTIES:
#    :BEAMER_act: +-
#    :END:      
#    - do not create /another archive/ of SW, become a /mirror node/!
#    - to ensure sustainability, pool resources, do not divide them!

