#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)
#+INCLUDE: "prelude.org" :minlevel 1
* Logo
# Short text for Collect/Preserve/Share
** Notre mission dans notre logo
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
*** \mbox{}
#+Latex: \begin{center}
  \animategraphics[autoplay,controls,width=.5\linewidth]{2}{SWH-logo_anim-texte-}{0}{28}
# add loop as parameter if needed
#+Latex: \end{center}

*** Collecter le logiciel, /tout/ le logiciel

#    Because any software artifact may turn out to be essential in the future.

#    We track software’s origin and history.
*** /preserver/ le logiciel
    il contient notre connaissance technique et scientifique.

#    Software is the means of accessing all of our knowledge.
*** Indexer, organiser, rendre facilement /accessible/ tout le logiciel
#    We are building the largest archive of software ever conceived.
    
